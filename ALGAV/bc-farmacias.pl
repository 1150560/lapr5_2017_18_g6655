%farmacia(farmaciaNome,latitude,longitude)

farmacia(armazem,41.177778,-8.607778).
farmacia(nau_vitoria,41.1741622,-8.5821195).
farmacia(barreiros,41.1605815,-8.6210144).
farmacia(central,41.1857224,-8.6537477).
farmacia(pinhais_da_foz,41.1555613,-8.6681147).
farmacia(pedra_verde,41.1933078,-8.6179276).
farmacia(cortes,41.1933045,-8.6109170).
farmacia(confianca,41.1932312,-8.6115012).
farmacia(costa_cabral,41.1720903,-8.5876506).
farmacia(cruz,41.1687099,-8.5907066).
farmacia(amial,41.1838825,-8.6135012).
farmacia(areosa,41.1813214,-8.5833519).
farmacia(antero_de_quental,41.1627813,-8.6110322).
farmacia(henriques,41.1447786,-8.6065682).
farmacia(saude,41.1653710,-8.5988215).
farmacia(alirio_de_barros,41.1639303,-8.6013361).
farmacia(silva_pereira,41.1640646,-8.6013830).
farmacia(cosme,41.1659656,-8.5975488).
farmacia(guerra,41.1622917,-8.6036776).
farmacia(martino,41.1613002,-8.6038802).
farmacia(vales,41.1861384,-8.5967432).
farmacia(central_corujeira,41.1592687,-8.5803088).
farmacia(hospital_sao_joao,41.1828290,-8.6016890).
farmacia(borralho,41.1559043,-8.5947325).
farmacia(louzada,41.1692780,-8.6088261).
farmacia(soeiro,41.1494042,-8.6005555).
farmacia(sao_dinis,41.1674205,-8.6180315).
farmacia(ramos,41.1661999,-8.6249140).
farmacia(alves,41.1668743,-8.6250454).
farmacia(mafalda,41.1615666,-8.6159409).
farmacia(silva_dias,41.1751795,-8.5654173).
farmacia(guimaraes,41.1671146,-8.6341355).
farmacia(cortes_pinto,41.1610881,-8.6082907).
farmacia(marques_de_mendonca,41.1749664,-8.6137437).
farmacia(alves_da_silva,41.1592476,-8.6356406).
farmacia(viamial,41.1792256,-8.6140360).
farmacia(sao_caetano,41.1691519,-8.5601841).
farmacia(magalhaes,41.1346218,-8.6318454).
farmacia(prelada,41.1693651,-8.6367504).
farmacia(oriental,41.1552360,-8.6077274).
farmacia(ferreira_da_silva,41.1805944,-8.6546745).
farmacia(fonte_da_moura,41.1624132,-8.6576170).
farmacia(central_da_liga,41.1491352,-8.6088019).
farmacia(correia,41.1583571,-8.6278354).
farmacia(boavista,41.1564064,-8.6203815).
farmacia(lapa,41.1600454,-8.6118548).
farmacia(campo,41.1551321,-8.6134212).
farmacia(cristo_rei,41.1593238,-8.6653715).
farmacia(serap_pinto,41.1663122,-8.6204918).
farmacia(queija_ferreira,41.1630983,-8.5900952).
farmacia(falcao,41.1651536,-8.6705991).

%rua(ruaNumero,latitude1,longitude1,latitude2,longitude2)

rua(rua1,41.1857224,-8.6537477, 41.18304, -8.65387).
rua(rua2,41.1805944,-8.6546745, 41.18304, -8.65387).
rua(rua3,41.1651536,-8.6705991, 41.16062, -8.66941).
rua(rua4,41.1555613,-8.6681147, 41.16062, -8.66941).
rua(rua5,41.1593238,-8.6653715, 41.16062, -8.66941).
rua(rua6,41.1624132,-8.6576170, 41.16088, -8.66143).
rua(rua7,41.1933078,-8.6179276, 41.18989, -8.61465).
rua(rua8,41.1932312,-8.6115012, 41.18989, -8.61465).
rua(rua9,41.1933045,-8.6109170, 41.18989, -8.61465).
rua(rua10,41.1346218,-8.6318454, 41.13774, -8.62873).
rua(rua11,41.1447786,-8.6065682, 41.14679, -8.61044).
rua(rua12,41.1491352,-8.6088019, 41.15177, -8.60538).
rua(rua13,41.1751795,-8.5654173, 41.17151, -8.56384).
rua(rua14,41.1691519,-8.5601841, 41.17151, -8.56384).
rua(rua15,41.1838825,-8.6135012, 41.18989, -8.61465).
rua(rua16,41.1861384,-8.5967432, 41.18446, -8.59946).
rua(rua17,41.1828290,-8.6016890, 41.18446, -8.59946).
rua(rua18,41.1792256,-8.6140360, 41.17762, -8.61104).
rua(rua19,41.177778,-8.607778, 41.17762, -8.61104).
rua(rua20,41.1749664,-8.6137437, 41.17762, -8.61104).
rua(rua21,41.1693651,-8.6367504, 41.16573, -8.63782).
rua(rua22,41.1671146,-8.6341355, 41.16573, -8.63782).
rua(rua23,41.1592476,-8.6356406, 41.16573, -8.63782).
rua(rua24,41.1813214,-8.5833519, 41.17765, -8.58238).
rua(rua25,41.1741622,-8.5821195, 41.17765, -8.58238).
rua(rua26,41.1583571,-8.6278354, 41.16211, -8.62495).
rua(rua27,41.1605815,-8.6210144, 41.16211, -8.62495).
rua(rua28,41.1661999,-8.6249140, 41.16211, -8.62495).
rua(rua29,41.1668743,-8.6250454, 41.16211, -8.62495).
rua(rua30,41.1615666,-8.6159409, 41.15797, -8.61671).
rua(rua31,41.1564064,-8.6203815, 41.15797, -8.61671).
rua(rua32,41.1551321,-8.6134212, 41.15797, -8.61671).
rua(rua33,41.1600454,-8.6118548, 41.15797, -8.61671).
rua(rua34,41.1674205,-8.6180315, 41.16947, -8.61431).
rua(rua35,41.1663122,-8.6204918, 41.16947, -8.61431).
rua(rua36,41.1692780,-8.6088261, 41.16947, -8.61431).
rua(rua37,41.1552360,-8.6077274, 41.15177, -8.60538).
rua(rua38,41.1559043,-8.5947325, 41.15094, -8.58860).
rua(rua39,41.1592687,-8.5803088, 41.15094, -8.58860).
rua(rua40,41.1630983,-8.5900952, 41.15094, -8.58860).
rua(rua41,41.1720903,-8.5876506, 41.16967, -8.58774).
rua(rua42,41.1687099,-8.5907066, 41.16967, -8.58774).
rua(rua43,41.1640646,-8.6013830, 41.16705, -8.60032).
rua(rua44,41.1639303,-8.6013361, 41.16705, -8.60032).
rua(rua45,41.1653710,-8.5988215, 41.16705, -8.60032).
rua(rua46,41.1659656,-8.5975488, 41.16705, -8.60032).
rua(rua47,41.1627813,-8.6110322, 41.16482, -8.60658).
rua(rua48,41.1610881,-8.6082907, 41.16482, -8.60658).
rua(rua49,41.1622917,-8.6036776, 41.16482, -8.60658).
rua(rua50,41.1613002,-8.6038802, 41.16482, -8.60658).
rua(rua51,41.16062,-8.66941, 41.16088, -8.66143).
rua(rua52,41.17025,-8.65920, 41.16062, -8.66941).
rua(rua53,41.17025,-8.65920, 41.16088, -8.66143).
rua(rua54,41.17025,-8.65920, 41.17271, -8.64606).
rua(rua55,41.17994,-8.64237, 41.17271, -8.64606).
rua(rua56,41.17994,-8.64237, 41.18304, -8.65387).
rua(rua57,41.18304,-8.65387, 41.17271, -8.64606).
rua(rua58,41.17994,-8.64237, 41.18614, -8.62795).
rua(rua59,41.17994,-8.64237, 41.17690, -8.62752).
rua(rua60,41.18614,-8.62795, 41.17690, -8.62752).
rua(rua61,41.18614,-8.62795, 41.18989, -8.61465).
rua(rua62,41.18543,-8.60718, 41.18989, -8.61465).
rua(rua63,41.18543,-8.60718, 41.18446, -8.59946).
rua(rua64,41.17962,-8.59285, 41.18446, -8.59946).
rua(rua65,41.18543,-8.60718, 41.17762, -8.61104).
rua(rua66,41.18614,-8.62795, 41.17762, -8.61104).
rua(rua67,41.17690,-8.62752, 41.17762, -8.61104).
rua(rua68,41.17962,-8.59285, 41.17762, -8.61104).
rua(rua69,41.17367,-8.60358, 41.17762, -8.61104).
rua(rua70,41.17367,-8.60358, 41.17962, -8.59285).
rua(rua71,41.17765,-8.58238, 41.17962, -8.59285).
rua(rua72,41.17765,-8.58238, 41.16967, -8.58774).
rua(rua73,41.17962,-8.59285, 41.16967, -8.58774).
rua(rua74,41.16935,-8.57585, 41.16967, -8.58774).
rua(rua75,41.16935,-8.57585, 41.17765, -8.58238).
rua(rua76,41.16935,-8.57585, 41.17151, -8.56384).
rua(rua77,41.17765,-8.58238, 41.17151, -8.56384).
rua(rua78,41.16967,-8.58774, 41.15904, -8.58860).
rua(rua79,41.16935,-8.57585, 41.15904, -8.58860).
rua(rua80,41.16967,-8.58774, 41.17367, -8.60358).
rua(rua81,41.16947,-8.61431, 41.17367, -8.60358).
rua(rua82,41.16947,-8.61431, 41.16418, -8.61508).
rua(rua83,41.16947,-8.61431, 41.17690, -8.62752).
rua(rua84,41.16573,-8.63782, 41.17690, -8.62752).
rua(rua85,41.16573,-8.63782, 41.16947, -8.61431).
rua(rua86,41.16573,-8.63782, 41.16211, -8.62495).
rua(rua87,41.16418,-8.61508, 41.16211, -8.62495).
rua(rua88,41.15481,-8.63516, 41.16211, -8.62495).
rua(rua89,41.15481,-8.63516, 41.16088, -8.66143).
rua(rua90,41.17025,-8.65920, 41.16573, -8.63782).
rua(rua91,41.17271,-8.64606, 41.16573, -8.63782).
rua(rua92,41.17367,-8.60358, 41.16705, -8.60032).
rua(rua93,41.17367,-8.60358, 41.16482, -8.60658).
rua(rua94,41.16947,-8.61431, 41.16482, -8.60658).
rua(rua95,41.16418,-8.61508, 41.16482, -8.60658).
rua(rua96,41.15797,-8.61671, 41.15481, -8.63516).
rua(rua97,41.15797,-8.61671, 41.16418, -8.61508).
rua(rua98,41.15836,-8.60066, 41.15904, -8.58860).
rua(rua99,41.15836,-8.60066, 41.15177, -8.60538).
rua(rua100,41.14679,-8.61044, 41.15177, -8.60538).
rua(rua101,41.14679,-8.61044, 41.14770, -8.62323).
rua(rua102,41.15481,-8.63516, 41.14770, -8.62323).
rua(rua103,41.15797,-8.61671, 41.14770, -8.62323).
rua(rua104,41.16482,-8.60658, 41.16705, -8.60032).
rua(rua105,41.15836,-8.60066, 41.16705, -8.60032).
rua(rua106,41.16967,-8.58774, 41.16705, -8.60032).
rua(rua107,41.15797,-8.61671, 41.15177, -8.60538).
rua(rua108,41.15481,-8.63516, 41.13774, -8.62873).
rua(rua109,41.14770,-8.62323, 41.13774, -8.62873).
rua(rua110,41.14770,-8.62323, 41.14679, -8.61044).
rua(rua111,41.1494042,-8.6005555, 41.15177, -8.60538).


% ---------------------- TSP 2 -----------------

dist_farmacia(C1,C2,Dist):-
    farmacia(C1,Lat1,Lon1),
    farmacia(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).

degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in Km)
distance(Lat1, Lon1, Lat2, Lon2, Dis3):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1),
	Dis3 is Dis2/1000.

tsp2(Cam, Custo, Lista, Tempo):-
    get_time(Ti),
    bfs(armazem, armazem, Lista, Cam, Custo), !,
    get_time(Tf),
    Tempo is Tf -Ti.

greedy([Dest|LA], CustoAtual, CustoFinal, Cidade, Lista):-
    findall((Dist, City),
            (dist_farmacia(City, Dest, Dist),
            \+ member(City, [Dest|LA]), member(City, Lista)),
            L),
    sort(L, Res),
    member((CustoCid, Cidade), Res),
    CustoFinal is CustoCid + CustoAtual.


bfs(Orig, Dest, Lista, Cam, Custo):-
     bfs2(Orig, [Dest], Lista, 0 ,Cam, 0, Custo).

bfs2(Orig, X, Lista, N, Cam, CustoAtual, CustoFinal):-
    findall(C, (farmacia(C,_,_), C \== Orig, member(C, Lista)), L),
    length(L, N1),
    N == N1,
    X = [C1|_],
    % adiciona custo da origem
    dist_farmacia(Orig, C1, Dist),
    CustoFinal is CustoAtual + Dist,
    % coloca por ordem correta
    TempL = [Orig | X],
    reverse(TempL, Cam) ,!.

bfs2(Orig, [Dest|LA], Lista, N, Cam, CustoAtual, CustoFinal):-
    N1 is N+1,
    greedy([Dest|LA], CustoAtual, CA1, Cidade, Lista),
    bfs2(Orig, [Cidade | [Dest | LA]], Lista, N1, Cam, CA1, CustoFinal).


determina_rota(Cam, Custo, Lista):-
    bfs(armazem, armazem, Lista,  CA, _),
    remove_intersections(CA, [], Cam),
    custo(Cam, Custo),
    !.

% Calcula custo dos segmentos de uma lista
% Para quando restarem 2 cidades
custo([C1, C2], Custo):- dist_farmacia(C1, C2, Custo).

custo([C1, C2 | T], Custo):-
    custo([C2 | T], CA1),
    dist_farmacia(C1, C2, CA2),
    Custo is CA1 + CA2.

remove_intersections(Original, Intersections, Final):-
    % Procura segmentos
    segment(Original, (C1, C2)),
    segment(Original, (C3, C4)),

    % Segmentos nao podem ser iguas nem pode ser 2 segmentos formados por 3 pontos
    C1 \== C3, C1 \== C4,
    C2 \== C3, C2 \== C4,

    % Verifica se segmentos intersetam
    doIntersectCity(C1, C2, C3, C4),

    % Resolve interse��o
    ((\+member((C1,C3,C2,C4), Intersections), solve_intersection(C1, C3, Original, LAlterada))
    ;
    (replace(C3, C4, Original, LA), solve_intersection(C1, C3, LA, LAlterada))),
    !,

    remove_intersections(LAlterada, [(C1,C2,C3,C4) | Intersections], Final).

% Termina quando n�o encontrar interse��es
remove_intersections(L, _, L).


% Devolve um segmento
segment([C1, C2 | _], (C1, C2)).
segment([_|T], Res):- segment(T, Res).

solve_intersection(Ini, Fim, List, Result):-
    solve_intersection2(Ini, Fim, List, [], Result), !.

% Quando encontra ponto inicial resolve a interse��o apartir dele
solve_intersection2(Ini, Fim, [Ini | List], LA, Result):-
    solve_intersection3(Fim, List, [], Solved),
    append(LA, [Ini | Solved], Result).

% Procura por ponto inicial
solve_intersection2(Ini, Fim, [H | List], LA, Result):-
    append(LA, [H], LA1),
    solve_intersection2(Ini, Fim, List, LA1, Result).

% Inverte a lista desde o ponto inicial at� ao ponto final
solve_intersection3(C, [C | T], Reverse, Result):-
     append([C | Reverse], T, Result).

% Procura por ponto final
solve_intersection3(C, [H | T], Reverse, Result):-
    solve_intersection3(C, T, [H | Reverse], Result).

replace(_, _, [], []).
replace(X, Y, [X|T], [Y|T1]) :- replace(X, Y, T, T1), !.
replace(X, Y, [Y|T], [X|T1]) :- replace(X, Y, T, T1), !.
replace(X, Y, [H|T], [H|T1]) :- H \== X, replace(X, Y, T, T1), !.

doIntersectCity(C1, C2, C3, C4):-
    farmacia(C1, C1X, C1Y),
    farmacia(C2, C2X, C2Y),
    farmacia(C3, C3X, C3Y),
    farmacia(C4, C4X, C4Y),
    doIntersect((C1X, C1Y), (C2X, C2Y), (C3X, C3Y), (C4X, C4Y)).

% Given three colinear points p, q, r, the function checks if
% point q lies on line segment 'pr'
%onSegment(P, Q, R)
%
onSegment((PX,PY), (QX,QY), (RX,RY)):-
    QX =< max(PX,RX),
    QX >= min(PX,RX),
    QY =< max(PY,RY),
    QY >= min(PY,RY).

% To find orientation of ordered triplet (p, q, r).
% The function returns following values
% 0 --> p, q and r are colinear
% 1 --> Clockwise
% 2 --> Counterclockwise
orientation((PX,PY), (QX,QY), (RX,RY), Orientation):-
             Val is (QY - PY) * (RX - QX) - (QX - PX) * (RY - QY),

             (
                 Val == 0, !, Orientation is 0;
                 Val >0, !, Orientation is 1;
                 Orientation is 2
             ).
orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4):-
    orientation(P1, Q1, P2,O1),
    orientation(P1, Q1, Q2,O2),
    orientation(P2, Q2, P1,O3),
    orientation(P2, Q2, Q1,O4).


% The main function that returns true if line segment 'p1q1'
% and 'p2q2' intersect.
doIntersect(P1,Q1,P2,Q2):-
    % Find the four orientations needed for general and
    % special cases
    orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4),

    (
    % General case
    O1 \== O2 , O3 \== O4,!;

    % Special Cases
    % p1, q1 and p2 are colinear and p2 lies on segment p1q1
    O1 == 0, onSegment(P1, P2, Q1),!;
    % p1, q1 and p2 are colinear and q2 lies on segment p1q1
    O2 == 0, onSegment(P1, Q2, Q1),!;
    % p2, q2 and p1 are colinear and p1 lies on segment p2q2
    O3 == 0, onSegment(P2, P1, Q2),!;
     % p2, q2 and q1 are colinear and q1 lies on segment p2q2
    O4 == 0, onSegment(P2, Q1, Q2),!
    ).


% ---------------------- A STAR -----------------

aStar(Orig, Dest, Cam, CustoF):- farmacia(Orig, Lat1, Lon1),
                                (rua(RuaInicial, Lat1, Lon1, Lat1F, Lon1F); rua(ruaInicial, Lat1F, Lon1F, Lat1, Lon1)),
                                farmacia(Dest, Lat2, Lon2),
                                (rua(RuaFinal, Lat2, Lon2, Lat2F, Lon2F); rua(ruaFinal, Lat2F, Lon2F, Lat2, Lon2)),
                                distance(Lat1, Lon1, Lat1F, Lon1F, CustoRuaInicial), distance(Lat2, Lon2, Lat2F, Lon2F, CustoRuaFinal),
                                Custo is CustoRuaInicial + CustoRuaFinal,
                                aStar2((RuaFinal, Lat2F, Lon2F), [(_, Custo, [(RuaInicial, Lat1F, Lon1F)])], Cam2, CustoF), !,
                                caminhoComDistancia(Cam2, Cam).

aStar2((Dest, Lat, Lon), [(_, Custo, [(Penultimo, Lat, Lon)|T])|_], Cam, Custo):- reverse([(Dest, Lat, Lon), (Penultimo, Lat, Lon)|T],Cam).
aStar2((Dest, LatDest, LonDest), [(_, Ca, LA)|Outros], Cam, Custo):- LA=[(_, LatAct, LonAct)|_],
    findall((CEX, CaX, [(X, LatX, LonX)|LA]),
            ((rua(X, LatAct, LonAct, LatX, LonX); rua(X, LatX, LonX, LatAct, LonAct)),
            \+member((X, _, _), LA), distance(LatX, LonX, LatAct, LonAct, CustoX),
            CaX is CustoX + Ca, distance(LatDest, LonDest, LatX, LonX, EstX), CEX is CaX+ EstX),
            Novos),
    append(Outros, Novos, Todos), sort(Todos, TodosOrd),
    aStar2((Dest, LatDest, LonDest), TodosOrd, Cam,Custo).

caminhoComDistancia([], []).
caminhoComDistancia([(Nome, _, _)|T], [(Nome, Custo)|Cam]):- caminhoComDistancia(T, Cam),
                                                    rua(Nome, Lat1, Lon1, Lat2, Lon2),
                                                    distance(Lat1, Lon1, Lat2, Lon2, Custo).

determinarRuas(Farmacias, Ruas, CustoTotal):- length(Farmacias, NFarmacias), NFarmacias > 1,
                                              determinarRuas(Farmacias, Ruas, [], CustoTotal, 0).
determinarRuas([_|[]], Ruas, List, Custo, Custo):- reverse(List, Ruas).
determinarRuas([F1, F2|T], Ruas, List, CustoTotal, Custo):- aStar(F1, F2, Cam, CustoF), Custo2 is Custo+CustoF,
                                                            determinarRuas([F2|T], Ruas, [[(F1-F2), CustoF, Cam] | List], CustoTotal, Custo2).
