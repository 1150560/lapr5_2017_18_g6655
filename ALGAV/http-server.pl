%  run using:
%  sudo swipl -q -s test.pl -- --syslog=swi-httpd --user=ubuntu

% Bibliotecas
:- module(http_daemon, []).
:- use_module(library(http/http_unix_daemon)).
%:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_json)).

:- consult('bc-farmacias.pl').

:- initialization http_daemon.

% Rela��o entre pedidos HTTP e predicados que os processam
:- http_handler('/rotas', handle_rotas, []).
:- http_handler('/ruas', handle_ruas, []).
:- http_handler('/ruasCaminho', handle_ruas_caminho, []).


% Cria��o de servidor HTTP no porto 'Port'
server(Port) :-
        http_server(http_dispatch, [port(Port)]).

handle_rotas(Request) :-
   http_read_json(Request, TermIn,[json_object(term)]),

   TermIn = json([farmacias=Farmacias]),

   determina_rota(Rota, Custo, Farmacias),

   TermOut = json([farmacias=Rota, custo=Custo]),

   reply_json(TermOut).

handle_ruas(Request) :-
   http_read_json(Request, TermIn,[json_object(term)]),

   TermIn = json([origem=Origem, destino=Destino]),

   aStar(Origem, Destino, Cam, Custo),

   formatCam(Cam, CamJSON),
   TermOut = json([ruas=CamJSON ,custo=Custo]),

   reply_json(TermOut).


handle_ruas_caminho(Request) :-
   http_read_json(Request, TermIn,[json_object(term)]),

   TermIn = json([rota=Rota]),

   determinarRuas(Rota, Ruas, Custo),
   formatRuas(Ruas, RuasJSON),
   TermOut = json([caminho=RuasJSON, custo=Custo]),

   reply_json(TermOut).


formatCam([], []).
formatCam([(Rua, Custo) | T], [TermOut | Terms]):-
        TermOut = json([rua=Rua, custo=Custo]),
        formatCam(T, Terms).


formatRuas([], []).
formatRuas([[Origem-Destino, Custo, Ruas] | T], [TermOut | Terms]):-
        formatCam(Ruas, RuasJSON),
        TermOut = json([origem=Origem, destino=Destino, custo=Custo, ruas=RuasJSON]),
        formatRuas(T, Terms).

