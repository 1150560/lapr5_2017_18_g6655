%farmacia(farmaciaNome,latitude,longitude)

farmacia(armazem,41.177778,-8.607778).
farmacia(nau_vitoria,41.1741622,-8.5821195).
farmacia(barreiros,41.1605815,-8.6210144).
farmacia(central,41.1857224,-8.6537477).
farmacia(pinhais_da_foz,41.1555613,-8.6681147).
farmacia(pedra_verde,41.1933078,-8.6179276).
farmacia(cortes,41.1933045,-8.6109170).
farmacia(confianca,41.1932312,-8.6115012).
farmacia(costa_cabral,41.1720903,-8.5876506).
farmacia(cruz,41.1687099,-8.5907066).
farmacia(amial,41.1838825,-8.6135012).
farmacia(areosa,41.1813214,-8.5833519).
farmacia(antero_de_quental,41.1627813,-8.6110322).
farmacia(henriques,41.1447786,-8.6065682).
farmacia(saude,41.1653710,-8.5988215).
farmacia(alirio_de_barros,41.1639303,-8.6013361).
farmacia(silva_pereira,41.1640646,-8.6013830).
farmacia(cosme,41.1659656,-8.5975488).
farmacia(guerra,41.1622917,-8.6036776).
farmacia(martino,41.1613002,-8.6038802).
farmacia(vales,41.1861384,-8.5967432).
farmacia(central_corujeira,41.1592687,-8.5803088).
farmacia(hospital_sao_joao,41.1828290,-8.6016890).
farmacia(borralho,41.1559043,-8.5947325).
farmacia(louzada,41.1692780,-8.6088261).
farmacia(soeiro,41.1494042,-8.6005555).
farmacia(sao_dinis,41.1674205,-8.6180315).
farmacia(ramos,41.1661999,-8.6249140).
farmacia(alves,41.1668743,-8.6250454).
farmacia(mafalda,41.1615666,-8.6159409).
farmacia(silva_dias,41.1751795,-8.5654173).
farmacia(guimaraes,41.1671146,-8.6341355).
farmacia(cortes_pinto,41.1610881,-8.6082907).
farmacia(marques_de_mendonca,41.1749664,-8.6137437).
farmacia(alves_da_silva,41.1592476,-8.6356406).
farmacia(viamial,41.1792256,-8.6140360).
farmacia(sao_caetano,41.1691519,-8.5601841).
farmacia(magalhaes,41.1346218,-8.6318454).
farmacia(prelada,41.1693651,-8.6367504).
farmacia(oriental,41.1552360,-8.6077274).
farmacia(ferreira_da_silva,41.1805944,-8.6546745).
farmacia(fonte_da_moura,41.1624132,-8.6576170).
farmacia(central_da_liga,41.1491352,-8.6088019).
farmacia(correia,41.1583571,-8.6278354).
farmacia(boavista,41.1564064,-8.6203815).
farmacia(lapa,41.1600454,-8.6118548).
farmacia(campo,41.1551321,-8.6134212).
farmacia(sao_jeronimo,41.1544293,-8.2994831).
farmacia(serap_pinto,41.1663122,-8.6204918).
farmacia(queija_ferreira,41.1630983,-8.5900952).
farmacia(falcao,41.1651536,-8.6705991).

% ---------------------- TSP 2 -----------------

dist_farmacia(C1,C2,Dist):-
    farmacia(C1,Lat1,Lon1),
    farmacia(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).

degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in Km)
distance(Lat1, Lon1, Lat2, Lon2, Dis3):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1),
	Dis3 is Dis2/1000.

tsp2(Cam, Custo, Lista, Tempo):-
    get_time(Ti),
    bfs(armazem, armazem, Lista, Cam, Custo),
    get_time(Tf),
    Tempo is Tf -Ti.

greedy([Dest|LA], CustoAtual, CustoFinal, Cidade, Lista):-
    findall((Dist, City),
            (dist_farmacia(City, Dest, Dist),
            \+ member(City, [Dest|LA]), member(City, Lista)),
            L),
    sort(L, Res),
    member((CustoCid, Cidade), Res),
    CustoFinal is CustoCid + CustoAtual.


bfs(Orig, Dest, Lista, Cam, Custo):-
     bfs2(Orig, [Dest], Lista, 0 ,Cam, 0, Custo).

bfs2(Orig, X, Lista, N, Cam, CustoAtual, CustoFinal):-
    findall(C, (farmacia(C,_,_), C \== Orig, member(C, Lista)), L),
    length(L, N1),
    N == N1,
    X = [C1|_],
    % adiciona custo da origem
    dist_farmacia(Orig, C1, Dist),
    CustoFinal is CustoAtual + Dist,
    % coloca por ordem correta
    TempL = [Orig | X],
    reverse(TempL, Cam) ,!.

bfs2(Orig, [Dest|LA], Lista, N, Cam, CustoAtual, CustoFinal):-
    N1 is N+1,
    greedy([Dest|LA], CustoAtual, CA1, Cidade, Lista),
    bfs2(Orig, [Cidade | [Dest | LA]], Lista, N1, Cam, CA1, CustoFinal).


determina_rota(Cam, Custo, Lista):-
    bfs(armazem, armazem, Lista,  CA, _),
    remove_intersections(CA, [], Cam),
    custo(Cam, Custo),
    !.

% Calcula custo dos segmentos de uma lista
% Para quando restarem 2 cidades
custo([C1, C2], Custo):- dist_farmacia(C1, C2, Custo).

custo([C1, C2 | T], Custo):-
    custo([C2 | T], CA1),
    dist_farmacia(C1, C2, CA2),
    Custo is CA1 + CA2.

remove_intersections(Original, Intersections, Final):-
    % Procura segmentos
    segment(Original, (C1, C2)),
    segment(Original, (C3, C4)),

    % Segmentos nao podem ser iguas nem pode ser 2 segmentos formados por 3 pontos
    C1 \== C3, C1 \== C4,
    C2 \== C3, C2 \== C4,

    % Verifica se segmentos intersetam
    doIntersectCity(C1, C2, C3, C4),

    % Resolve interse��o
    ((\+member((C1,C3,C2,C4), Intersections), solve_intersection(C1, C3, Original, LAlterada))
    ;
    (replace(C3, C4, Original, LA), solve_intersection(C1, C3, LA, LAlterada))),
    !,

    remove_intersections(LAlterada, [(C1,C2,C3,C4) | Intersections], Final).

% Termina quando n�o encontrar interse��es
remove_intersections(L, _, L).


% Devolve um segmento
segment([C1, C2 | _], (C1, C2)).
segment([_|T], Res):- segment(T, Res).

solve_intersection(Ini, Fim, List, Result):-
    solve_intersection2(Ini, Fim, List, [], Result), !.

% Quando encontra ponto inicial resolve a interse��o apartir dele
solve_intersection2(Ini, Fim, [Ini | List], LA, Result):-
    solve_intersection3(Fim, List, [], Solved),
    append(LA, [Ini | Solved], Result).

% Procura por ponto inicial
solve_intersection2(Ini, Fim, [H | List], LA, Result):-
    append(LA, [H], LA1),
    solve_intersection2(Ini, Fim, List, LA1, Result).

% Inverte a lista desde o ponto inicial at� ao ponto final
solve_intersection3(C, [C | T], Reverse, Result):-
     append([C | Reverse], T, Result).

% Procura por ponto final
solve_intersection3(C, [H | T], Reverse, Result):-
    solve_intersection3(C, T, [H | Reverse], Result).

replace(_, _, [], []).
replace(X, Y, [X|T], [Y|T1]) :- replace(X, Y, T, T1), !.
replace(X, Y, [Y|T], [X|T1]) :- replace(X, Y, T, T1), !.
replace(X, Y, [H|T], [H|T1]) :- H \== X, replace(X, Y, T, T1), !.

doIntersectCity(C1, C2, C3, C4):-
    farmacia(C1, C1X, C1Y),
    farmacia(C2, C2X, C2Y),
    farmacia(C3, C3X, C3Y),
    farmacia(C4, C4X, C4Y),
    doIntersect((C1X, C1Y), (C2X, C2Y), (C3X, C3Y), (C4X, C4Y)).

% Given three colinear points p, q, r, the function checks if
% point q lies on line segment 'pr'
%onSegment(P, Q, R)
%
onSegment((PX,PY), (QX,QY), (RX,RY)):-
    QX =< max(PX,RX),
    QX >= min(PX,RX),
    QY =< max(PY,RY),
    QY >= min(PY,RY).

% To find orientation of ordered triplet (p, q, r).
% The function returns following values
% 0 --> p, q and r are colinear
% 1 --> Clockwise
% 2 --> Counterclockwise
orientation((PX,PY), (QX,QY), (RX,RY), Orientation):-
             Val is (QY - PY) * (RX - QX) - (QX - PX) * (RY - QY),

             (
                 Val == 0, !, Orientation is 0;
                 Val >0, !, Orientation is 1;
                 Orientation is 2
             ).
orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4):-
    orientation(P1, Q1, P2,O1),
    orientation(P1, Q1, Q2,O2),
    orientation(P2, Q2, P1,O3),
    orientation(P2, Q2, Q1,O4).


% The main function that returns true if line segment 'p1q1'
% and 'p2q2' intersect.
doIntersect(P1,Q1,P2,Q2):-
    % Find the four orientations needed for general and
    % special cases
    orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4),

    (
    % General case
    O1 \== O2 , O3 \== O4,!;

    % Special Cases
    % p1, q1 and p2 are colinear and p2 lies on segment p1q1
    O1 == 0, onSegment(P1, P2, Q1),!;
    % p1, q1 and p2 are colinear and q2 lies on segment p1q1
    O2 == 0, onSegment(P1, Q2, Q1),!;
    % p2, q2 and p1 are colinear and p1 lies on segment p2q2
    O3 == 0, onSegment(P2, P1, Q2),!;
     % p2, q2 and q1 are colinear and q1 lies on segment p2q2
    O4 == 0, onSegment(P2, Q1, Q2),!
    ).


