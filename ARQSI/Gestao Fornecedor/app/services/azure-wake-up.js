const schedule = require('node-schedule');
const axios = require('axios');

// Create instance
var instance = axios.create({
    baseURL: 'https://apigestaofornecedor.azurewebsites.net/api/',
});

function getAzure() {
    return instance({
        method: 'get',
        url: ''
    });
}

/**
 * Stops azure from entering sleep mode and stoping scheduled jobs
 */
module.exports.startService = () => {
    console.log('Azure Anti-Timeout online');

    schedule.scheduleJob('*/4 * * * *', function () {

        Promise.all([getAzure()])
    });
};