const schedule = require('node-schedule');
const config = require('../config/environment');
const OrdemEncomenda = require('../models/ordem-encomenda');
const apiRotas = require('../connections/api-rotas');
const apiFarmacias = require('../connections/api-farmacias');

const PlanoViagem = require('../models/planoViagem');

/**
 * Compilas as ordens de encomenda e planeia a viagem com base nas suas restricoes temporais 
 */
module.exports.startService = () => {
    console.log('Encomenda scheduler online');

    schedule.scheduleJob(config.encomendaScheduleTime, function () {
        OrdemEncomenda.find({ isEntregue: false }, function (err, ordens) {

            if (!err) {

                // Planeia viagem para manha e tarde
                let ordensManha = ordens.filter(o => o.isManha === true);
                let ordensTarde = ordens.filter(o => o.isManha === false);

                planeiaViagem(ordensManha, true);

                planeiaViagem(ordensTarde, false);
            }

        });

    });
};

function planeiaViagem(ordens, isManha) {
    if (ordens.length == 0) {
        return;
    }

    // Organiza ordens em funcção da farmacia
    let ordensComp = compilaOrdens(ordens);

    // Obter lista de farmacias
    let farmacias = [];
    ordensComp.forEach(o => {
        farmacias.push(o.nomeFarmacia);
    });

    // Obtem rota atraves de api de gestao de rotas
    apiRotas.requestRota(farmacias)
        .then(rota => {
            // Organiza ordens consuante a rota
            let ordensRota = [];

            rota.farmacias.forEach(nomeFarmacia => {
                ordensFarmacia = ordensComp.find(o => o.nomeFarmacia === nomeFarmacia);

                ordensRota.push(ordensFarmacia);
            });

            // Grava rota
            let planoViagem = new PlanoViagem();
            planoViagem.data = Date.now();
            planoViagem.data.setHours(0, 0, 0, 0);
            planoViagem.isManha = isManha;
            planoViagem.rota = rota.farmacias;
            planoViagem.ordensFarmacia = ordensComp;

            planoViagem.save(function (err, savedPlanoViagem) {
                if (!err) {

                    // Notifica Farmacias das entregas a ser realizadas
                    ordens.forEach(ordem => {
                        apiFarmacias.notificaFarmaciaASerEntregue(ordem);
                    });
                }
            });
        });
}

/**
 * Cria lista com todas as ordens organizadas por farmacia.
 * Ignora limitações temporais.
 * 
 * @param {*} ordens ordens a ser compiladas
 */
function compilaOrdens(ordens) {
    let ordensComp = [];

    ordens.forEach(o => {

        let exists = false;
        for (let i = 0; i < ordensComp.length; i++) {
            let oc = ordensComp[i];

            // Se a farmacia já estiver na lista entao adiciona a ordem
            if (o.nomeFarmacia === oc.nomeFarmacia) {
                exists = true;
                oc.ordens.push(o);
                break;
            }
        }

        // Se a farmacia ainda não estiver na lista, adiciona novo objeto da farmacia com a ordem já inserida
        if (!exists) {
            ordensComp.push({
                nomeFarmacia: o.nomeFarmacia,
                ordens: [o]
            });
        }

    });

    return ordensComp;
}

module.exports.compilaOrdens = compilaOrdens;
