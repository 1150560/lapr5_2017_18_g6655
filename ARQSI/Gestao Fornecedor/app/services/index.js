const encomendaScheduler = require('./encomenda-scheduler');
const azureScheduler = require('./azure-wake-up');
const config = require('../config/environment');

let services = [
    encomendaScheduler
];

if (config.env == 'production'){
    services.push(azureScheduler);
}

// Runs all services
module.exports.startServices = () => {
    services.forEach(service => {
        service.startService();
    });
};