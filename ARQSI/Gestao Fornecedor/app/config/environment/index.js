var _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
var all = {
    env: env,
    
    // Server port
    port: process.env.PORT || 3001,

    encomendaScheduleTime: '0 0 7 * * *',
    
    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://<dbuser>:<dbpassword>@<dbserver>/<dbname>'
    },

    secret: 'segredosecreto'
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`) || {}
);