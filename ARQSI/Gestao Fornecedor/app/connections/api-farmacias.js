const axios = require('axios');

// Create instance
var instance = axios.create({
    baseURL: 'https://apigestaofarmacias.azurewebsites.net/api/farmacia',
});

function postEncomendasPorConfirmar(ordem) {
    return instance({
        method: 'post',
        url: '/ordemEncomenda',
        data: {
            idOrdemEncomenda: ordem.idOrdem
        }
    });
}

module.exports.notificaFarmaciaASerEntregue = (ordem) => {
    return Promise.all([postEncomendasPorConfirmar(ordem)]);
};