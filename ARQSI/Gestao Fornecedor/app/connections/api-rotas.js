const axios = require('axios');

// Create instance
var instance = axios.create({
    baseURL: 'http://34.216.175.83',
});

function reqRota(farmacias) {
    return instance({
        method: 'post',
        url: '/rotas',
        data: {
            farmacias: farmacias
        }

    });
}

function reqRua(origem, destino) {
    return instance({
        method: 'post',
        url: '/ruas',
        data: {
            origem: origem,
            destino: destino
        }

    });
}

function reqRuaCaminho(farmacias) {
    return instance({
        method: 'post',
        url: '/ruasCaminho',
        data: {
            rota: farmacias
        }

    });
}

module.exports.requestRota = (farmacias) => {
    return Promise.all([reqRota(farmacias)])
        .then(function (results) {
            return results[0].data;
        });
};

module.exports.requestRua = (origem, destino) => {
    return Promise.all([reqRua(origem, destino)])
        .then(function (results) {
            return results[0].data;
        });
};

module.exports.requestRuaCaminho = (farmacias) => {
    return Promise.all([reqRuaCaminho(farmacias)])
        .then(function (results) {
            return results[0].data;
        });
};
