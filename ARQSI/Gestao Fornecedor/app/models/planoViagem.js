var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlanoViagemSchema = new Schema({
    rota: { type: [String], required: [true, "É necessário"] },
    ordensFarmacia: [{
        nomeFarmacia: { type: String, required: [true, "É necessário"] },
        ordens: [{ type: Schema.Types.ObjectId, ref: 'OrdemEncomenda', required: [true, "É necessário"] }],
    }],
    data: { type: Date, required: [true, "É necessário"] },
    isManha: { type: Boolean, required: [true, "É necessário"] }
});

module.exports = mongoose.model('PlanoViagens', PlanoViagemSchema);