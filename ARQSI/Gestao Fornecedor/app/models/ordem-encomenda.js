const mongoose = require('mongoose');
var MedicamentoSchema = require('./medicamento');

var Schema = mongoose.Schema;

var OrdemSchema = new Schema({
    idOrdem: { type: String, required: [true, "É necessário"] },
    medicamento: {type: MedicamentoSchema, required: [true, "É necessário"] },
    nomeFarmacia: { type: String, required: [true, "É necessário"] },
    quantidade: { type: Number, required: [true, "É necessário"], min: 1 },
    isManha: { type: Boolean, required: [true, "É necessário"] },
    isEntregue: { type: Boolean, default: false },
    data: { type: Date, default: Date.now },
});

module.exports = mongoose.model('OrdemEncomenda', OrdemSchema);