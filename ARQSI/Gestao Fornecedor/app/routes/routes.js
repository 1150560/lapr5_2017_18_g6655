const router = require('express').Router();
const ordemController = require('./controllers/ordem-controller');
const ruasController = require('./controllers/ruas-controller');

// ROUTES FOR OUR API
// =============================================================================
router.get('/', (req, res) => {
    res.status(200).json({ message: 'Hooray! Welcome to our api!' });
});

router.post('/ordem', ordemController.postOrdem);
router.put('/ordem/:idOrdem', ordemController.confirmaOrdem);
router.post('/rua', ruasController.postRua);
router.post('/ruaCaminho', ruasController.postRuaCaminho);
router.get('/ruaCaminho/Manha', ruasController.getRuaCaminhoManha);
router.get('/ruaCaminho/Tarde', ruasController.getRuaCaminhoTarde);

module.exports = router;