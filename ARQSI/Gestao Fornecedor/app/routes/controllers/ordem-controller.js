const express = require('express');
const OrdemEncomenda = require('../../models/ordem-encomenda');

module.exports.postOrdem = (req, res) => {
    ordem = new OrdemEncomenda();
    ordem.idOrdem = req.body.idOrdem;
    ordem.idMedicamento = req.body.idMedicamento;
    ordem.nomeFarmacia = req.body.nomeFarmacia;
    ordem.quantidade = req.body.quantidade;
    ordem.isManha = req.body.isManha;
    ordem.medicamento = req.body.medicamento;

    ordem.save(function (err, savedOrdem) {
        if (err) {
            return res.status(400).json({ err });
        }

        return res.status(201).json({ ordem: savedOrdem });
    });
};

module.exports.confirmaOrdem = (req, res) => {
    OrdemEncomenda.findOne({ idOrdem: req.params.idOrdem }, function (err, ordem) {
        if (err) {
            return res.status(400).json({ err });
        }
        if(!ordem){
            return res.status(404).json({message: "Id da ordem incorreto"});
        }
        ordem.isEntregue = true;

        ordem.save(function (err, savedOrdem) {
            if (err) {
                return res.status(400).json({ err });
            }

            return res.status(201).json({ ordem: savedOrdem });
        });
    });
};