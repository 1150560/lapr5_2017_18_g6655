const express = require('express');
const apiRotas = require('../../connections/api-rotas');
const PlanoViagens = require('../../models/planoViagem');
var moment = require('moment');

module.exports.postRua = (req, res) => {
    if (!req.body.origem || !req.body.destino) {
        return res.status(400).json({ message: "É necessário origem e destino" })
    }
    apiRotas.requestRua(req.body.origem, req.body.destino)
        .then(rua => {
            return res.status(200).json(rua);
        }).catch(erro => {
            return res.status(400).json({ message: "origem ou destino inválidos" });
        });
};


module.exports.postRuaCaminho = (req, res) => {
    if (!req.body.farmacias) {
        return res.status(400).json({ message: "É necessário farmacias" })
    }
    if (req.body.farmacias.length < 2) {
        return res.status(400).json({ message: "É necessário pelo menos 2 farmácias" })
    }
    apiRotas.requestRuaCaminho(req.body.farmacias)
        .then(caminho => {
            return res.status(200).json(caminho);
        }).catch(erro => {
            return res.status(400).json({ message: "Uma das farmácias é inválida" });
        });
};

module.exports.getRuaCaminhoManha = (req, res) => {
    getRuaCaminhoTempo(req, res, true);
};

module.exports.getRuaCaminhoTarde = (req, res) => {
    getRuaCaminhoTempo(req, res, false);
};

function getRuaCaminhoTempo(req, res, isManha) {
    let dataHoje = new Date().setHours(0, 0, 0, 0);
    PlanoViagens.findOne({ isManha: isManha, data: dataHoje }, function (err, plano) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!plano) {
                if (isManha) {
                    return res.status(400).json({ message: "Não tem planos para hoje de manha" });
                } else {
                    return res.status(400).json({ message: "Não tem planos para hoje de tarde" });
                }
            } else {
                apiRotas.requestRuaCaminho(plano.rota)
                    .then(caminho => {
                        return res.status(200).json(caminho);
                    }).catch(erro => {
                        return res.status(400).json({ message: "Uma das farmácias é inválida" });
                    });
            }
        }
    });
}