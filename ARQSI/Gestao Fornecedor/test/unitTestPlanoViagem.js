//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");

//Require the dev-dependencies
let chai = require('chai');
let should = chai.should();
let PlanoViagem = require('../app/models/planoViagem');
let assert = chai.assert;
let expect = chai.expect;

//Our parent block
describe('Unit Test of Plano Viagem', () => {

    it('should not allow to register a plano viagem without rotas', (done) => {
        let planoViagem = new PlanoViagem();
        let validator = planoViagem.validateSync();
        assert.nestedProperty(validator, 'errors.rota', 'Rota is required');

        planoViagem.rota = ["farmacia1", "farmacia2"];
        validator = planoViagem.validateSync();
        assert.notNestedProperty(validator, 'errors.rota', 'Allow to create plano viagem with rota');
        done();
    });

    it('should not allow to register a plano viagem without data', (done) => {
        let planoViagem = new PlanoViagem();
        let validator = planoViagem.validateSync();
        assert.nestedProperty(validator, 'errors.data', 'Data is required');

        planoViagem.data = "invalidData";
        validator = planoViagem.validateSync();
        assert.nestedProperty(validator, 'errors.data', 'Data should be type of date');

        planoViagem.data = Date.now();
        validator = planoViagem.validateSync();
        assert.notNestedProperty(validator, 'errors.data', 'Allow to create plano with data');
        done();
    });

    it('should not allow to register a plano viagem without isManha', (done) => {
        let planoViagem = new PlanoViagem();
        let validator = planoViagem.validateSync();
        assert.nestedProperty(validator, 'errors.isManha', 'isManha is required');

        planoViagem.isManha = true;
        validator = planoViagem.validateSync();
        assert.notNestedProperty(validator, 'errors.isManha', 'Allow to create plano with isManha(true)');

        planoViagem.isManha = false;
        validator = planoViagem.validateSync();
        assert.notNestedProperty(validator, 'errors.isManha', 'Allow to create plano with isManha(false)');
        done();
    });
    it('should compile ordens', (done) => {
        let compile = require('../app/services/encomenda-scheduler').compilaOrdens;
        let ordens = [
            { "nomeFarmacia": "teste1" },
            { "nomeFarmacia": "teste3" },
            { "nomeFarmacia": "teste4" },
            { "nomeFarmacia": "teste2" },
            { "nomeFarmacia": "teste1" },
            { "nomeFarmacia": "teste2" },
            { "nomeFarmacia": "teste2" },
            { "nomeFarmacia": "teste3" },
        ];
        let ordensCompiladas = compile(ordens);
        assert(ordensCompiladas.length == 4, 'ordensCompiladas.length should be 4');
        assert(ordensCompiladas[0].ordens.length == 2, 'ordensCompiladas teste1 should be 2');
        assert(ordensCompiladas[1].ordens.length == 2, 'ordensCompiladas teste3 should be 2');
        assert(ordensCompiladas[2].ordens.length == 1, 'ordensCompiladas teste4 should be 1');
        assert(ordensCompiladas[3].ordens.length == 3, 'ordensCompiladas teste2 should be 3');
        done();
    });

});