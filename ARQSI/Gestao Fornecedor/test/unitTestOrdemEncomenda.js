//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");

//Require the dev-dependencies
let chai = require('chai');
let should = chai.should();
let OrdemEncomenda = require('../app/models/ordem-encomenda');
let assert = chai.assert;
let expect = chai.expect;
let medicamento = {};

//Our parent block
describe('Unit Test of Ordem Encomenda', () => {
    before(() => {
        medicamento.nome = "MedNomeTest";
        medicamento.quantidade = "MedQuantidadeTest";
        medicamento.concentracao = "MedConcentracaoTest";
        medicamento.forma = "MedFormaTest";
        medicamento.nomeFarmaco = "MedFarmacoTest";
    });

    it('should not allow to register an ordem encomenda without medicamento or invalid medicamento', (done) => {
        let ordem = new OrdemEncomenda();
        let validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.medicamento', 'Medicamento is required');

        ordem.medicamento = {
            quantidade: "MedQuantidadeTest",
            concentracao: "MedConcentracaoTest",
            forma: "MedFormaTest",
            nomeFarmaco: "MedFarmacoTest",
        };
        validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.medicamento', 'Medicamento nome is required');

        ordem.medicamento = {
            nome: "MedNomeTest",
            concentracao: "MedConcentracaoTest",
            forma: "MedFormaTest",
            nomeFarmaco: "MedFarmacoTest",
        };
        validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.medicamento', 'Medicamento quantidade is required');

        ordem.medicamento = {
            nome: "MedNomeTest",
            quantidade: "MedQuantidadeTest",
            forma: "MedFormaTest",
            nomeFarmaco: "MedFarmacoTest",
        };
        validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.medicamento', 'Medicamento concentracao is required');

        ordem.medicamento = {
            nome: "MedNomeTest",
            quantidade: "MedQuantidadeTest",
            concentracao: "MedConcentracaoTest",
            nomeFarmaco: "MedFarmacoTest",
        };
        validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.medicamento', 'Medicamento forma is required');

        ordem.medicamento = {
            nome: "MedNomeTest",
            quantidade: "MedQuantidadeTest",
            concentracao: "MedConcentracaoTest",
            forma: "MedFormaTest",
        };
        validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.medicamento', 'Medicamento nomeFarmaco is required');

        ordem.medicamento = medicamento;
        validator = ordem.validateSync();
        assert.notNestedProperty(validator, 'errors.medicamento', 'Allow create ordem with valid medicamento');
        done();
    });

    it('should not allow to register an ordem encomenda without idOrdem', (done) => {
        let ordem = new OrdemEncomenda();
        let validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.idOrdem', 'IdOrdem is required');

        ordem.idOrdem = "1";
        validator = ordem.validateSync();
        assert.notNestedProperty(validator, 'errors.idOrdem', 'Allow to create ordem with idOrdem');
        done();
    });

    it('should not allow to register an ordem encomenda without nome of farmacia', (done) => {
        let ordem = new OrdemEncomenda();
        let validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.nomeFarmacia', 'nomeFarmacia is required');

        ordem.nomeFarmacia = "FarmaciaTest";
        validator = ordem.validateSync();
        assert.notNestedProperty(validator, 'errors.nomeFarmacia', 'Allow to create ordem with nomeFarmacia');
        done();
    });

    it('should not allow to register an ordem encomenda without quantidade', (done) => {
        let ordem = new OrdemEncomenda();
        let validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.quantidade', 'Quantidade is required');

        ordem.nomeFarmacia = "FarmaciaTest";
        validator = ordem.validateSync();
        assert.notNestedProperty(validator, 'errors.nomeFarmacia', 'Allow to create ordem with Quantidade');
        done();
    });

    it('should not allow to register an ordem encomenda without isManha', (done) => {
        let ordem = new OrdemEncomenda();
        let validator = ordem.validateSync();
        assert.nestedProperty(validator, 'errors.isManha', 'IdOrdem is required');

        ordem.isManha = true;
        validator = ordem.validateSync();
        assert.notNestedProperty(validator, 'errors.isManha', 'Allow to create ordem with isManha');

        ordem.isManha = false;
        validator = ordem.validateSync();
        assert.notNestedProperty(validator, 'errors.isManha', 'Allow to create ordem with isManha');
        done();
    });

    it('should register an ordem encomenda with isEntregue (by default is false)', (done) => {
        let ordem = new OrdemEncomenda();
        assert.isNotTrue(ordem.isEncomendado, 'Is encomendando should be false');
        done();
    });

    it('should register an ordem encomenda with date (by default is the current date)', (done) => {
        let ordem = new OrdemEncomenda();
        assert.equal((new Date(ordem.data)).getDay(), (new Date(Date.now())).getDay(), 'Should create an ordem with current date');
        done();
    });
});