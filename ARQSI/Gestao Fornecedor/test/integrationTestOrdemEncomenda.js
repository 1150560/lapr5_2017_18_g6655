process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

let app;
let request;
let OrdemEncomenda = require('../app/models/ordem-encomenda');

describe('Integration Testing', () => {

    before(() => {
        app = require('../app').app;
        request = chai.request(app);
    });



    after(() => {
        require('../app').stop();
    });
    describe('/Post Ordem Encomenda', () => {

        beforeEach((done) => {
            OrdemEncomenda.remove({}, (err) => { done() });
        });

        it('It should not post a ordem: Missing (idOrdem, nomeFarmacia, quantidade, isManha, medicamento)', (done) => {
            request
                .post('/api/ordem').send({})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: idOrdem: É necessário, nomeFarmacia: É necessário, quantidade: É necessário, isManha: É necessário, medicamento: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (nomeFarmacia, quantidade, isManha, medicamento) ', (done) => {
            request
                .post('/api/ordem').send({ "idOrdem": "5a3820230c66b00b28654e33" })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: nomeFarmacia: É necessário, quantidade: É necessário, isManha: É necessário, medicamento: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (quantidade, isManha, medicamento)', (done) => {
            request
                .post('/api/ordem').send({ "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome" })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: quantidade: É necessário, isManha: É necessário, medicamento: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (isManha, medicamento)', (done) => {
            request
                .post('/api/ordem').send({ "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10 })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: isManha: É necessário, medicamento: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (medicamento)', (done) => {
            request
                .post('/api/ordem').send({ "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: medicamento: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (forma, concentracao, quantidade, nome, nomeFarmaco)', (done) => {
            request
                .post('/api/ordem').send({ "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true, "medicamento": {} })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: medicamento.nomeFarmaco: É necessário, medicamento.forma: É necessário, medicamento.concentracao: É necessário, medicamento.quantidade: É necessário, medicamento.nome: É necessário, medicamento: Validation failed: nomeFarmaco: É necessário, forma: É necessário, concentracao: É necessário, quantidade: É necessário, nome: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (concentracao, quantidade, nome, nomeFarmaco)', (done) => {
            request
                .post('/api/ordem').send({
                    "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true,
                    "medicamento": { "forma": "comprimidos" }
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: medicamento.nomeFarmaco: É necessário, medicamento.concentracao: É necessário, medicamento.quantidade: É necessário, medicamento.nome: É necessário, medicamento: Validation failed: nomeFarmaco: É necessário, concentracao: É necessário, quantidade: É necessário, nome: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (quantidade, nome, nomeFarmaco)', (done) => {
            request
                .post('/api/ordem').send({
                    "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true,
                    "medicamento": { "forma": "comprimidos", "concentracao": "500mg" }
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: medicamento.nomeFarmaco: É necessário, medicamento.quantidade: É necessário, medicamento.nome: É necessário, medicamento: Validation failed: nomeFarmaco: É necessário, quantidade: É necessário, nome: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (nome, nomeFarmaco)', (done) => {
            request
                .post('/api/ordem').send({
                    "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true,
                    "medicamento": { "forma": "comprimidos", "concentracao": "500mg", "quantidade": "10" }
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: medicamento.nomeFarmaco: É necessário, medicamento.nome: É necessário, medicamento: Validation failed: nomeFarmaco: É necessário, nome: É necessário');
                    done();
                });
        });
        it('It should not post a ordem: Missing (nomeFarmaco)', (done) => {
            request
                .post('/api/ordem').send({
                    "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true,
                    "medicamento": { "forma": "comprimidos", "concentracao": "500mg", "quantidade": "10", "nome": "Aspirina" }
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('err');
                    res.body.err.should.have.property('message');
                    res.body.err.message.should.be.eql('OrdemEncomenda validation failed: medicamento.nomeFarmaco: É necessário, medicamento: Validation failed: nomeFarmaco: É necessário');
                    done();
                });
        });
        it('It should post a ordem', (done) => {
            request
                .post('/api/ordem').send({
                    "idOrdem": "5a3820230c66b00b28654e33", "nomeFarmacia": "nome", "quantidade": 10, "isManha": true,
                    "medicamento": { "forma": "comprimidos", "concentracao": "500mg", "quantidade": "10", "nome": "Aspirina", "nomeFarmaco": "Paracetamol" }
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('ordem');
                    done();
                });
        });
    });
    describe('/Put Ordem Encomenda', () => {
        let ordem;
        beforeEach((done) => {
            OrdemEncomenda.remove({}, (err) => { });
            ordem = new OrdemEncomenda();

            ordem.nomeFarmacia = "FarmaciaTeste";
            ordem.quantidade = 10;
            ordem.isManha = true;
            ordem.medicamento = {};
            ordem.idOrdem = "5a3820230c66b00b28654e33";
            ordem.medicamento.forma = "comprimidos";
            ordem.medicamento.concentracao = "300mg";
            ordem.medicamento.quantidade = "100 ml";
            ordem.medicamento.nome = "Brufen";
            ordem.medicamento.nomeFarmaco = "Paracetamol";
            OrdemEncomenda.create(ordem, (err, o) => {
                if (err) {
                    console.log(err);
                    done();
                } else {
                    ordem._id = o._id;
                    done();
                }
            })
        });

        it('It should put a ordem', (done) => {
            request
                .put('/api/ordem/' + ordem.idOrdem).send({})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('ordem');
                    done();
                });
        });
        it('It should not put a ordem', (done) => {
            request
                .put('/api/ordem/idDaOrdemIncorreto').send({})
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Id da ordem incorreto');

                    done();
                });
        });
    });

});