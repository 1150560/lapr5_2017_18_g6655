// Needed packages
const express = require('express');
const config = require('./app/config/environment');
const services = require('./app/services');

const app = express();  // Define app with express

// Configure app
require('./app/config')(app);

// Register routes
// all routes will be prefixed with /api
app.use('/api', require('./app/routes/routes'));

// START THE SERVER
// =============================================================================
let server = app.listen(config.port, () => {
    console.log('Express server listening on port %d, in %s mode', config.port, config.env);

    services.startServices();
});

// For testing
module.exports.stop = () => {
    require('./app/config').closeMongoose();
    server.close();
};

module.exports.app = app;