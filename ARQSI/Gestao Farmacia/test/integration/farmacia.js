process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();

let app;
let request;
let Farmacia = require('../../models/farmacia');

describe('Integration Testing Farmacia', () => {

    let token;

    before(() => {
        app = require('../../app').app;
        request = chai.request(app);
    });


    after(() => {
        require('../../app').stop();
    });

    describe('/Get Stocks', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                Farmacia.create(farmacia, (err, f) => {
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should get stocks', (done) => {
            request
                .get('/api/farmacia/stocks').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.length(1);
                    done();
                });
        });
    });
    describe('/Get Apresentacacao/:id/medicamentos', () => {
        it('It should get 1 medicamento', (done) => {
            request
                .get('/api/farmacia/apresentacao/1/medicamentos').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.length(1);
                    done();
                });
        });
        it('It should get 404', (done) => {
            request
                .get('/api/farmacia/apresentacao/16/medicamentos').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Request failed with status code 404');
                    done();
                });
        });
    });
    describe('/Get Stock/medicamentos', () => {
        it('It should get 17 medicamentos', (done) => {
            request
                .get('/api/farmacia/stock/medicamentos').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.length(17);
                    done();
                });
        });
    });
    describe('/Get Stock/:id', () => {
        let farmacia;
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should get 1 stock', (done) => {
            request
                .get('/api/farmacia/stocks/' + farmacia.stocks[0]._id).set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property("quantidadeAtual");
                    res.body.should.have.property("ordensEncomenda");
                    res.body.ordensEncomenda.should.have.length(0);
                    done();
                });
        });
        it('It should get 1 stock', (done) => {
            request
                .get('/api/farmacia/stocks/idIncorreto').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.eql("O id do stock está incorreto");
                    done();
                });
        });
    });
    describe('/Get ordensEncomenda/PorEntregar', () => {
        let farmacia;
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should get 0 ordensEncomendaPorEntregar', (done) => {
            request
                .get('/api/farmacia/ordensEncomenda/PorEntregar').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.length(0);
                    done();
                });
        });
    });
    describe('/Get ordensEncomenda/PorConfirmar', () => {
        let farmacia;
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should get 0 ordensEncomendaPorConfirmar', (done) => {
            request
                .get('/api/farmacia/ordensEncomenda/PorConfirmar').set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.length(0);
                    done();
                });
        });
    });
    describe('/Post Farmacia', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                Farmacia.create(farmacia, (err, f) => {
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not post a farmacia (No Token)', (done) => {
            request
                .post('/api/farmacia').send({})
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('No token provived.');
                    done();
                });
        });
        it('It should not post a farmacia (nome, isManha, farmaceuticos)', (done) => {
            request
                .post('/api/farmacia').send({}).set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Farmacia validation failed: nome: É necessário, isManha: É necessário, farmaceuticos: É necessário');
                    done();
                });
        });
        it('It should post a farmacia', (done) => {
            request
                .post('/api/farmacia').send({
                    "nome": "nomeTeste", "isManha": true, "farmaceuticos":
                        [{ "emailGdr": "farmaceutico1@gmail.com", "email": "email@teste.com" }]
                }).set("x-acess-token", token)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Farmácia criada');
                    done();
                });
        });
    });
    describe('/Post Stock', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not post a stock (quantidade atual < quantidade minima)', (done) => {
            request
                .post('/api/farmacia/stock').send({
                    "quantidadeAtual": 3, "quantidadeMinima": 5, "quantidadeLote": 8, "idMedicamento": 3
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Quantidade atual tem de ser maior ou igual à quantidade minima');
                    done();
                });
        });
        it('It should not post a stock (stock existe)', (done) => {
            request
                .post('/api/farmacia/stock').send({
                    "quantidadeAtual": 8, "quantidadeMinima": 5, "quantidadeLote": 8, "idMedicamento": 1
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Já existe um stock para este medicamento');
                    done();
                });
        });
        it('It should not post a stock (idMedicamento invalido)', (done) => {
            request
                .post('/api/farmacia/stock').send({
                    "quantidadeAtual": 8, "quantidadeMinima": 5, "quantidadeLote": 8, "idMedicamento": 40
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Id do medicamento inválido');
                    done();
                });
        });
        it('It should post a stock', (done) => {
            request
                .post('/api/farmacia/stock').send({
                    "quantidadeAtual": 8, "quantidadeMinima": 5, "quantidadeLote": 8, "idMedicamento": 2
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Stock criado com sucesso');
                    done();
                });
        });

    });
    describe('/Put Stock', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not put a stock (quantidade minima > quantidade atual)', (done) => {
            request
                .put('/api/farmacia/stock/' + farmacia.stocks[0]._id).send({
                    "quantidadeMinima": 15, "quantidadeLote": 35
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Quantidade mínima não pode ser maior que a atual');
                    done();
                });
        });
        it('It should not put a stock (idStock incorreto)', (done) => {
            request
                .put('/api/farmacia/stock/idIncorreto').send({
                    "quantidadeMinima": 15, "quantidadeLote": 35
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Id do stock incorreto');
                    done();
                });
        });
        it('It should not put a stock (idStock incorreto)', (done) => {
            request
                .put('/api/farmacia/stock/' + farmacia.stocks[0]._id).send({
                    "quantidadeMinima": 5, "quantidadeLote": 35
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Stock editado com sucesso');
                    done();
                });
        });
    });
    describe('/Aviar Medicamento', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not aviar a stock (quantidade < 0)', (done) => {
            request
                .put('/api/farmacia/medicamento/1/aviar').send({
                    "quantidade": -3
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Quantidade tem de ser positiva');
                    done();
                });
        });
        it('It should not aviar a stock (quantidade > quantidadeAtual)', (done) => {
            request
                .put('/api/farmacia/medicamento/1/aviar').send({
                    "quantidade": 13
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Quantidade não pode ser superior à quantidade atual (12)');
                    done();
                });
        });
        it('It should not aviar a stock (idStock incorreto)', (done) => {
            request
                .put('/api/farmacia/medicamento/2/aviar').send({
                    "quantidade": 13
                }).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Id do stock incorreto');
                    done();
                });
        });
    });
    describe('/Confirmar ordemEncomenda', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12;
                farmacia.stocks[0].quantidadeMinima = 10;
                farmacia.stocks[0].quantidadeLote = 30;
                farmacia.stocks[0].idMedicamento = 1;
                farmacia.stocks[0].isEncomendado = false;
                farmacia.stocks[0].ordensEncomenda = [];
                farmacia.stocks[0].ordensEncomenda[0] = {};
                farmacia.stocks[0].ordensEncomenda[0].isEntregue = true;
                farmacia.stocks[0].ordensEncomenda[0].isManha = true;
                farmacia.stocks[0].ordensEncomenda[0].nomeFarmacia = "Farmacia Teste";
                farmacia.stocks[0].ordensEncomenda[0].quantidade = 15;
                farmacia.stocks[0].ordensEncomenda[0].medicamento = {};
                farmacia.stocks[0].ordensEncomenda[0].medicamento.nome = "Medicamento 1";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.forma = "comprimidos";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.concentracao = "500 mg";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.quantidade = "2";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.nomeFarmaco = "Farmaco 1";
                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    farmacia.stocks[0].ordensEncomenda[0]._id = f.stocks[0].ordensEncomenda[0]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not confirm ordemEncomenda (idOrdemEncomenda incorreto)', (done) => {
            request
                .put('/api/farmacia/ordemEncomenda/idIncorreto/confirmar').send({})
                .set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Id da ordem de encomenda incorreto');
                    done();
                });
        });
        it('It should not confirm ordemEncomenda (ordem isEntregue)', (done) => {
            request
                .put('/api/farmacia/ordemEncomenda/' + farmacia.stocks[0].ordensEncomenda[0]._id + '/confirmar').send({})
                .set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Esta ordem já foi confirmada');
                    done();
                });
        });
    });
    describe('/Post ordemEncomendaPorConfirmar', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12;
                farmacia.stocks[0].quantidadeMinima = 10;
                farmacia.stocks[0].quantidadeLote = 30;
                farmacia.stocks[0].idMedicamento = 1;
                farmacia.stocks[0].isEncomendado = false;
                farmacia.stocks[0].ordensEncomenda = [];
                farmacia.stocks[0].ordensEncomenda[0] = {};
                farmacia.stocks[0].ordensEncomenda[0].isEntregue = true;
                farmacia.stocks[0].ordensEncomenda[0].isManha = true;
                farmacia.stocks[0].ordensEncomenda[0].nomeFarmacia = "Farmacia Teste";
                farmacia.stocks[0].ordensEncomenda[0].quantidade = 15;
                farmacia.stocks[0].ordensEncomenda[0].medicamento = {};
                farmacia.stocks[0].ordensEncomenda[0].medicamento.nome = "Medicamento 1";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.forma = "comprimidos";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.concentracao = "500 mg";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.quantidade = "2";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.nomeFarmaco = "Farmaco 1";
                farmacia.stocks[0].ordensEncomenda[1] = {};
                farmacia.stocks[0].ordensEncomenda[1].isEntregue = false;
                farmacia.stocks[0].ordensEncomenda[1].isManha = true;
                farmacia.stocks[0].ordensEncomenda[1].nomeFarmacia = "Farmacia Teste";
                farmacia.stocks[0].ordensEncomenda[1].quantidade = 20;
                farmacia.stocks[0].ordensEncomenda[1].medicamento = {};
                farmacia.stocks[0].ordensEncomenda[1].medicamento.nome = "Medicamento 1";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.forma = "comprimidos";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.concentracao = "500 mg";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.quantidade = "2";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.nomeFarmaco = "Farmaco 1";

                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    farmacia.stocks[0].ordensEncomenda[0]._id = f.stocks[0].ordensEncomenda[0]._id;
                    farmacia.stocks[0].ordensEncomenda[1]._id = f.stocks[0].ordensEncomenda[1]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not post ordemEncomenda (idOrdemEncomenda incorreto)', (done) => {
            request
                .post('/api/farmacia/ordemEncomenda').send({ "idOrdemEncomenda": farmacia.stocks[0]._id })
                .set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('O id da farmácia está incorreto');
                    done();
                });
        });
        it('It should not post ordemEncomenda (ordem isEntregue)', (done) => {
            request
                .post('/api/farmacia/ordemEncomenda').send({ "idOrdemEncomenda": farmacia.stocks[0].ordensEncomenda[0]._id })
                .set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Esta ordem já foi confirmada');
                    done();
                });
        });
        it('It should post ordemEncomenda', (done) => {
            request
                .post('/api/farmacia/ordemEncomenda').send({ "idOrdemEncomenda": farmacia.stocks[0].ordensEncomenda[1]._id })
                .set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Ordem por confirmar adicionada com sucesso');
                    done();
                });
        });
    });
    describe('/Delete Stock', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12
                farmacia.stocks[0].quantidadeMinima = 10
                farmacia.stocks[0].quantidadeLote = 30
                farmacia.stocks[0].idMedicamento = 1
                farmacia.stocks[0].isEncomendado = false
                farmacia.stocks[1] = {};
                farmacia.stocks[1].quantidadeAtual = 0
                farmacia.stocks[1].quantidadeMinima = 0
                farmacia.stocks[1].quantidadeLote = 0
                farmacia.stocks[1].idMedicamento = 2
                farmacia.stocks[1].isEncomendado = false
                Farmacia.create(farmacia, (err, f) => {
                    farmacia._id = f._id;
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    farmacia.stocks[1]._id = f.stocks[1]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should not delete a stock (quantidadeAtual > 0)', (done) => {
            request
                .delete('/api/farmacia/stock/' + farmacia.stocks[0]._id).send({}).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Não é possível apagar este stock.');
                    done();
                });
        });
        it('It should not delete a stock (idStock incorreto)', (done) => {
            request
                .delete('/api/farmacia/stock/' + farmacia._id).send({}).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Id do stock incorreto');
                    done();
                });
        });
        it('It should delete a stock', (done) => {
            request
                .delete('/api/farmacia/stock/' + farmacia.stocks[1]._id).send({}).set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Stock elminado com sucesso.');
                    done();
                });

        });
    });
    describe.only('/Get stocksChart', () => {
        beforeEach((done) => {
            Farmacia.remove({}, (err) => {
                farmacia = new Farmacia();
                farmacia.nome = "Farmacia Teste";
                farmacia.isManha = true;
                farmacia.farmaceuticos = [];
                farmacia.farmaceuticos[0] = {};
                farmacia.farmaceuticos[0].emailGdr = "farmaceutico1@gmail.com";
                farmacia.farmaceuticos[0].email = "farmaceutico@farmaciateste.com";
                farmacia.stocks = [];
                farmacia.stocks[0] = {};
                farmacia.stocks[0].quantidadeAtual = 12;
                farmacia.stocks[0].quantidadeMinima = 10;
                farmacia.stocks[0].quantidadeLote = 30;
                farmacia.stocks[0].idMedicamento = 1;
                farmacia.stocks[0].isEncomendado = false;
                farmacia.stocks[0].ordensEncomenda = [];
                farmacia.stocks[0].ordensEncomenda[0] = {};
                farmacia.stocks[0].ordensEncomenda[0].isEntregue = true;
                farmacia.stocks[0].ordensEncomenda[0].isManha = true;
                farmacia.stocks[0].ordensEncomenda[0].nomeFarmacia = "Farmacia Teste";
                farmacia.stocks[0].ordensEncomenda[0].quantidade = 15;
                farmacia.stocks[0].ordensEncomenda[0].medicamento = {};
                farmacia.stocks[0].ordensEncomenda[0].medicamento.nome = "Medicamento 1";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.forma = "comprimidos";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.concentracao = "500 mg";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.quantidade = "2";
                farmacia.stocks[0].ordensEncomenda[0].medicamento.nomeFarmaco = "Farmaco 1";
                farmacia.stocks[0].ordensEncomenda[1] = {};
                farmacia.stocks[0].ordensEncomenda[1].isEntregue = false;
                farmacia.stocks[0].ordensEncomenda[1].isManha = true;
                farmacia.stocks[0].ordensEncomenda[1].nomeFarmacia = "Farmacia Teste";
                farmacia.stocks[0].ordensEncomenda[1].quantidade = 20;
                farmacia.stocks[0].ordensEncomenda[1].medicamento = {};
                farmacia.stocks[0].ordensEncomenda[1].medicamento.nome = "Medicamento 1";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.forma = "comprimidos";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.concentracao = "500 mg";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.quantidade = "2";
                farmacia.stocks[0].ordensEncomenda[1].medicamento.nomeFarmaco = "Farmaco 1";

                Farmacia.create(farmacia, (err, f) => {
                    farmacia.stocks[0]._id = f.stocks[0]._id;
                    farmacia.stocks[0].ordensEncomenda[0]._id = f.stocks[0].ordensEncomenda[0]._id;
                    farmacia.stocks[0].ordensEncomenda[1]._id = f.stocks[0].ordensEncomenda[1]._id;
                    request.post('/api/login').send({
                        "email": "farmaceutico@farmaciateste.com",
                        "password": "teste"
                    }).set('Content-Type', "application/json")
                        .end((err, res) => {
                            token = res.body.token;
                            done();
                        });
                });
            });
        });
        it('It should get stocksChart', (done) => {
            request
                .get('/api/farmacia/stocksChart')
                .set('x-acess-token', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property("stocks");
                    res.body.stocks.length.should.be.eql(1);
                    done();
                });
        });
        it('It should not get stocksChart', (done) => {
            request
                .get('/api/farmacia/stocksChart')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    res.body.should.have.property("message");
                    res.body.message.should.be.eql("No token provived.");
                    done();
                });
        });
    });
});