const assert = require('chai').assert
const Farmacia = require('../../models/farmacia');

describe('Unit::Farmaceutico', () => {

    it('it should  require email field', (done) => {
        let farmacia = new Farmacia({
            farmaceuticos: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'farmaceuticos.0.email', 'email field is always required');

        farmacia.farmaceuticos[0].email = 'email@email.com';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'farmaceuticos.0.email');

        done();
    });

    it('it should  require emailGdr field', (done) => {
        let farmacia = new Farmacia({
            farmaceuticos: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'farmaceuticos.0.emailGdr', 'emailGdr field is always required');

        farmacia.farmaceuticos[0].emailGdr = 'email@email.com';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'farmaceuticos.0.emailGdr');

        done();
    });
});