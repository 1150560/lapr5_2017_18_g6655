const assert = require('chai').assert
const Farmacia = require('../../models/farmacia');

describe('Unit::Ordem Encomenda', () => {

    it('it should  require isManha field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.isManha', 'isManha field is always required');

        farmacia.ordensPorConfirmar[0].isManha = true;
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.isManha');

        done();
    });

    it('it should  require nomeFarmacia field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.nomeFarmacia', 'nomeFarmacia field is always required');

        farmacia.ordensPorConfirmar[0].nomeFarmacia = 'Farmacia';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.nomeFarmacia');

        done();
    });

    it('it should have a valid quantidade', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.quantidade', 'quantidade field is always required');

        farmacia.ordensPorConfirmar[0].quantidade = 0;
        validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.quantidade', 'quantidade field must be at least 1');

        farmacia.ordensPorConfirmar[0].quantidade = 1;
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.quantidade');

        done();
    });
});