const assert = require('chai').assert
const Farmacia = require('../../models/farmacia');

describe('Unit::Stock', () => {

    it('it should have a valid quantidade minima', (done) => {
        let farmacia = new Farmacia({
            stocks: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.quantidadeMinima', 'quantidadeMinima field is always required');

        farmacia.stocks[0].quantidadeMinima = -1;
        validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.quantidadeMinima', 'quantidadeMinima field must be at least 0');

        farmacia.stocks[0].quantidadeMinima = 1;
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'stocks.0.quantidadeMinima');

        done();
    });

    it('it should have a valid quantidade atual', (done) => {
        let farmacia = new Farmacia({
            stocks: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.quantidadeAtual', 'quantidadeAtual field is always required');

        farmacia.stocks[0].quantidadeAtual = -1;
        validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.quantidadeAtual', 'quantidadeAtual field must be at least 0');

        farmacia.stocks[0].quantidadeAtual = 1;
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'stocks.0.quantidadeAtual');

        done();
    });

    it('it should have a valid quantidade lote', (done) => {
        let farmacia = new Farmacia({
            stocks: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.quantidadeLote', 'quantidadeLote field is always required');

        farmacia.stocks[0].quantidadeLote = -1;
        validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.quantidadeLote', 'quantidadeLote field must be at least 0');

        farmacia.stocks[0].quantidadeLote = 1;
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'stocks.0.quantidadeLote');

        done();
    });

    it('it should  require idMedicamento field', (done) => {
        let farmacia = new Farmacia({
            stocks: [{}]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'stocks.0.idMedicamento', 'idMedicamento field is always required');

        farmacia.stocks[0].idMedicamento = 'ID';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'stocks.0.idMedicamento');

        done();
    });
});