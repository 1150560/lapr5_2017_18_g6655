const assert = require('chai').assert
const Farmacia = require('../../models/farmacia');

describe('Unit::Farmacia', () => {

    it('it should require nome field', (done) => {
        let farmacia = new Farmacia();

        let validator = farmacia.validateSync();
        assert.nestedProperty(validator, 'errors.nome', 'nome field is always required');

        farmacia.nome = 'Test Name';
        validator = farmacia.validateSync();
        assert.notNestedProperty(validator, 'errors.nome');

        done();
    });

    it('it should require isManha field', (done) => {
        let farmacia = new Farmacia();

        let validator = farmacia.validateSync();
        assert.nestedProperty(validator, 'errors.isManha', 'isManha field is always required');

        farmacia.isManha = true;
        validator = farmacia.validateSync();
        assert.notNestedProperty(validator, 'errors.isManha');


        done();
    });
});