const assert = require('chai').assert
const Farmacia = require('../../models/farmacia');

describe('Unit::Medicamento', () => {

    it('it should  require nome field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{ medicamento: {} }]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.medicamento.nome', 'nome field is always required');

        farmacia.ordensPorConfirmar[0].medicamento.nome = 'nome';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.medicamento.nome');

        done();
    });

    it('it should  require quantidade field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{ medicamento: {} }]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.medicamento.quantidade', 'quantidade field is always required');

        farmacia.ordensPorConfirmar[0].medicamento.quantidade = '2';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.medicamento.quantidade');

        done();
    });

    it('it should  require concentracao field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{ medicamento: {} }]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.medicamento.concentracao', 'concentracao field is always required');

        farmacia.ordensPorConfirmar[0].medicamento.concentracao = '100 mg';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.medicamento.concentracao');

        done();
    });

    it('it should  require forma field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{ medicamento: {} }]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.medicamento.forma', 'forma field is always required');

        farmacia.ordensPorConfirmar[0].medicamento.forma = 'Comprimido';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.medicamento.forma');

        done();
    });

    it('it should  require nomeFarmaco field', (done) => {
        let farmacia = new Farmacia({
            ordensPorConfirmar: [{ medicamento: {} }]
        });

        let validator = farmacia.validateSync();
        assert.property(validator.errors, 'ordensPorConfirmar.0.medicamento.nomeFarmaco', 'nomeFarmaco field is always required');

        farmacia.ordensPorConfirmar[0].medicamento.nomeFarmaco = 'Ibuprofeno';
        validator = farmacia.validateSync();
        assert.notProperty(validator.errors, 'ordensPorConfirmar.0.medicamento.nomeFarmaco');

        done();
    });
});