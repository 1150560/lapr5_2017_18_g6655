// grab our packages
var gulp = require('gulp');
var gls = require('gulp-live-server');
var jshint = require('gulp-jshint');

let paths = {
    src: ['app.js', 'app/**/*.js']
};

let server;

// define the default task and add the serve task to it
gulp.task('default', ['serve']);

// configure the jshint task
gulp.task('jshint', function () {
    return gulp.src(paths.src)
        .pipe(jshint({ esversion: 6 }))
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('restart-server', ['jshint'], function () {
    if (server) {
        server.start.bind(server)();
    }
});

gulp.task('serve', ['jshint'], function () {
    //1. run your script as a server 
    server = gls.new('app.js');
    server.start();

    //use gulp.watch to trigger server actions(notify, start or stop) 
    gulp.watch(paths.src, ['restart-server']);
});