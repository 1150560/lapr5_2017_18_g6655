const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const morgan = require('morgan');
var interceptor = require('../interceptor');
var decrypt = require('../auth/decrypt');
var config = require('./environment');

module.exports = (app) => {
    app.use(cors());

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    if (config.env != 'test') {
        app.use(interceptor);
        app.use(decrypt);
    }
    if (config.env == 'production') {
        app.use(morgan('common'));
    } else {
        app.use(morgan('dev'));
    }

    mongoose.connect(config.mongoose.uri, { useMongoClient: true }); // Connect to database
    mongoose.Promise = global.Promise;

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
        console.log("Connected to database!");
    });
};

// For testing
module.exports.closeMongoose = () => {
    mongoose.connection.close();
};
