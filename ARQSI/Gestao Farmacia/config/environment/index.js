var _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
var all = {
    env: env,
    
    // Server port
    port: process.env.PORT || 3000,

    tokenExpire: 3600, // 1 Hour

    encomendaScheduleTime: '0 1 * * *',

    key: 'arqsi19823',

    algorithmEncryption: 'aes256',
    
    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://<dbuser>:<dbpassword>@<dbserver>/<dbname>'
    },

    secret: 'secretFarmaceutico'
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`) || {}
);