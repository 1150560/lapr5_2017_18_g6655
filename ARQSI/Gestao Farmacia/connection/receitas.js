var instance = require('../auth/axios_config');

function getMedicamento(idMedicamento) {
    return instance({
        method: 'get',
        url: '/medicamentos/' + idMedicamento
    });
};

getMedicamentoInfo = (idMedicamento) => {
    return Promise.all([getMedicamento(idMedicamento)])
        .then(function (results) {
            return results[0].data;
        }).catch(function (error) {
            return res.status(error.response.status).send({ erro: error.message });
        });
};

module.exports = {
    getMedicamentoInfo: getMedicamentoInfo
}