const axios = require('axios');

// Create instance
var instance = axios.create({
    baseURL: 'https://apigestaofornecedor.azurewebsites.net/api',
});

function postOrdem(ordem) {
    return instance({
        method: 'post',
        url: '/ordem',
        data: {
            idOrdem: ordem._id,
            idMedicamento: ordem.idMedicamento,
            nomeFarmacia: ordem.nomeFarmacia,
            quantidade: ordem.quantidade,
            isManha: ordem.isManha,
            medicamento: ordem.medicamento
        }

    });
};

function confirmOrdem(ordem) {
    return instance({
        method: 'put',
        url: '/ordem/' + ordem._id,
    });
};

module.exports.createOrdem = (ordem) => {
    return Promise.all([postOrdem(ordem)])
        .then(function (results) {
            return results[0].data;
        });
};

module.exports.confirmaOrdem = (ordem) => {
    return Promise.all([confirmOrdem(ordem)])
        .then(function (results) {
            return results[0].data;
        });
};