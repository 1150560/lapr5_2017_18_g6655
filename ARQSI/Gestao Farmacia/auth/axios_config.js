var axios = require('axios');
var encryption = require('./encryption');

var instance = axios.create({
	baseURL: 'http://apigestaoreceitas.azurewebsites.net/api'
});
// ---------------------------- WHEN UI IS DONE UNCOMMENT THIS -------------------------------


// Add a request interceptor - encrypt data
instance.interceptors.request.use(function (config) {
	if (config.data) {
		let encryptText = encryption.encryptString(JSON.stringify(config.data));
		config.data = { messageEncrp: encryptText };
	}
	return config;
}, function (error) {
	return Promise.reject(error);
});


// Add a response interceptor - decrypt all request of gestao receitas
instance.interceptors.response.use(function (response) {
	if (response.data) {
		let decryptedMessage = encryption.decryptString(response.data.messageEncrp);
		response.data = JSON.parse(decryptedMessage);
	}
	return response;
}, function (error) {
	if (error.response.data) {
		let decryptedMessage = encryption.decryptString(error.response.data.messageEncrp);
		error.response.data = JSON.parse(decryptedMessage);
	}
	return Promise.reject(error);
});


module.exports = instance;