var jwt = require('jsonwebtoken');
var encryption = require('../auth/encryption');

/**
 * Verify if json object is empty
 * @param {*object json} obj 
 */
function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return JSON.stringify(obj) === JSON.stringify({});
}
/**
 * Middeware to handle, post and put (decrypt messages)
 * @param {*request} req 
 * @param {*response} res 
 * @param {*next} next 
 */
function decryptRequest(req, res, next) {
    if (req.method === 'POST' || req.method === 'PUT') {
        if (!isEmpty(req.body) && req.body.messageEncrp) {
           let decryptedMsg = encryption.decryptString(req.body.messageEncrp);
            req.body = JSON.parse(decryptedMsg);
            console.log(req.body);
        }
        next();
    } else {
        next();
    }
}
module.exports = decryptRequest;