var jwt = require('jsonwebtoken');
var config = require('../config/environment');
var Farmacias = require('../models/farmacia');

function verifyToken(req, res, next) {
    var token = req.headers['x-acess-token'];
    if (!token) {
        return res.status(401).send({
            auth: false, message: 'No token provived.'
        });
    }
    jwt.verify(token, config.secret, function (err, decoded) {
        if (err) {
            return res.status(500).send({
                auth: false, message: 'Failed to autenticate token'
            });
        }
        const tokenDecoded = jwt.decode(token);
        Farmacias.findOne({ 'farmaceuticos.email': decoded.userFarmaceutico }, (err, farmacia) => {
            if (farmacia) {
                req.farmacia = farmacia._id;
                req.tokenGdr = tokenDecoded.tokenGdr;
                next();
            }
        })
    });
}
module.exports = verifyToken;