var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var ordemEncomendaSchema = require('./ordemEncomenda');
var Schema = mongoose.Schema;

//mongoose schema Stock
var stockSchema = new Schema({
    quantidadeMinima: { type: Number, required: [true, "É necessário"], min: [0, "Quantidade Minima tem de ser pelo menos 0"] },
    quantidadeAtual: { type: Number, required: [true, "É necessário"], min: [0, "Quantidade Atual tem de ser pelo menos 0"] },
    quantidadeLote: { type: Number, required: [true, "É necessário"], min: [0, "Quantidade Lote tem de ser pelo menos 0"] },
    isEncomendado: { type: Boolean, default: false },
    ordensEncomenda: { type: [ordemEncomendaSchema], default: []},
    idMedicamento: { type: String, dropDups: true, required: [true, "É necessário"] },
});

stockSchema.plugin(uniqueValidator);

module.exports = stockSchema;