var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var MedicamentoSchema = require('./medicamento');

//mongoose schema Ordem
var ordemEncomendaSchema = new Schema({
    isEntregue: { type: Boolean, default: false },
    isManha: { type: Boolean, required: [true, "É necessário"] },
    nomeFarmacia: { type: String, required: [true, "É necessário"] },
    medicamento: { type: MedicamentoSchema, required: [true, "É necessário"] },
    quantidade: { type: Number, required: [true, "É necessário"], min: [1, "Quantidade tem de ser pelo menos 1"] },
    isOnFornecedor: { type: Boolean, default: false },
    data: { type: Date, default: Date.now },
});

module.exports = ordemEncomendaSchema;