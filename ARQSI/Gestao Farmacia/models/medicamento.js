var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//mongoose schema Stock
var medicamentoSchema = new Schema({
    nome: { type: String, required: [true, "É necessário"]},
    quantidade: { type: String, required: [true, "É necessário"]},
    concentracao: { type: String, required: [true, "É necessário"]},
    forma: { type: String, required: [true, "É necessário"]},
    nomeFarmaco: { type: String, required: [true, "É necessário"]}
});

module.exports = medicamentoSchema;