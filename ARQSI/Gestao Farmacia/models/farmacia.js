var mongoose = require('mongoose');
var farmaceuticoSchema = require('./farmaceutico');
var stockSchema = require('./stock');
var ordemEncomendaSchema = require('./ordemEncomenda');
var uniqueValidator = require('mongoose-unique-validator');
var mongoose_id_validator = require("mongoose-id-validator");
var Schema = mongoose.Schema;

//mongoose schema Farmacia
var farmaciaSchema = new Schema({
    nome: { type: String, unique: true, required: [true, "É necessário"] },
    isManha: { type: Boolean, required: [true, "É necessário"] },
    farmaceuticos: [{ type: farmaceuticoSchema, required: [true, "É necessário"] }],
    stocks: { type: [stockSchema], default: [] },
    ordensPorConfirmar: { type: [ordemEncomendaSchema], default: [] },
});

farmaciaSchema.plugin(uniqueValidator);
farmaciaSchema.plugin(mongoose_id_validator);

module.exports = mongoose.model('Farmacia', farmaciaSchema);