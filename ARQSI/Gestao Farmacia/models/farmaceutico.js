var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

//mongoose schema Farmaceutico
var farmaceuticoSchema = new Schema({
    email: {type: String, dropDups: true, required: [true, "É necessário"] },
    emailGdr: {type: String,  required: [true, "É necessário"] }
});

farmaceuticoSchema.plugin(uniqueValidator);

module.exports = farmaceuticoSchema;