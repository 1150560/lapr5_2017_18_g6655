// Needed packages
const express = require('express');
const config = require('./config/environment');

const app = express();  // Define app with express

// Configure the app
require('./config')(app);

// Register routes
require('./routes')(app);

// START THE SERVER
// =============================================================================
let server = app.listen(config.port, () => {
  console.log('Express server listening on port %d, in %s mode', config.port, config.env);
});

// For testing
module.exports.stop = () => {
  require('./config').closeMongoose();
  server.close();
}

module.exports.app = app;