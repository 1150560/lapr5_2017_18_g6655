var express = require('express');
var router = express.Router();
var Farmacia = require('../models/farmacia');
var axios = require('../auth/axios_config');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config/environment');
var verifyToken = require('../auth/VerifyToken');

/**
 * Set the default header to token
 * @param {*token} token 
 */
function defaultHeaders(token) {
    axios.defaults.headers.common['x-acess-token'] = token;
}

/**
 * Login do farmaceutico na gestao de receitas
 * @param {Email do farmaceutico na gestao de receitas} emailFarmaceutico 
 * @param {Password do farmaceutico} passwordFarmaceutico 
 */
function loginGdr(emailFarmaceutico, passwordFarmaceutico) {
    return axios.post('/users/login', { email: emailFarmaceutico, password: passwordFarmaceutico })
        .then(function (response) {
            return response.data.token;
        })
        .catch(function (error) {
            return Promise.reject(error.response.data);
        });
}

/**
 * Verificar se o token do utilizador é de um farmaceutico
 * @param {token da gestão de receitas} token 
 */
function isFarmaceutico(token) {
    const tokenDecoded = jwt.decode(token);
    return tokenDecoded.farmaceutico;
}

/**
 * Sink hole - confirmar Secret
 */
router.post('/confirmarSecret', verifyToken, function (req, res, next) {
    
    defaultHeaders(req.tokenGdr);
    axios.post('/users/confirmarSecret/', { secret: req.body.secret })
        .then(response => {
             return res.status(200).send(response.data);
        })
        .catch(error => {

        return res.status(error.response.status).send(error.response.data);
     });
  
});

/**
 * Sink hole - Alterar 2 fatores
 */
router.put('/alterarDoisFatores', verifyToken, function (req, res, next) {
    
    defaultHeaders(req.tokenGdr);
    axios.put('/users/alterarDoisFatores/', { })
        .then(response => {
             return res.status(200).send(response.data);
        })
        .catch(error => {

        return res.status(error.response.status).send(error.response.data);
     });
  
});

router.get('/doisFatores/isAtivo', verifyToken, function (req, res, next) {
    
    defaultHeaders(req.tokenGdr);
    axios.get('/users/doisFatores/isAtivo/')
        .then(response => {
             return res.status(200).send(response.data);
        })
        .catch(error => {

        return res.status(error.response.status).send(error.response.data);
     });
  
});

/**
 * Login na gestao de farmacias, token da gestao de receitas sera guardado no token de gestao de farmacias
 */
router.post('/', function (req, res) {
    Farmacia.findOne({ 'farmaceuticos.email': req.body.email }, function (err, farmacia) {
        if (err) {
            return;
        } else {
            if (farmacia) {
                let farmaceutico = farmacia.farmaceuticos.find(f => f.email === req.body.email);
                loginGdr(farmaceutico.emailGdr, req.body.password)
                    .then(function (response) {
                        if (isFarmaceutico(response)) {
                            const payload = {
                                tokenGdr: response,
                                userFarmaceutico: farmaceutico.email
                            };
                            var token = jwt.sign(payload, config.secret, {
                                expiresIn: config.tokenExpire
                            });
                            return res.status(200).json({
                                sucess: true,
                                message: 'Enjoy your token',
                                token: token
                            });
                        } else {
                            return res.status(401).json({ sucess: false, message: 'Authentication failed' });
                        }
                    }).catch(function (error) {
                        return res.status(401).json({ sucess: false, message: 'Authentication failed' });
                    })
            } else {
                return res.status(401).json({ sucess: false, message: 'Authentication failed' });
            }
        }
    })
})



module.exports = router;