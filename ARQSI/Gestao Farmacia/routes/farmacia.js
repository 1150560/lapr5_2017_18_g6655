var express = require('express');
var router = express.Router();
var Farmacia = require('../models/farmacia');
var Farmaceutico = require('../models/farmaceutico');
var Stock = require('../models/stock');
var FornecedorAPI = require('../connection/fornecedor');
var ReceitaAPI = require('../connection/receitas');
var axios = require('../auth/axios_config');
var verifyToken = require('../auth/verifyToken');
var moment = require('moment');
var idOrdemEncomenda;

function defaultHeaders(token) {
    axios.defaults.headers.common['x-acess-token'] = token;
}

// Get all stocks from a farmacia
router.get('/stocks', verifyToken, function (req, res, next) {

    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(400).json({ message: 'O id da farmácia está incorreto' });
            } else {
                axios.get('/medicamentos')
                    .then(response => {
                        //adicionar o medicamento ao stock (alem do id)
                        let stocks = farmacia.stocks.map(stock => {
                            let medicamento = response.data.find(medicamento => medicamento.id == stock.idMedicamento);
                            let stock2 = stock.toObject();
                            stock2.medicamento = medicamento;
                            return stock2;
                        });
                        return res.status(200).json(stocks);
                    })
                    .catch(error => {
                        return res.status(error.response.status).send({ message: error.message });
                    })
            }
        }
    });
});

/**
 * Get all medicamentos of apresentacao in farmacia
 */
router.get('/apresentacao/:id2/medicamentos', verifyToken, function (req, res, next) {
    Farmacia.findOne({ _id: req.farmacia }, (err, farmacia) => {
        if (err) {
            return res.status(400).json({ message: err.message });
        }
        if (!farmacia) {
            return res.status(404).json({ message: 'O id da farmácia está incorreto' });
        }
        axios.get('/medicamentos/apresentacao/' + req.params.id2)
            .then(response => {
                let medicamentos = farmacia.stocks.reduce((medicamentos, stock, index) => {
                    //verificar se o medicamento é da apresentacao
                    medicamento = response.data.find((med) => {
                        return stock.idMedicamento == med.id;
                    })
                    if (medicamento) {
                        medicamentos.push(medicamento);
                    }
                    return medicamentos;
                }, [])
                return res.status(200).send(medicamentos);
            })
            .catch(error => {
                return res.status(error.response.status).send({ message: error.message });
            });
    });
});

/**
 * Get medicamentos that are not in farmacia (to create stocks)
 */
router.get('/stock/medicamentos', verifyToken, function (req, res, next) {
    Farmacia.findOne({ _id: req.farmacia }, (error, farmacia) => {
        if (error) {
            return res.status(400).json({ message: error.message });
        }
        if (!farmacia) {
            return res.status(404).json({ message: 'O id da farmácia está incorreto' });
        }
        axios.get('/medicamentos')
            .then(response => {
                let medicamentos = response.data.reduce((medicamentos, medicamento, index) => {
                    let containsMedicamento = farmacia.stocks.find((stock) => {
                        return medicamento.id == stock.idMedicamento;
                    })
                    if (!containsMedicamento) {
                        medicamentos.push(medicamento);
                    }
                    return medicamentos;
                }, []);
                return res.status(200).send(medicamentos);
            })
            .catch(error => {
                return res.status(error.response.status).send({ message: error.message });
            })
    });
});
// Get stock by id from a farmacia
router.get('/stocks/:id2', verifyToken, function (req, res, next) {

    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(400).json({ message: 'O id da farmácia está incorreto' });
            } else {
                for (var i = 0; i < farmacia.stocks.length; i++) {
                    if (farmacia.stocks[i]._id == req.params.id2) {
                        return res.status(200).json(farmacia.stocks[i]);
                    }
                }
                return res.status(400).json({ message: 'O id do stock está incorreto' });
            }
        }
    });
});

// Get ordens encomenda por entregar
router.get('/ordensEncomenda/PorEntregar', verifyToken, function (req, res, next) {

    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(400).json({ message: 'O id da farmácia está incorreto' });
            } else {
                let ordensEncomenda = [];
                for (let stock of farmacia.stocks) {
                    for (let ordemEncomenda of stock.ordensEncomenda) {
                        if (!ordemEncomenda.isEntregue && ordemEncomenda.isOnFornecedor) {
                            ordensEncomenda.push(ordemEncomenda);
                        }
                    }
                }
                return res.status(200).json(ordensEncomenda);
            }
        }
    });
});

// Get ordens encomenda por confirmar
router.get('/ordensEncomenda/PorConfirmar', verifyToken, function (req, res, next) {

    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(400).json({ message: 'O id da farmácia está incorreto' });
            } else {
                return res.status(200).json(farmacia.ordensPorConfirmar);
            }
        }
    });
});

// Create farmacia
router.post('/', verifyToken, function (req, res, next) {
    var farmacia = new Farmacia();
    farmacia.nome = req.body.nome;
    farmacia.isManha = req.body.isManha;
    farmacia.farmaceuticos = req.body.farmaceuticos;
    farmacia.stocks = req.body.stocks;

    farmacia.save(function (err) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            return res.status(201).json({ message: 'Farmácia criada' });
        }
    });
});

// Create a stock in a farmacia
router.post('/stock/', verifyToken, function (req, res, next) {
    ReceitaAPI.getMedicamentoInfo(req.body.idMedicamento)
        .then(medicamento => {
            Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
                if (err) {
                    return res.status(400).json({ message: err.message });
                } else {
                    if (!farmacia) {
                        return res.status(404).json({ message: 'O id da farmácia está incorreto' });
                    } else {
                        if (req.body.quantidadeAtual < req.body.quantidadeMinima) {
                            return res.status(400).json({ message: 'Quantidade atual tem de ser maior ou igual à quantidade minima' });
                        } else {
                            var stockErro = farmacia.stocks.find(s => s.idMedicamento == req.body.idMedicamento);
                            if (stockErro) {
                                return res.status(400).json({ message: 'Já existe um stock para este medicamento' });
                            } else {
                                var stock = {};
                                stock.quantidadeAtual = req.body.quantidadeAtual;
                                stock.quantidadeMinima = req.body.quantidadeMinima;
                                stock.quantidadeLote = req.body.quantidadeLote;
                                stock.idMedicamento = req.body.idMedicamento;
                                farmacia.stocks.push(stock);
                                farmacia.save(function (err) {
                                    if (err) {
                                        return res.status(400).json({ message: err.message });
                                    } else {
                                        let s = farmacia.stocks.find(s => s.idMedicamento == req.body.idMedicamento);
                                        return res.status(201).json({ message: 'Stock criado com sucesso', id: s._id });
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }).catch(function (error) {
            return res.status(400).json({ message: 'Id do medicamento inválido' });
        });
});
// Update a stock in a farmacia (quantidadeMinima, quantidadeLote)
router.put('/stock/:id2', verifyToken, function (req, res, next) {

    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(404).json({ message: 'O id da farmácia está incorreto' });
            } else {
                let stock = farmacia.stocks.id(req.params.id2);
                if (stock) {
                    if (req.body.quantidadeMinima > stock.quantidadeAtual) {
                        return res.status(400).json({ message: "Quantidade mínima não pode ser maior que a atual" });
                    } else {
                        stock.quantidadeMinima = req.body.quantidadeMinima;
                        stock.quantidadeLote = req.body.quantidadeLote;
                        farmacia.save(function (err) {
                            if (err) {
                                return res.status(400).json({ message: err.message });
                            } else {
                                return res.status(201).json({ message: 'Stock editado com sucesso' });
                            }
                        });
                    }

                } else {
                    return res.status(404).json({ message: 'Id do stock incorreto' });
                }
            }
        }
    });
});

// Aviar medicamento in a stock (quantidadeAtual)
router.put('/medicamento/:id2/aviar', verifyToken, function (req, res, next) {
    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(404).json({ message: 'Id da farmácia incorreto' });
            } else {

                let stock = farmacia.stocks.find(s => s.idMedicamento == req.params.id2);
                if (stock) {
                    if (!(req.body.quantidade > 0)) {
                        return res.status(400).json({ message: "Quantidade tem de ser positiva" });
                    } else if (req.body.quantidade > stock.quantidadeAtual) {
                        return res.status(400).json({
                            message: "Quantidade não pode ser superior à quantidade atual (" +
                                stock.quantidadeAtual + ")"
                        });
                    } else {
                        // Aviar na Gestao Receitas
                        defaultHeaders(req.tokenGdr);
                        axios.post('/receita/' + req.body.idReceita + '/prescricao/' + req.body.idPrescricao + '/aviar',
                            { quantidade: req.body.quantidade })
                            .then(response => {
                                stock.quantidadeAtual -= req.body.quantidade;

                                // Desencadear ordem
                                if (stock.quantidadeAtual < stock.quantidadeMinima && !stock.isEncomendado && stock.quantidadeLote > 0) {
                                    desencadearOrdem(farmacia, stock, req.params.id2, res);
                                } else {
                                    // Guardar dados
                                    farmacia.save(function (err) {
                                        if (err) {
                                            return res.status(400).json({ message: err.message });
                                        } else {
                                            return res.status(201).json({ message: 'Stock aviado com sucesso.' });
                                        }
                                    });
                                }
                            })
                            //Erro a aviar Prescricao
                            .catch(error => {
                                //      return res.status(400).json({message:"Dados da prescrição incorretos"});
                                return res.status(error.response.status).json({ message: error.response.data.message });
                            })

                    }

                } else {
                    return res.status(404).json({ message: 'Id do stock incorreto' });
                }
            }
        }
    });
});

// Desencadeia uma ordem de encomenda
// Guarda a ordem na BD e na API de fornecedor
function desencadearOrdem(farmacia, stock, idMedicamento, res) {
    ReceitaAPI.getMedicamentoInfo(idMedicamento)
        .then(medicamento => {
            let ordem = {
                isManha: farmacia.isManha,
                nomeFarmacia: farmacia.nome,
                medicamento: {
                    nome: medicamento.nome,
                    quantidade: medicamento.quantidade,
                    concentracao: medicamento.concentracao,
                    forma: medicamento.forma,
                    nomeFarmaco: medicamento.nomeFarmaco
                },
                quantidade: stock.quantidadeLote,
            }

            stock.ordensEncomenda.push(ordem);
            stock.isEncomendado = true;

            // Guardar ordem na BD da farmacia
            farmacia.save(function (err) {
                if (err) {
                    return res.status(400).json({ message: err.message });
                } else {
                    var lastOrdem = stock.ordensEncomenda[stock.ordensEncomenda.length - 1];
                    FornecedorAPI.createOrdem(lastOrdem)
                        .then(result => {
                            lastOrdem.isOnFornecedor = true;
                            // Confirmar que o fornecedor recebeu a encomenda
                            farmacia.save(function (err) {
                                if (err) {
                                    return res.status(400).json({ message: err.message });
                                }
                                return res.status(201).json({ message: 'Stock aviado com sucesso. Foi realizada uma ordem de encomenda.' });
                            });
                        }).catch(function (error) {
                            stock.isEncomendado = false;
                            // Confirmar que o fornecedor recebeu a encomenda
                            farmacia.save(function (err) {
                                if (err) {
                                    return res.status(400).json({ message: err.message });
                                } else {
                                    return res.status(201).json({ message: 'Foi realizada uma ordem de encomenda, porém o fornecedor não a recebeu' });
                                }
                            });
                        });
                }
            });
        }
        ).catch(function (error) {
            return res.status(400).json({ message: 'Id do medicamento inválido' });
        });
};

function ordemEncomenda(stock) {
    let ordemEncomenda = stock.ordensEncomenda.find(o => o._id == idOrdemEncomenda);
    if (ordemEncomenda) {
        return true;
    }
    return false;
}

// Confirm delivery (quantidadeAtual, isEncomendado)
router.put('/ordemEncomenda/:id2/confirmar', verifyToken, function (req, res, next) {
    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(404).json({ message: 'Id da farmácia incorreto' });
            } else {


                idOrdemEncomenda = req.params.id2;
                let stock = farmacia.stocks.find(ordemEncomenda);
                if (!stock) {
                    return res.status(404).json({ message: 'Id da ordem de encomenda incorreto' });
                } else {
                    let ordemEncomenda = stock.ordensEncomenda.id(req.params.id2);
                    if (ordemEncomenda.isEntregue) {
                        return res.status(400).json({ message: 'Esta ordem já foi confirmada' });
                    } else {
                        stock.quantidadeAtual += ordemEncomenda.quantidade;
                        stock.isEncomendado = false;
                        ordemEncomenda.isEntregue = true;

                        // Remover da Lista por Confirmar
                        let newOrdensPorConfirmar = farmacia.ordensPorConfirmar.filter(ordem => ordem._id != req.params.id2);
                        farmacia.ordensPorConfirmar = newOrdensPorConfirmar;
                        farmacia.save(function (err) {
                            if (err) {
                                return res.status(400).json({ message: err.message });
                            } else {
                                FornecedorAPI.confirmaOrdem(ordemEncomenda)
                                    .then(result => {
                                        return res.status(200).json({ message: 'Ordem confirmada com sucesso.' });

                                    }).catch(function (error) {
                                        return res.status(201).json({ message: 'Foi confirmada a ordem de encomenda, porém o fornecedor não recebeu a confirmação' });
                                    });
                            }
                        });
                    }
                }
            }
        }
    });
});

// Create a ordemEncomendaPorConfirmar in a farmacia
router.post('/ordemEncomenda', function (req, res, next) {

    Farmacia.findOne({ 'stocks.ordensEncomenda._id': req.body.idOrdemEncomenda }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(404).json({ message: 'O id da farmácia está incorreto' });
            } else {
                idOrdemEncomenda = req.body.idOrdemEncomenda;
                let stock = farmacia.stocks.find(ordemEncomenda);
                if (!stock) {
                    return res.status(404).json({ message: 'Id da ordem de encomenda incorreto' });
                } else {
                    let ordemEncomenda = stock.ordensEncomenda.id(req.body.idOrdemEncomenda);
                    let ordemExistente = farmacia.ordensPorConfirmar.id(idOrdemEncomenda);
                    if (ordemExistente) {
                        return res.status(400).json({ message: 'Esta ordem já tinha sido adicionada à ordens por confirmar' });
                    }
                    if (ordemEncomenda.isEntregue) {
                        return res.status(400).json({ message: 'Esta ordem já foi confirmada' });
                    }
                    farmacia.ordensPorConfirmar.push(ordemEncomenda);
                    farmacia.save(function (err) {
                        if (err) {
                            return res.status(400).json({ message: err.message });
                        } else {
                            return res.status(201).json({ message: 'Ordem por confirmar adicionada com sucesso' });
                        }
                    });
                }
            }
        }
    });
});

// Delete a stock (only if quantidadeAtual is 0 and is not encomendado)
router.delete('/stock/:id2', verifyToken, function (req, res, next) {
    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(404).json({ message: 'Id da farmácia incorreto' });
            } else {

                let stock = farmacia.stocks.id(req.params.id2);
                if (stock) {
                    if (stock.quantidadeAtual > 0 || stock.isEncomendado) {
                        return res.status(400).json({ message: 'Não é possível apagar este stock.' });
                    } else {
                        var index = farmacia.stocks.indexOf(stock);
                        if (index > -1) {
                            farmacia.stocks.splice(index, 1);
                        }
                        farmacia.save(function (err) {
                            if (err) {
                                return res.status(400).json({ message: err.message });
                            } else {
                                return res.status(200).json({ message: 'Stock elminado com sucesso.' });
                            }
                        });
                    }

                } else {
                    return res.status(404).json({ message: 'Id do stock incorreto' });
                }
            }
        }
    });
});

// Get top 10 Stocks With Encomendas
router.get('/stocksChart', verifyToken, function (req, res, next) {
    Farmacia.findOne({ _id: req.farmacia }, function (err, farmacia) {
        if (err) {
            return res.status(400).json({ message: err.message });
        } else {
            if (!farmacia) {
                return res.status(404).json({ message: 'Id da farmácia incorreto' });
            } else {
                axios.get('/medicamentos')
                    .then(response => {

                        let stocks = farmacia.stocks.map(stock => {
                            let stock2 = {};
                            stock2.quantidade = 0;
                            stock.ordensEncomenda.forEach(element => {
                                var diffDays = moment(moment()).diff(element.data, 'day');
                                if (diffDays < 15) {
                                    stock2.quantidade++;
                                }
                            });

                            stock2.nome = response.data.find(med => med.id == stock.idMedicamento).nome;
                            return stock2;
                        })
                        stocks.sort(function(a, b){
                            return b.quantidade-a.quantidade;
                        });
                        if(stocks.length > 10){
                            stocks.splice(10);
                        }
                        return res.status(200).json({ stocks });

                    })
                    .catch(error => {
                        return res.status(error.response.status).send({ message: error.message });
                    })
            }
        }
    });
});

module.exports = router;