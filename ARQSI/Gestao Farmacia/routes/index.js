const farmacia = require('./farmacia');
const login = require('./login');
const receitas = require('./receitas');

module.exports = (app) => {
    app.use('/api/farmacia', farmacia);
    app.use('/api/login', login);
    app.use('/api/receitas', receitas);
};