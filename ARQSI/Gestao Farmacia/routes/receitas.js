var express = require('express');
var router = express.Router();
var verifyToken = require('../auth/verifyToken');
var axios = require('../auth/axios_config');

/**
 * Set the default header to token
 * @param {*token} token 
 */
function defaultHeaders(token) {
    axios.defaults.headers.common['x-acess-token'] = token;
}

/**
 * Sinkhole - return receita with given id (connection made with gestao receitas)
 */
router.get('/:id', verifyToken, function (req, res, next) {
    defaultHeaders(req.tokenGdr);
    axios.get('/receita/' + req.params.id)
        .then(response => {
            return res.status(200).send(response.data);
        })
        .catch(error => {
            return res.status(error.response.status).send(error.response.data);
        })
})

/**
 * Sinkhole - return all aviamentos of prescricao (connection wit gestao receitas)
 */
router.get('/:id/prescricao/:id2/aviamentos', verifyToken, function (req, res, next) {
    defaultHeaders(req.tokenGdr);
    axios.get('/receita/' + req.params.id + '/prescricao/' + req.params.id2 + '/aviamentos')
        .then(response => {
            return res.status(200).send(response.data);
        })
        .catch(error => {
            return res.status(error.response.status).send(error.response.data);
        })
})
module.exports = router;