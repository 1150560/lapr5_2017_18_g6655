var seeder = require('mongoose-seeder');
var data = require('./data.json');
var mongoose = require('mongoose');

require('./models/farmacia');

mongoose.connect('mongodb://Admin:admin@ds159344.mlab.com:59344/gestao-farmacias', { useMongoClient: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on('connected', function () {
    console.log("we're connected!");
    seeder.seed(data).then(function (dbData) {
        console.log("DB criada");
        db.close();
    }).catch(function (err) {
        console.log("Erro a criar DB");
        db.close();
    });

});
