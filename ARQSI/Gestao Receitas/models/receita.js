var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");

var prescricaoSchema =  require('./prescricao');
var Schema = mongoose.Schema;

//mongoose schema Receita
var receitaSchema = new Schema({
    medico: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: [true, "Is required"] },
    paciente: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: [true, "Is required"] },
    data: { type: Date, default: Date.now },
    prescricoes :{type: [prescricaoSchema], required: [true, "Is required"]}
});

receitaSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Receita', receitaSchema);