var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoose_validator = require("mongoose-id-validator");
var uniqueValidator = require('mongoose-unique-validator');

//mongoose schema USER
var userSchema = new Schema({
    name: {type: String},
    password: {type: String},
    email: {type: String},
    medico: {type:Boolean},
    farmaceutico: {type:Boolean},
    contaAtivada: {type:Boolean, default: true },
    doisFatoresAtivado: {type:Boolean, default: false},
    secret:{type:String}
});



userSchema.plugin(uniqueValidator);
userSchema.plugin(mongoose_validator);
module.exports = mongoose.model('User', userSchema);