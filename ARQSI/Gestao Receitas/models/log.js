var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var Schema = mongoose.Schema;
var validators = require("mongoose-validators");

// schema Logs
var logSchema = new Schema({
    titulo: { type: String, required: [true, "Is required"] },
    data: { type: Date, default: Date.now, required: [true, "Is required"] },
    texto: { type: String, required: [true, "Is required"] }
});

logSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Log', logSchema);