var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var axios = require('../auth/config_axios');
var axiosGeral = require('axios');
var validators = require("mongoose-validators");
var Schema = mongoose.Schema;


//All methods to verify if the iformation in prescricao schema is valid
function getFarmacoById(id) {
    return axios.get('/farmaco/' + id);
}

function getApresentacaoById(id) {
    return axios.get('/apresentacao/' + id);

}

function getPosologiaById(id) {
    return axios.get('/posologia/' + id);
}

// schema Prescricao
var prescricaoSchema = new Schema({
    farmaco: { type: String },
    farmacoId: { type: String, required: [true, "Is required"] },
    apresentacao: {
        forma: String,
        concentracao: String,
        quantidade: String
    },
    apresentacaoId: { type: String, required: [true, "Is required"] },
    posologiaPrescrita: { type: String },
    posologiaId: { type: String, required: [true, "Is required"] },
    quantidade: { type: Number, required: [true, "Is required"], min: [1, "Number must be at least 1"] },
    validade: { type: Date, validate: validators.isAfter({ message: "Validade must be after currente date" }), required: [true, "Is required"] },
    aviamentos: [{
        farmaceutico: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: [true, "Is required"] },
        data: { type: Date, default: Date.now },
        quantidade: { type: Number, required: [true, "Is required"], min: [1, "Quantidade must be at least 1"] },
    }],
    notificado: {type: Boolean, default: false}
});

prescricaoSchema.pre('save', function (next) {
    var prescricao = this;
    Promise.all([getFarmacoById(this.farmacoId),
    getApresentacaoById(this.apresentacaoId),
    getPosologiaById(this.posologiaId)])
        .then((function (res) {
            prescricao.farmaco = res[0].data.nome;
            prescricao.apresentacao.forma = res[1].data.forma;
            prescricao.apresentacao.concentracao = res[1].data.concentracao;
            prescricao.apresentacao.quantidade = res[1].data.quantidade;
            prescricao.posologiaPrescrita = res[2].data.frequencia;
            next();
        }))
        .catch(function (error) {
            var err = new Error(error.request.path +" Not Found");
            err.status = 404;
            next(err);
        });

});
module.exports = prescricaoSchema;