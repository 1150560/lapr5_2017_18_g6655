var interceptor = require('express-interceptor');
var encryption = require('./config/encryption');

var encryptMessage = interceptor(function (req, res) {
    return {
        // Only JSON responses will be intercepted 
        isInterceptable: function () { 
            return /application\/json/.test(res.get('Content-Type'));;
        },
        // Encrypt message using AES56
        intercept: function (body, send) {
            let messageEncrp = encryption.encryptString(body);
            send(JSON.stringify({messageEncrp}));
        }
    };
})

module.exports = encryptMessage;