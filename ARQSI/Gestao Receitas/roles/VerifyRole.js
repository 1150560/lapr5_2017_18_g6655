var User = require('../models/user.js');

var VerifyRole = function () {
  var verify = function (userEmail, role, func) {
    User.findOne({
      email: userEmail
    }, function (err, user) {
      if (err) {
        throw err;
      }
      if (!user) {
        res.json({ success: false, message: 'Authentication failed.' });
      } else if (user) {
        // check if role matches
        if (role === 'medico') {
          func(user.medico === true);
        } else if (role === 'farmaceutico') {
          func(user.farmaceutico === true);
        } else {
          func(user.medico === false && user.farmaceutico === false);
        }
      }
    })
  }

  return {
    verify: function (userEmail, role, func) {
      return verify(userEmail, role, func);
    }
  }

}();

module.exports = VerifyRole;
