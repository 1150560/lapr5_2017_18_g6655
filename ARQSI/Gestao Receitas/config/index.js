var path = require('path');
const morgan = require('morgan');
var logger = require('morgan');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');
var config = require('./environment');
var cors = require('cors');
var socket = require('../socket');
var decryptRequest = require('./decrypt');
var interceptor = require('../interceptor');

module.exports = (app) => {
    app.use(cors());

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    if (config.env != 'test') {
        app.use(interceptor);
        app.use(decryptRequest);
    }
    if (config.env == 'production') {
        app.use(morgan('common'));
    } else {
        app.use(morgan('dev'));
    }

    mongoose.connect(config.mongoose.uri, { useMongoClient: true }); // Connect to database
    mongoose.Promise = global.Promise;

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function () {
        console.log("Connected to database!");
    });

    app.use(morgan('dev'));
};

// For testing
module.exports.closeMongoose = () => {
    mongoose.connection.close();
};
