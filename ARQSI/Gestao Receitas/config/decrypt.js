var jwt = require('jsonwebtoken');
var encryption = require('./encryption');

/**
 * Verify if json object is empty
 * @param {*object json} obj 
 */
function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return JSON.stringify(obj) === JSON.stringify({});
}

/**
 * Middewrae to handle cypher messages in post, put
 * @param {*request} req 
 * @param {*response} res 
 * @param {*next} next 
 */
function decryptRequest(req, res, next) {
    if (req.method === 'POST' || req.method === 'PUT') {
        if (!isEmpty(req.body) && req.body.messageEncrp) {
           let decryptedMsg = encryption.decryptString(req.body.messageEncrp);
            req.body = JSON.parse(decryptedMsg);
        }
        next();
    } else {
        next();
    }
}
module.exports = decryptRequest;