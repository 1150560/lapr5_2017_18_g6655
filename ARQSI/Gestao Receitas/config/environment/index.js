var _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
var all = {
    env: env,

    // Server port
    port: process.env.PORT || 3004,

    tokenExpire: 3600, // 1 Hour

    key: 'arqsi19823',

    algorithmEncryption: 'aes256',

    hostEmail: "mail.smtp2go.com",

    portEmail: 587,

    userEmail: "1150833",

    passEmail: "309901636",

    fromEmail: "hospital@isep.ipp.pt",

    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://<dbuser>:<dbpassword>@<dbserver>/<dbname>'
    },

    secret: 'secretFarmaceutico'
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`) || {}
);