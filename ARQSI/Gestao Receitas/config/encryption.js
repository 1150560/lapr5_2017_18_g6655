var crypto = require('crypto-js');
var config = require('./environment');

/**
 * Encrypt text, using aes256 algorithm
 * @param {*text to be encrypt} text 
 */
function encryptString(text) {
    return crypto.AES.encrypt(text, config.key).toString();
};

/**
 * Texto that needs to be decrypted
 * @param {*text to be decrypted} text 
 */
function decryptString(text) {
    var bytes = crypto.AES.decrypt(text, config.key);
    return bytes.toString(crypto.enc.Utf8);
};

module.exports = {
    encryptString: encryptString,
    decryptString: decryptString
}