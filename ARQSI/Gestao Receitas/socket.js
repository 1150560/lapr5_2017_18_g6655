
var socket_io = require('socket.io');
var io;
function start(server) {
    io = socket_io(server);
    io.on('connection', (socket) => {
      console.log('Connected');
    });
}

function notificar(id, mensagem) {
    io.emit('notify_' + id, mensagem);
}

module.exports = {
    start: start,
    notificar: notificar
}