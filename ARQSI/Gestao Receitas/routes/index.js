var express = require('express');
var router = express.Router();
var socket = require('../socket');

var users = require('./users');
var receita = require('./receita');
var apresentacoes = require('./apresentacoes');
var farmacos = require('./farmacos');
var posologias = require('./posologias');
var comentarios = require('./comentarios');
var medicamentos = require('./medicamentos');

module.exports = (app) => {
  app.use('/api/receita', receita);
  app.use('/api/users', users);
  app.use('/api/apresentacoes', apresentacoes);
  app.use('/api/farmacos', farmacos);
  app.use('/api/posologias', posologias);
  app.use('/api/comentarios', comentarios);
  app.use('/api/medicamentos', medicamentos);

  /* GET home page. */
  router.get('/', function (req, res, next) {
    // socket.notificar('5a28440b9bc2b81fe4b34995', 'Mensagem de test');
    res.send("OK");
    //res.render('index', { title: 'Express' });
  });

  function sendEmail(email, text) {
    var nodemailer = require("nodemailer");

    var smtpTransport = nodemailer.createTransport({
      host: "mail.smtp2go.com",
      port: 587, // 8025, 587 and 25 can also be used. 
      auth: {
        user: "diogo_filipinos@hotmail.com",
        pass: "xBKEqMsB2gmS"
      }
    });
    smtpTransport.sendMail({
      from: "notificar@isep.ipp.pt",
      to: email,
      subject: "Notificação Prescrições",
      text: text
    }, function (error, response) {
      if (error) {
        return;
      }
    });
  };

  router.post('/notificar/:id', function (req, res, next) {

    sendEmail(req.body.email, (req.body.mensagem || ""));
    socket.notificar(req.params.id, "Mensagem: " + (req.body.mensagem || ""));
    res.send("OK");
  });

  app.use('/', router);
};

