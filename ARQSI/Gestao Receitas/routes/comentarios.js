var express = require('express');
var router = express.Router();
var axios = require('../auth/config_axios');
var VerifyToken = require('../auth/VerifyToken');
var VerifyRole = require('../roles/VerifyRole');


router.post('/', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'medico', function (decision) {
        if (!decision) {
            return res.status(403).send(
                { auth: false, message: 'You have no authorization. You must be a Medico' });
        } else {
            return new Promise(function (resolve, reject) {
                axios.post('/comentarios', {apresentacaoId: req.body.apresentacaoId, descricao: req.body.descricao})
                    .then(function (response) {
                        return res.status(200).send(response.data);
                    }).catch(function (error) {
                        return res.status(error.response.status).send({erro: error.message});
                    });
            });
        }
    });
});

module.exports = router;