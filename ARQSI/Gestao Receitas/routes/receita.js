var express = require('express');
var router = express.Router();
var User = require('../models/user');
var VerifyToken = require('../auth/VerifyToken');
var TokenGdm = require('../auth/TokenGdm');
var VerifyRole = require('../roles/VerifyRole');
var Receita = require('../models/receita');
var axios = require('../auth/config_axios');
var Prescricoes = require('../models/prescricao');
var Log = require('../models/log');
var moment = require('moment');
var email = require('../email');

function createTextEmailReceita(receitaCode, interacoes) {
  var textEmail = 'Receita criada com o código: ' + receitaCode;
  if (interacoes.length > 0) {
    var textInteracoes = '';
    interacoes.forEach(interacao => {
      textInteracoes += interacao.Farmaco1 + ' x ' + interacao.Farmaco2 + '\n';
    })
    textEmail += '\nExistem as seguintes interações danosas:\n' + textInteracoes;
    return textEmail;
  } else {
    return textEmail;
  }
}

function getReceitas(findCondition, res) {
  Receita.find(findCondition).populate('medico', 'name email').populate('paciente', 'name email')
    .exec(function (err, receitas) {
      if (err) {
        return res.status(400).json({ message: err.message });
      } else {
        if (!receitas) {
          return res.status(404).json({ message: 'You have no Receitas' });
        } else {
          return res.status(200).json(receitas);
        }
      }
    });
};


//Get Top Farmacos mais vendidos
router.get('/farmacos/chart', function (req, res, next) {
  axios.get('/farmaco')
    .then(response => {
      let farmacos = response.data.map(farmaco => {
        farmaco.quantidadeTotal = 0;
        farmaco.farmacoName = farmaco.nome;
        return farmaco;
      });
      Receita.find({ 'prescricoes.aviamentos': { $gt: [] } }, (err, receitas) => {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          if (!receitas) {
            return res.status(404).json({ message: 'You have no Receitas' });
          } else {
            receitas.forEach(receita => {
              receita.prescricoes.forEach(prescricao => {
                let farmaco = farmacos.find(farmaco => farmaco.id == prescricao.farmacoId);
                prescricao.aviamentos.forEach(aviamento => {
                  farmaco.quantidadeTotal += aviamento.quantidade;
                })
              })
            });
            return res.status(200).json(farmacos);
          }
        }
      });
    })
    .catch(error => {
      return res.status(400).json({ message: error });
    })
});

function getReceita(findCondition, res) {
  Receita.findOne(findCondition).populate('medico', 'name email').populate('paciente', 'name email')
    .exec(function (err, receita) {
      if (err) {
        return res.status(400).json({ message: err.message });
      } else {
        if (!receita) {
          return res.status(404).json({ message: 'You have no Receita with this id' });
        } else {
          return res.status(200).json(receita);
        }
      }
    });
};

function getPrescricoes(findCondition, res) {
  Receita.findOne(findCondition, function (err, receita) {
    if (err) {
      return res.status(400).json({ message: err.message });
    } else {
      if (!receita) {
        return res.status(404).json({ message: 'You have no Receita with this id' });
      } else {

        return res.status(200).json(receita.prescricoes);
      }
    }
  });
};

function getPrescricao(findCondition, res, prescricaoId) {
  Receita.findOne(findCondition, function (err, receita) {
    if (err) {
      return res.status(400).json({ message: err.message });
    } else {
      if (!receita) {
        return res.status(404).json({ message: 'You have no Receita with this id' });
      } else {
        for (var i = 0; i < receita.prescricoes.length; i++) {
          if (receita.prescricoes[i]._id == prescricaoId) {
            return res.status(200).json(receita.prescricoes[i]);
          }
        }
        return res.status(404).json({ message: 'You have no Prescricao with this id in Receita with this id' });
      }
    }
  });
};


function getAviamentos(findCondition, prescricaoId, res) {
  Receita.findOne(findCondition, function (err, receita) {
    if (err) {
      return res.status(400).json({ message: err.message });
    } else {
      if (!receita) {
        return res.status(404).json({ message: 'You have no Receita with this id' });
      } else {
        var tamanhoPrescricoes = receita.prescricoes.length;
        for (var i = 0; i < tamanhoPrescricoes; i++) {
          if (receita.prescricoes[i]._id == prescricaoId) {
            return res.status(200).json(receita.prescricoes[i].aviamentos);
          }
        }
        return res.status(404).json({ message: 'You have no Prescricao with this id in this Receita' });
      }
    }
  });
};

function getAviamento(findCondition, req, res) {
  Receita.findOne(findCondition, function (err, receita) {
    if (err) {
      return res.status(400).json({ message: err.message });
    } else {
      if (!receita) {
        return res.status(404).json({ message: 'You have no Receita with this id' });
      } else {
        var tamanhoPrescricoes = receita.prescricoes.length;
        for (var i = 0; i < tamanhoPrescricoes; i++) {
          if (receita.prescricoes[i]._id == req.params.id2) {
            var tamanhoAviamentos = receita.prescricoes[i].aviamentos.length;
            for (var j = 0; j < tamanhoAviamentos; j++) {
              if (receita.prescricoes[i].aviamentos[j]._id == req.params.id3) {
                return res.status(200).json(receita.prescricoes[i].aviamentos[j]);
              }
            }
            return res.status(404).json({ message: 'You have no Aviamento with this id in this Prescricao' });
          }
        }
        return res.status(404).json({ message: 'You have no Prescricao with this id in this Receita' });
      }
    }
  });
};

function createAviamento(receita, user, req, res) {
  var tamanhoPrescricoes = receita.prescricoes.length;
  for (var i = 0; i < tamanhoPrescricoes; i++) {
    if (receita.prescricoes[i]._id == req.params.id2) {
      var tamanhoAviamentos = receita.prescricoes[i].aviamentos.length;
      var quantidadeRestante = receita.prescricoes[i].quantidade;
      for (var j = 0; j < tamanhoAviamentos; j++) {
        quantidadeRestante -= receita.prescricoes[i].aviamentos[j].quantidade;
      }
      if (quantidadeRestante == 0) {
        return res.status(403).json({ message: 'Não pode criar mais aviamentos' });
      } else {
        if (req.body.quantidade > quantidadeRestante) {
          return res.status(403).json({ message: 'Quantidade tem de ser menor ou igual a ' + quantidadeRestante });
        } else {
          const aviamento = {
            farmaceutico: user._id,
            quantidade: req.body.quantidade
          }
          receita.prescricoes[i].aviamentos.push(aviamento);
          receita.save(function (err) {
            if (err) {
              return res.status(400).json({ message: 'Data de validade expirada' });
            } else {
              createLog("Aviamento", req.params.id2, user._id);
              return res.status(201).json({ message: 'Aviamento e log criados' });
            }
          });
        }
      }
    }

  }
}

function updateAviamento(receita, user, req, res) {
  var tamanhoPrescricoes = receita.prescricoes.length;
  for (var i = 0; i < tamanhoPrescricoes; i++) {
    if (receita.prescricoes[i]._id == req.params.id2) {

      var tamanhoAviamentos = receita.prescricoes[i].aviamentos.length;
      var quantidadeRestante = receita.prescricoes[i].quantidade;

      var aviamentoNumber = 0;
      for (var j = 0; j < tamanhoAviamentos; j++) {
        if (receita.prescricoes[i].aviamentos[j]._id != req.params.id3) {
          quantidadeRestante -= receita.prescricoes[i].aviamentos[j].quantidade;
        } else {
          aviamentoNumber = j;
        }
      }
      if (String(receita.prescricoes[i].aviamentos[aviamentoNumber].farmaceutico) != String(user._id)) {
        return res.status(403).json({ message: 'You are not the author of this Aviamento' });
      } else {
        if (quantidadeRestante == 0) {
          return res.status(403).json({ message: 'Can not create more Aviamentos' });
        } else {
          if (req.body.quantidade > quantidadeRestante) {
            return res.status(403).json({ message: 'Quantidade must be less or equals to ' + quantidadeRestante });
          } else {
            receita.prescricoes[i].aviamentos[aviamentoNumber].quantidade = req.body.quantidade;
            receita.save(function (err) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                return res.status(200).json({ message: 'Aviamento updated' });
              }
            });
          }
        }
      }
    }

  }
}
/**
 * New created Log 
 */
function createLog(tipo, id, userId) {
  var log = new Log();
  log.titulo = "Criar " + tipo;
  log.data = Date.now();
  if (tipo == "Receita") {
    log.texto = tipo + " " + id + " criada por " + userId;
  } else if (tipo == "Prescricao") {
    log.texto = tipo + " da Receita " + id + " criada por " + userId;
  } else {
    log.texto = tipo + " da Prescricao " + id + " criado por " + userId;
  }
  log.save(function (err) {
    if (err) {
      //return res.status(400).json({ message: err.message });
    }
  });
};


/**
 * New updated Log 
 */
function updatedLog(tipo, id, userId) {
  var log = new Log();
  log.titulo = "Alterar " + tipo;
  log.data = Date.now();
  if (tipo == "Receita") {
    log.texto = tipo + " " + id + " alterada por " + userId;
  } else if (tipo == "Prescricao") {
    log.texto = tipo + " da Receita " + id + " alterada por " + userId;
  } else {
    log.texto = tipo + " da Prescricao " + id + " alterado por " + userId;
  }
  log.save(function (err) {
    if (err) {
      return res.status(400).json({ message: err.message });
    }
  });
};


/**
 * Get todas as prescricoes de uma receita, mas so pode se for medico 
 *  e autor da receita ou paciente e paciente da receita ou farmaceutico
 */
router.get('/:id/prescricoes/', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (decision) {
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          getPrescricoes({ _id: req.params.id, medico: user._id }, res);
        }
      });
      return;
    } else {
      VerifyRole.verify(req.user, 'paciente', function (decision2) {
        if (decision2) {
          User.findOne({ email: req.user }, function (err, user) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              getPrescricoes({ _id: req.params.id, paciente: user._id }, res);
            }
          });
          return;
        } else {
          getPrescricoes({ _id: req.params.id }, res);
          return;
        }
      });
    }
  });
});

// ================================================================================================================

//Get uma prescricao, mas so pode criar se for medico e autor da receita
//ou paciente e paciente da receita ou farmaceutico
router.get('/:id/prescricao/:id2', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (decision) {
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          getPrescricao({ _id: req.params.id, medico: user._id }, res, req.params.id2);
          //getFarmaco({ _id: req.params.id, medico: user._id }, res, req.params.id2, req.body.farmacoId);
        }
      });
      return;
    } else {
      VerifyRole.verify(req.user, 'paciente', function (decision2) {
        if (decision2) {
          User.findOne({ email: req.user }, function (err, user) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              getPrescricao({ _id: req.params.id, paciente: user._id }, res, req.params.id2);
              //getFarmaco({ _id: req.params.id, paciente: user._id }, res, req.params.id2, req.body.farmacoId);
            }
          });
          return;
        } else {
          getPrescricao({ _id: req.params.id }, res, req.params.id2);
          //getFarmaco({ _id: req.params.id }, res, req.params.id2, req.body.farmacoId);
          return;
        }
      });
    }
  });
});

//Get todos os aviamentos de uma prescricao, mas so pode se for medico
// e autor da receita ou paciente e paciente da receita ou farmaceutico
router.get('/:id/prescricao/:id2/aviamentos', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (decision) {
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          getAviamentos({ _id: req.params.id, medico: user._id }, req.params.id2, res);
        }
      });
      return;
    } else {
      VerifyRole.verify(req.user, 'paciente', function (decision2) {
        if (decision2) {
          User.findOne({ email: req.user }, function (err, user) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              getAviamentos({ _id: req.params.id, paciente: user._id }, req.params.id2, res);
            }
          });
          return;
        } else {
          getAviamentos({ _id: req.params.id }, req.params.id2, res);
          return;
        }
      });
    }
  });
});


//Get todos os aviamentos de uma prescricao, mas so pode se for medico
// e autor da receita ou paciente e paciente da receita ou farmaceutico
router.get('/:id/prescricao/:id2/aviamento/:id3', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (decision) {
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          getAviamento({ _id: req.params.id, medico: user._id }, req, res);
        }
      });
      return;
    } else {
      VerifyRole.verify(req.user, 'paciente', function (decision2) {
        if (decision2) {
          User.findOne({ email: req.user }, function (err, user) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              getAviamento({ _id: req.params.id, paciente: user._id }, req, res);
            }
          });
          return;
        } else {
          getAviamento({ _id: req.params.id }, req, res);
          return;
        }
      });
    }
  });
});

//Get todas as receitas, mas so pode se for medico 
// e autor das receitas ou paciente e paciente da receitas 
router.get('/', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (decision) {
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          getReceitas({ medico: user._id }, res);
          //getTopFarmacosVendidos(res);
        }
      });
      return;
    } else {
      VerifyRole.verify(req.user, 'paciente', function (decision2) {
        if (decision2) {
          User.findOne({ email: req.user }, function (err, user) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              getReceitas({ paciente: user._id }, res);
            }
          });
          return;
        } else {
          return res.status(403).json({ message: 'Farmaceuticos cant read all Receitas' });
          return;
        }
      });
    }
  });
});


//Get todas as receita, mas so pode se for medico 
// e autor das receitas ou paciente e paciente da receitas 
router.get('/:id', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (decision) {
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return res.status(400).json({ message: err.message });
        } else {
          getReceita({ _id: req.params.id, medico: user._id }, res);
        }
      });
      return;
    } else {
      VerifyRole.verify(req.user, 'paciente', function (decision2) {
        if (decision2) {
          User.findOne({ email: req.user }, function (err, user) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              getReceita({ _id: req.params.id, paciente: user._id }, res);
            }
          });
          return;
        } else {
          getReceita({ _id: req.params.id }, res);
          return;
        }
      });
    }
  });
});

//post de uma receita, so pode se for medico
router.post('/', VerifyToken, function (req, res, next) {
  VerifyRole.verify(req.user, 'medico', function (decision) {
    if (!decision) {
      return res.status(403).send(
        { auth: false, message: 'You have no authorization. You must be a Medico' });
    } else {
      var receita = new Receita();
      User.findOne({ email: req.user }, function (err, user) {
        if (err) {
          return;
        } else {
          receita.medico = user._id;
          receita.paciente = req.body.paciente;
          receita.prescricoes = req.body.prescricoes;
          receita.save(function (err) {
            if (err) {
              return res.status(400).json({ message: err.message });
            } else {
              createLog("Receita", receita._id, receita.medico);
              let text = createTextEmailReceita(receita._id, req.body.interacoes);
              email.sendEmail(req.body.emailPaciente, 'Receita Criada', text);
              email.sendEmail(req.user, 'Receita Criada', text);
              res.status(201).json({ message: 'Receita and log created' });
            }
          });
        }
      })
    };
  });
});

  //Criar uma prescricao, mas so pode criar se for medico e se for autor da receita
  router.post('/:id/prescricao/', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'medico', function (decision) {
      if (!decision) {
        return res.status(403).json(
          { auth: false, message: 'You have no authorization. You must be a Medico' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return;
          } else {
            Receita.findOne({ _id: req.params.id, medico: user._id }, function (err, receita) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                if (!receita) {
                  return res.status(404).json({ message: 'You have no Receita with this id' });
                } else {
                  receita.prescricoes.push(req.body);
                  receita.save(function (err) {
                    if (err) {
                      return res.status(400).json({ message: err.message });
                    } else {
                      createLog("Prescricao", receita._id, receita.medico);
                      res.status(201).json({ message: 'Prescricao registered and log created' });
                    }
                  });
                }
              }
            });
          }
        });

      }
    });
  });

  //Criar um aviamento, mas so pode criar se for farmaceutico
  router.post('/:id/prescricao/:id2/aviar', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'farmaceutico', function (decision) {
      if (!decision) {
        return res.status(403).json(
          { auth: false, message: 'You have no authorization. You must be a Farmaceutico' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return;
          } else {
            Receita.findOne({ _id: req.params.id }, function (err, receita) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                if (!receita) {
                  return res.status(404).json({ message: 'You have no Receita with this id' });
                } else {
                  createAviamento(receita, user, req, res);
                  return;
                }
              }
            });
          }
        });
      }
    });
  });

  //Retorna um array de interacoes danosas de uma receita
  router.post('/checkInteracaoDanosa', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'medico', function (decision) {
      if (!decision) {
        return res.status(403).json(
          { auth: false, message: 'You have no authorization. You must be a medico' });
      } else {
        axios.get('/interacoesDanosas')
          .then(response => {
            let prescricoes = req.body.prescricoes;
            let interacoes2 = [];
            let interacoesExistentes = [];
            let index = 0;
            let length = prescricoes.length;
            if (length > 1) {
              for (let i = 0; i < length - 1; i++) {
                for (let j = i + 1; j < length; j++) {
                  let farmaco1 = prescricoes[i].farmacoId;
                  let farmaco2 = prescricoes[j].farmacoId;
                  let interacao = response.data.find((interacao) => {
                    return interacao.farmacoId1 == farmaco1 && interacao.farmacoId2 == farmaco2
                      || interacao.farmacoId1 == farmaco2 && interacao.farmacoId2 == farmaco1;
                  });
                  if (interacao) {
                    if (interacoesExistentes.indexOf(interacao) == -1) {
                      interacoes2.push({
                        Farmaco1: prescricoes[i].farmaco,
                        Farmaco2: prescricoes[j].farmaco
                      });
                      interacoesExistentes.push(interacao);
                    }
                  }
                }
                if (i == (length - 2)) {
                  res.status(200).json({ message: interacoes2 });
                }
              }
            } else {
              res.status(200).json({ message: interacoes2 });
            }
          })
          .catch(error => {
            return res.status(400).json({ message: error });
          });
      }
    })
  });
  //Alterar uma receita
  router.put('/:id', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'medico', function (decision) {
      if (!decision) {
        return res.status(403).json(
          { auth: false, message: 'You have no authorization. You must be a Medico' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return;
          } else {
            Receita.findOne({ _id: req.params.id, medico: user._id }, function (err, receita) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                if (!receita) {
                  return res.status(404).json({ message: 'You have no Receita with this id' });
                } else {
                  receita.prescricoes = req.body.prescricoes;
                  axios.get('/interacoesDanosas')
                    .then(response => {
                      let interacoes2 = [];
                      let index = 0;
                      let length = receita.prescricoes.length;
                      for (let i = 0; i < length - 1; i++) {
                        for (let j = i + 1; j < length; j++) {
                          let farmaco1 = receita.prescricoes[i].farmacoId;
                          let farmaco2 = receita.prescricoes[j].farmacoId;
                          let interacao = response.data.find((interacao) => {
                            return interacao.farmacoId1 == farmaco1 && interacao.farmacoId2 == farmaco2
                              || interacao.farmacoId1 == farmaco2 && interacao.farmacoId2 == farmaco1;
                          });
                          if (interacao) {
                            interacoes2.push({
                              Farmaco1: receita.prescricoes[i].farmaco,
                              Farmaco2: receita.prescricoes[j].farmaco
                            });
                          }
                        }
                      }
                      receita.save(function (err) {
                        if (err) {
                          return res.status(400).json({ message: err.message });
                        } else {
                          updatedLog("Receita", receita._id, user._id);
                          res.status(200).json({ message: 'Receita alterada', interacoes: interacoes2 });
                        }
                      });

                    })
                    .catch(error => {

                    });
                }
              }
            });
          }
        });
      }
    });
  });




  //Alterar uma prescricao, mas so pode alterar se for medico e se for autor da receita
  router.put('/:id/prescricao/:id2', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'medico', function (decision) {
      if (!decision) {
        return res.status(403).json(
          { auth: false, message: 'You have no authorization. You must be a Medico' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return;
          } else {
            Receita.findOne({ _id: req.params.id, medico: user._id }, function (err, receita) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                if (!receita) {
                  return res.status(404).json({ message: 'You have no Receita with this id' });
                } else {
                  var tamanhoPrescricoes = receita.prescricoes.length;
                  for (i = 0; i < tamanhoPrescricoes; i++) {
                    if (receita.prescricoes[i]._id == req.params.id2) {
                      //So modifica a prescricao caso nao exista ainda aviamentos
                      if (receita.prescricoes[i].aviamentos.length == 0) {
                        receita.prescricoes[i].farmacoId = req.body.farmacoId;
                        receita.prescricoes[i].apresentacaoId = req.body.apresentacaoId;
                        receita.prescricoes[i].posologiaId = req.body.posologiaId;
                        receita.prescricoes[i].validade = req.body.validade;
                        receita.prescricoes[i].quantidade = req.body.quantidade;
                        receita.save(function (err) {
                          if (err) {
                            return res.status(400).json({ message: err.message });
                          } else {
                            updatedLog("Aviamento", receita._id, user._id);
                            res.status(200).json({ message: 'Altered prescricao' });
                          }
                        });
                      } else {
                        return res.status(403).json({ auth: false, message: 'You have no authorization. Prescricao already has aviamentos.' })
                      }
                      break;
                    }
                  }
                }
              }
            });
          }
        });

      }
    });
  });





  //Alterar um aviamento, mas so pode alterar se for farmaceutico e se for autor do aviamento
  router.put('/:id/prescricao/:id2/aviamento/:id3', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'farmaceutico', function (decision) {
      if (!decision) {
        return res.status(403).json(
          { auth: false, message: 'You have no authorization. You must be a Farmaceutico' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return;
          } else {
            Receita.findOne({ _id: req.params.id, }, function (err, receita) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                if (!receita) {
                  return res.status(404).json({ message: 'You have no Receita with this id' });
                } else {
                  updateAviamento(receita, user, req, res);
                  return;
                }
              }
            });
          }
        });
      }
    });
  });

  router.get('/PorDiaDaSemana/UltimoAno', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'medico', function (decision) {
      if (!decision) {
        return res.status(403).send(
          { auth: false, message: 'You have no authorization. You must be a Medico' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return res.status(400).json({ message: err.message });
          } else {
            Receita.find({ medico: user._id }, function (err, receitas) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                let receitasDiaSemana = [];
                let diaSemana = ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"];
                for (let i = 0; i < 7; i++) {
                  receitasDiaSemana[i] = {};
                  receitasDiaSemana[i].dia = diaSemana[i];
                  receitasDiaSemana[i].quantidade = 0;
                }
                receitas.forEach(receita => {
                  var diffDays = moment(moment()).diff(receita.data, 'day');
                  if (diffDays < 365) {
                    receitasDiaSemana[receita.data.getDay()].quantidade++;
                  }
                });

                return res.status(200).json(receitasDiaSemana);
              }
            });
          }
        })

      }
    });
  });

  router.get('/Formas/TopConsumidas', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'paciente', function (decision) {
      if (!decision) {
        return res.status(403).send(
          { auth: false, message: 'You have no authorization. You must be a Paciente' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return res.status(400).json({ message: err.message });
          } else {

            Receita.find({ paciente: user._id }, function (err, receitas) {
              if (err) {
                return res.status(400).json({ message: err.message });
              } else {
                if (!receitas.length > 0) {
                  return res.status(400).json({ message: "Não tem receitas" });
                } else {
                  let formas = [];
                  let quantidadeTotal = 0;
                  receitas.forEach(receita => {
                    receita.prescricoes.forEach(prescricao => {
                      let quantidade = 0;
                      prescricao.aviamentos.forEach(aviamento => {
                        quantidade += aviamento.quantidade;
                      })
                      let existe = false;
                      for (let j = 0; j < formas.length; j++) {
                        if (prescricao.apresentacao.forma == formas[j].nome) {
                          formas[j].quantidade += quantidade;
                          quantidadeTotal += quantidade;
                          existe = true;
                        }
                      }
                      if (!existe) {
                        formas[formas.length] = {};
                        formas[formas.length - 1].nome = prescricao.apresentacao.forma;
                        formas[formas.length - 1].quantidade = quantidade;
                        quantidadeTotal += quantidade;
                      }
                    });
                  });
                  let percentagemTotal = 0;
                  for (let i = 0; i < formas.length; i++) {
                    formas[i].percentagem = parseInt(formas[i].quantidade / quantidadeTotal * 100);
                    percentagemTotal += parseInt(formas[i].quantidade / quantidadeTotal * 100);
                  }
                  formas.sort(function (a, b) { return b.quantidade - a.quantidade });

                  formas[0].percentagem += (100 - percentagemTotal);

                  return res.status(200).json(formas);
                }
              }
            });
          }
        })

      }
    });
  });

  router.get('/prescricoesInfo/info', VerifyToken, function (req, res, next) {
    VerifyRole.verify(req.user, 'paciente', function (decision) {
      if (!decision) {
        return res.status(403).send(
          { auth: false, message: 'You have no authorization. You must be a Paciente' });
      } else {
        User.findOne({ email: req.user }, function (erro, user) {
          if (erro) {
            return res.status(400).json({ message: err.message });
          } else {
            let numPrescPorAviar = 0, percPrescPorAviar = 0;
            let numPrescExp = 0, percPrescExp = 0;
            let numPrescAviadas = 0, percPrescAviadas = 0;
            let numTotalPresc = 0;
            let prescricoes;
            let result = [];
            Receita.find({ paciente: user._id }, function (err, receitas) {

              if (err) {
                return res.status(400).json({ message: err.message });
              } else {

                for (let i = 0; i < receitas.length; i++) {
                  for (let j = 0; j < receitas[i].prescricoes.length; j++) {
                    if (receitas[i].prescricoes[j].validade < (new Date(Date.now()))) {
                      numPrescExp += 1;
                    } else if (receitas[i].prescricoes[j].aviamentos.length == 0) {
                      numPrescPorAviar += 1;
                    } else {
                      numPrescAviadas += 1;
                    }
                    numTotalPresc += 1;
                  }
                }
                if (numTotalPresc == 0) {
                  result.push({ nome: "aviadas: ", valor: 0 }, { nome: "expiradas: ", valor: 0 }, { nome: "por aviar: ", valor: 0 });
                  return res.status(200).json(result);
                } else {
                  percPrescExp = numPrescExp / numTotalPresc * 100;
                  percPrescAviadas = numPrescAviadas / numTotalPresc * 100;
                  percPrescPorAviar = numPrescPorAviar / numTotalPresc * 100;
                  let totalPerc = percPrescExp + percPrescAviadas + percPrescPorAviar;

                  result.push({ nome: "aviadas: ", valor: parseInt(percPrescAviadas) }, { nome: "expiradas: ", valor: parseInt(percPrescExp) }, { nome: "por aviar: ", valor: parseInt(percPrescPorAviar) });
                  result.sort(function (a, b) { return b.valor - a.valor });

                  result[0].valor += (100 - (result[0].valor + result[1].valor + result[2].valor));
                  return res.status(200).json(result);
                }
              }
            });
          }

        });
      }
    });

  });

  module.exports = router;