var express = require('express');
var router = express.Router();
var axios = require('../auth/config_axios');

router.get('/', function (req, res, next) {
	return new Promise(function (resolve, reject) {
		axios.get('/farmaco')
			.then(function (response) {
				return res.status(200).send(response.data);
			}).catch(function (error) {
				return res.status(error.response.status).send({ erro: error.message });
			});
	});
});

router.get('/:id/apresentacoes', function (req, res, next) {
	return new Promise(function (resolve, reject) {
		axios.get('/farmaco/' + req.params.id + '/apresentacoes')
			.then(function (response) {
				return res.status(200).send(response.data);
			}).catch(function (error) {
				return res.status(error.response.status).send({ erro: error.message });
			});
	});
});

router.get('/:id/posologias', function (req, res, next) {
	return new Promise(function (resolve, reject) {
		axios.get('/farmaco/' + req.params.id + '/posologias')
			.then(function (response) {
				return res.status(200).send(response.data);
			}).catch(function (error) {
				return res.status(error.response.status).send({ erro: error.message });
			});
	});
});

module.exports = router;