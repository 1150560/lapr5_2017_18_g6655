var express = require('express');
var router = express.Router();
var axios = require('../auth/config_axios');

router.get('/:id', function (req, res, next) {

    return new Promise(function (resolve, reject) {
        axios.get('/medicamento/' + req.params.id + '/info')
            .then(function (response) {
                return res.status(200).send(response.data);

            }).catch(function (error) {
                return res.status(error.response.status).send({ erro: error.message });
            });
    });
});

/**
 * Sinkhole - return all medicamentos of gestao medicamentos
 */
router.get('/', function (req, res, next) {
    axios.get('/medicamento')
        .then(function (response) {
            return res.status(200).send(response.data);
        }).catch(function (error) {
            return res.status(error.response.status).send({ erro: error.message });
        });
});

/**
 * Sinkhole - return all medicamentos of apresentacao (connection with gestao medicamentos)
 */
router.get('/apresentacao/:id', function (req, res, next) {
    axios.get('/apresentacao/' + req.params.id + '/medicamentos')
        .then(response => {
            return res.status(202).send(response.data);
        })
        .catch(error => {
            return res.status(error.response.status).send({ message: error.message });
        })
})

module.exports = router;