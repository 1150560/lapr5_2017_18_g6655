var express = require('express');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config/environment');
var User = require('../models/user.js');
var Receita = require('../models/receita');
var VerifyToken = require('../auth/VerifyToken');
var speakeasy = require('speakeasy');
var VerifyRole = require('../roles/VerifyRole');
var router = express.Router();
var email = require('../email');
var Log = require('../models/log');

function dateIsValid(d) {
  if (Object.prototype.toString.call(d) === "[object Date]") {
    // it is a date
    if (isNaN(d.getTime())) {  // d.valueOf() could also work
      return false;
    }
    else {
      // date is valid
      return true;
    }
  }
  else {
    return false;
  }
}

/**
 * New created Log 
 */
function createLog(tipo, id) {
  var log = new Log();
  log.titulo = tipo + "de conta de utilizador";
  log.data = Date.now();
  if (tipo == "Registo") {
    log.texto = tipo + " do utilizador " + id;
  } else if (tipo == "Desativação") {
    log.texto = tipo + " da conta do utilizador " + id;
  }
  log.save(function (err) {
  });
};

router.post('/register', function (req, res) {
  var user = new User();
  var error = "";

  User.findOne({
    email: req.body.email
  }, function (err, user) {
    if (err) {
      return res.status(500).json({ err });
    }

    if (user) {
      error += "The email is already in use.";
      return res.status(400).json({ message: "Email is already in use!" });
    } else {

      if (!req.body.password) {
        return res.status(400).json({ message: "Password is required" });
      }
      if (!req.body.name) {
        error += "Name is required. ";
      }
      if (!req.body.email) {
        error += "Email is required. ";
      }
      if (typeof req.body.medico == 'undefined') {
        error += "Medico is required. ";
      }
      if (typeof req.body.farmaceutico == 'undefined') {
        error += "Farmaceutico is required. ";
      }

    }
  });

  user.name = req.body.name;
  user.password = bcrypt.hashSync(req.body.password, 8);
  user.email = req.body.email;
  user.medico = req.body.medico;
  user.farmaceutico = req.body.farmaceutico;

  if (user.medico && user.farmaceutico) {
    return res.status(400).json({ message: "The user can not be medico and farmaceutico at the same time." });
  } else if (error.length == 0) {
    user.save(function (err) {
      if (err) {
        return res.status(400).json({ message: err.message });
      }
      createLog("Registo" + user._id);
      res.status(201).json({ message: 'User registered' });
    })
  } else {
    return res.status(400).json({ message: error });
  }

});

router.post('/login', function (req, res) {
  User.findOne({
    email: req.body.email
  }, function (err, user) {
    if (err) {
      return;
    }
    if (!user) {
      return res.status(401).json({ sucess: false, message: 'Authentication failed. Email incorrect' });
    } else if (user) {
      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid) {
        return res.status(401).json({ auth: false, token: null, message: 'Authentication failed. Password incorrect' });
      } else if (!user.contaAtivada) {
        return res.status(403).json({ auth: false, token: null, message: 'Authentication failed. Account is deactivated' });
      } else {
        //create token
        const payload = {
          id: user._id,
          user: user.email,
          medico: user.medico,
          farmaceutico: user.farmaceutico,
          name: user.name,
          contaAtivada: user.contaAtivada,
          doisFatoresAtivado: user.doisFatoresAtivado
        };
console.log(config.tokenExpire);
console.log(config.secret);
        var token = jwt.sign(payload, config.secret, {
          expiresIn: config.tokenExpire
        });

        user.secret = speakeasy.generateSecret({ length: 10 }).base32;
        if (user.doisFatoresAtivado) {
          email.sendEmail(user.email, "Chave de autenticação", "Chave de autenticação: " + user.secret);
        }
        user.save(function (err) {
          if (err) {
            return res.status(400).json({ message: err.message });
          }
          return res.status(200).json({
            sucess: true,
            message: 'Enjoy your token',
            token: token
          });
        })

      }
    }
  });
});

router.post('/confirmarSecret', VerifyToken, function (req, res, next) {
  User.findOne({
    email: req.user
  }, function (err, user) {
    if (err) {
      return res.status(401).json({ message: 'Database error!' });
    }

    if (req.body.secret == user.secret) {
      return res.status(200).json({ message: 'Login Succeeded!' });
    } else {
      return res.status(401).json({ message: 'Login Failed!' });
    }
  });
});


router.put('/changePassword', VerifyToken, function (req, res, next) {
  User.findOne({
    email: req.user
  }, function (err, user) {
    if (err) {
      return res.status(401).json({ message: 'Database error!' });
    }
    var passwordIsValid = bcrypt.compareSync(req.body.oldPassword, user.password);
    if (!passwordIsValid) {
      return res.status(401).json({ message: 'Password antiga inválida!' });
    }
    user.password = bcrypt.hashSync(req.body.newPassword, 8);
    user.save(function (err) {
      if (err) {
        res.status(400).json({ message: 'Erro ao alterar password' });
      } else {
        res.status(200).json({ message: 'Password alterada com sucesso' });
      }
    })
  });
});

router.get('/:id/prescricao/poraviar/:data?', function (req, res) {
  Receita.find({ paciente: req.params.id }, function (err, receitas) {
    if (err) {
      return;
    } else {
      if (!receitas) {
        return res.status(404).json({ message: 'You have no Receitas' });
      } else {
        var receitasTamanho = receitas.length;
        var data = new Date(req.params.data);
        var dataAtual = new Date().setHours(0, 0, 0, 0);
        var prescricoes = [];
        for (var i = 0; i < receitasTamanho; i++) {
          var prescricoesTamanho = receitas[i].prescricoes.length;
          for (var j = 0; j < prescricoesTamanho; j++) {
            if (receitas[i].prescricoes[j].validade >= dataAtual) {
              if (dateIsValid(data) && data >= receitas[i].prescricoes[j].validade || typeof req.params.data == 'undefined') {
                var aviamentosTamanho = receitas[i].prescricoes[j].aviamentos.length;
                var quatidadePorAviar = receitas[i].prescricoes[j].quantidade;
                for (var k = 0; k < aviamentosTamanho; k++) {
                  quatidadePorAviar -= receitas[i].prescricoes[j].aviamentos[k].quantidade;
                }
                if (quatidadePorAviar > 0) {
                  prescricoes.push(receitas[i].prescricoes[j]);
                }
              }
            }
          }
        }
        return res.status(200).json(prescricoes);
      }
    }
  });
});


router.get('/:email', function (req, res) {
  User.findOne({ email: req.params.email }, function (err, user) {
    if (err) {
      return;
    } else {
      if (!user) {
        return res.status(404).json({ message: 'There is no User with this email' });
      } else {
        return res.status(200).json({ id: user._id });
      }
    }
  });
});

router.put('/deactivate', VerifyToken, function (req, res) {


  User.findOne({ email: req.user }, function (err, user) {
    if (err) throw err;

    if (!user) {
      return res.status(500).send("There was a problem deactivating the account. Account doesnt exist");
    }

    user.email = user._id;
    user.contaAtivada = false;
    user.name = "CONTA DESATIVADA";
    user.password = "XXXXX";
    user.secret = "XXXXX";

    user.save(function (err) {
      if (err) {
        return res.status(500).send("There was a problem deactivating the account.");
      } else {
        createLog("Desativação" + user._id);
        return res.status(200).json({ message: 'Conta desativada!' });
      }
    });
  });
});

router.put('/alterarDoisFatores', VerifyToken, function (req, res, next) {
  User.findOne({
    email: req.user
  }, function (err, user) {
    if (err) {
      return res.status(401).json(err.message);
    } else {
      if (!user.doisFatoresAtivado) {
        user.doisFatoresAtivado = true;
      } else {
        user.doisFatoresAtivado = false;
      }
    }
    user.save(function (err) {
      if (err) {
        return res.status(err.status).send(err.message);
      } else {
        return res.status(200).json({ message: 'Configurações alteradas.' });
      }
    });

  });

});


router.get('/doisFatores/isAtivo', VerifyToken, function (req, res, next) {
  User.findOne({
    email: req.user
  }, function (err, user) {
    if (err) {
      return res.status(401).json(err.message);
    } else {
      return res.status(200).json({ doisFatoresAtivado: user.doisFatoresAtivado });
    }
  });
});


router.get('/email/loggedUser', VerifyToken, function (req, res, next) {
  return res.status(200).json({ email: req.user });
});


router.put('/editUser', VerifyToken, function (req, res) {


  User.findOne({ email: req.user }, function (err, user) {
    if (err) throw err;

    if (!user) {
      return res.status(500).send("There was a problem editing the account. Account doesnt exist.");
    }

    user.email = req.body.email;
    user.name = req.body.name;


    user.save(function (err) {
      if (err) {
        return res.status(500).send("There was a problem editing the account.");
      } else {
        return res.status(200).json({ message: 'Conta editada!' });
      }
    });
  });
});


module.exports = router;
