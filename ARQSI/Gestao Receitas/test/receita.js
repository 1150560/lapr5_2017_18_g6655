let mongoose = require("mongoose");

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server;
let should = chai.should();
let Receita = require('../models/receita');
let Log = require('../models/log');
let User = require('../models/user');
var config = require('../config/environment');
let jwt = require('jsonwebtoken');
let assert = chai.assert;
let expect = chai.expect;
let app = require('../app');

chai.use(chaiHttp);
let user = [{
    name: "medico",
    email: "medico@gmail.com",
    password: "testes",
    medico: true,
    farmaceutico: false
}, {
    name: "farmaceutico",
    email: "farmaceuico@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: true
},
{
    name: "paciente",
    email: "paciente@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: false
},
{
    name: "paciente2",
    email: "paciente2@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: false
},
{
    name: "medico2",
    email: "medico2@gmail.com",
    password: "testes",
    medico: true,
    farmaceutico: false
},
{
    name: "farmaceutico2",
    email: "farmaceutico2@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: true
}];

let receitaId, prescricaoId, aviamentoId, receitaId2, prescricaoId2;
//Our parent block
describe('Receita', () => {

    before(() => {
        server = app.server;
    });


   /* after(() => {
       app.stop();
    });*/


    let tokenMedico, tokenPaciente, tokenFarmaceutico, tokenPaciente2, tokenMedico2, tokenFarmaceutico2;
    before(function (done) {
        // remover os tilizadores caso eles existam, so depois criar
        User.remove({}, (err) => {
            User.create(user, (err, us) => {
                tokenMedico = jwt.sign({ user: user[0].email }, config.secret, {
                    expiresIn: 86400
                });
                tokenFarmaceutico = jwt.sign({ user: user[1].email }, config.secret, {
                    expiresIn: 86400
                });
                tokenPaciente = jwt.sign({ user: user[2].email }, config.secret, {
                    expiresIn: 86400
                });
                tokenPaciente2 = jwt.sign({ user: user[3].email }, config.secret, {
                    expiresIn: 86400
                });
                tokenMedico2 = jwt.sign({ user: user[4].email }, config.secret, {
                    expiresIn: 86400
                });
                tokenFarmaceutico2 = jwt.sign({ user: user[5].email }, config.secret, {
                    expiresIn: 86400
                });
                user[0]._id = us[0]._id;
                user[1]._id = us[1]._id;
                user[2]._id = us[2]._id;
                user[3]._id = us[3]._id;
                user[4]._id = us[4]._id;
                user[5]._id = us[5]._id;
                done();
            });
        });
    });
    before((done) => { //Before each test we empty the database
        Receita.remove({}, (err) => {
            done();
        });
    });

    describe('/Post Receita', () => {
        afterEach((done) => { //after each test we empty the database
            Receita.remove({}, (err) => {
                Log.remove({}, (err) => {
                    done();
                });
            });
        });
        it('it should not POST Receita because its not medico', (done) => {
            chai.request(server)
                .post('/api/receita').set('x-acess-token', tokenPaciente)
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('You have no authorization. You must be a Medico');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });
        it('it should POST Receita sending a message to paciente and medico', (done) => {
            chai.request(server)
                .post('/api/receita').set('x-acess-token', tokenMedico)
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }],
                    'interacoes': []
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Receita and log created');
                    done();
                });
        });
        it('it should POST Receita sending a message to paciente and medico with interacoes', (done) => {
            chai.request(server)
                .post('/api/receita').set('x-acess-token', tokenMedico)
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }],
                    'interacoes': [{Farmaco1: 'Farmaco1'}, {Farmco2: 'Farmaco2'}]
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Receita and log created');
                    done();
                });
        });
    });


    // Test the /GET route
    describe('/GET Receita', () => {
        it('it should not GET all the Receitas because is a farmaceutico', (done) => {
            chai.request(server)
                .get('/api/receita').set('x-acess-token', tokenFarmaceutico)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Farmaceuticos cant read all Receitas');
                    done();
                });
        });
        it('it should not GET all the Receitas because is a paciente', (done) => {
            chai.request(server)
                .get('/api/receita').set('x-acess-token', tokenPaciente)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
        it('it should not GET the Receita with id', (done) => {
            chai.request(server)
                .get('/api/receita/1').set('x-acess-token', tokenPaciente)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    done();
                });
        });

    });


    // Test post prescricao
    describe('/Post Prescricao', () => {
        it('it should not POST the Prescricao because its not a medico', (done) => {
            chai.request(server)
                .post('/api/receita/' + receitaId + '/prescricao').set('x-acess-token', tokenFarmaceutico)
                .send({
                    "apresentacaoId": "1",
                    "posologiaId": "1",
                    "farmacoId": 1,
                    "quantidade": 5,
                    "validade": "11-11-18",
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('You have no authorization. You must be a Medico');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });

        it('it should not POST Prescricao, because its not a valid id', (done) => {
            chai.request(server)
                .post('/api/receita/123/prescricao').set('x-acess-token', tokenMedico)
                .send({
                    "apresentacaoId": "1",
                    "posologiaId": "1",
                    "farmacoId": 1,
                    "quantidade": 5,
                    "validade": "11-11-18"
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    done();
                });
        });
    });

    //Put Prescricao
    describe('/PUT Prescricao', () => {

        it('it should not Put Prescricao, posologiaId is required', (done) => {
            chai.request(server)
                .put('/api/receita/' + receitaId + '/prescricao/' + prescricaoId).set('x-acess-token', tokenMedico)
                .send({
                    "apresentacaoId": "1",
                    "farmacoId": 1,
                    "quantidade": 5,
                    "validade": "11-11-18"
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    done();
                });
        });
        it('it should not Put Prescricao, because its not a medico', (done) => {
            chai.request(server)
                .put('/api/receita/' + receitaId + '/prescricao/' + prescricaoId).set('x-acess-token', tokenPaciente)
                .send({
                    "apresentacaoId": "1",
                    "farmacoId": 1,
                    "quantidade": 5,
                    "validade": "11-11-18"
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('You have no authorization. You must be a Medico');
                    res.body.auth.should.be.eql(false);
                    done();
                });

        });


    });


    // GET Prescricoes
    describe('/GET Prescricao', () => {
        it('it should not GET Prescricao, id receita its not valid', (done) => {
            chai.request(server)
                .get('/api/receita/a/prescricoes').set('x-acess-token', tokenPaciente)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    done();
                });
        });
    });

    // Post of aviamentos
    describe('/Post Aviamento', () => {

        it('it should not POST Aviamento, because quantidade should be at least one', (done) => {
            chai.request(server)
                .post('/api/receita/' + receitaId + '/prescricao/' + prescricaoId + '/aviar').set('x-acess-token', tokenFarmaceutico)
                .send({
                    "quantidade": 0,
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    done();
                });
        });
        it('it should not POST Aviamento, because quantidade is required', (done) => {
            chai.request(server)
                .post('/api/receita/' + receitaId + '/prescricao/' + prescricaoId + '/aviar').set('x-acess-token', tokenFarmaceutico)
                .send({})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    done();
                });
        });

        it('it should not POST Aviamento, because its not a farmaceutico', (done) => {
            chai.request(server)
                .post('/api/receita/' + receitaId + '/prescricao/' + prescricaoId + '/aviar').set('x-acess-token', tokenMedico)
                .send({
                    "quantidade": 2,
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('You have no authorization. You must be a Farmaceutico');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });
    });

    // PUT of aviamentos
    describe('/PUT Aviamento', () => {
        it('it should not PUT Aviamento, because its not a farmaceutico', (done) => {
            chai.request(server)
                .put('/api/receita/' + receitaId + '/prescricao/' + prescricaoId + '/aviamento/' + aviamentoId).set('x-acess-token', tokenMedico)
                .send({
                    "quantidade": 2,
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('You have no authorization. You must be a Farmaceutico');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });
    });

    //Get Charts
    describe('/Get Farmacos Chart', () => {
        it('it should get farmacos chart', (done) => {
            chai.request(server)
                .get('/api/receita/farmacos/chart')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.length.should.be.eql(3);
                    done();
                });
        });
    });
    describe('/Get Receitas Por Dia Semana', () => {
        it('it should not get receitas because its not logged in', (done) => {
            chai.request(server)
                .get('/api/receita/PorDiaDaSemana/UltimoAno')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    done();
                });
        });
        it('it should not get receitas because its not medico', (done) => {
            chai.request(server)
                .get('/api/receita/PorDiaDaSemana/UltimoAno').set('x-acess-token', tokenPaciente)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql("You have no authorization. You must be a Medico");
                    done();
                });
        });
        it('it should get receitas', (done) => {
            chai.request(server)
                .get('/api/receita/PorDiaDaSemana/UltimoAno').set('x-acess-token', tokenMedico)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.length.should.be.eql(7);
                    done();
                });
        });
    });
    describe('/Get Formas Top Consumidas', () => {

        beforeEach((done) => {
            Receita.remove({}, (err) => {
                if (err) {
                    console.log('Error clearing Receita table');
                    process.exit();
                }

                let receita = new Receita({
                    "medico": user[0]._id,
                    "paciente": user[3]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    },
                    {
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                });

                receita.save(function (err) {
                    if (err) {
                        console.log('Error saving Receita');
                        process.exit();
                    }

                    done();
                });

            });

        });

        it('it should not get formas because its not logged in', (done) => {
            chai.request(server)
                .get('/api/receita/Formas/TopConsumidas')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    done();
                });
        });
        it('it should not get formas because its not paciente', (done) => {
            chai.request(server)
                .get('/api/receita/Formas/TopConsumidas').set('x-acess-token', tokenMedico)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql("You have no authorization. You must be a Paciente");
                    done();
                });
        });
        it('it should not get formas because paciente have no receitas', (done) => {
            chai.request(server)
                .get('/api/receita/Formas/TopConsumidas').set('x-acess-token', tokenPaciente)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql("Não tem receitas");
                    done();
                });
        });
        it('it should not get formas because paciente have no receitas', (done) => {
            chai.request(server)
                .get('/api/receita/Formas/TopConsumidas').set('x-acess-token', tokenPaciente2)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });

    describe('/Get PrescricoesInfo', () => {
        it('it should not get prescricoes because its not logged in', (done) => {
            chai.request(server)
                .get('/api/receita/prescricoesInfo/info')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    done();
                });
        });
        it('it should not get prescricoes because its not paciente', (done) => {
            chai.request(server)
                .get('/api/receita/prescricoesInfo/info').set('x-acess-token', tokenMedico)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql("You have no authorization. You must be a Paciente");
                    done();
                });
        });
        it('it should get prescricoes', (done) => {
            chai.request(server)
                .get('/api/receita/prescricoesInfo/info').set('x-acess-token', tokenPaciente)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.length.should.be.eql(3);
                    done();
                });
        });
    });
    describe('Logs', () => {

        beforeEach((done) => {
            Log.remove({}, (err) => {
                if (err) {
                    console.log('Error clearing Log table');
                    process.exit();
                }

                Receita.remove({}, (err) => {
                    if (err) {
                        console.log('Error clearing Receita table');
                        process.exit();
                    }
                    done();
                });
            });
        });

        describe('Log Create Receita', () => {

            it('it should log the event', (done) => {
                chai.request(server)
                    .post('/api/receita').set('x-acess-token', tokenMedico)
                    .send({
                        "paciente": user[2]._id,
                        "interacoes": [],
                        "prescricoes": [{
                            "farmacoId": 1,
                            "apresentacaoId": 1,
                            "posologiaId": 1,
                            "quantidade": 15,
                            "validade": "11-20-18"
                        },
                        {
                            "farmacoId": 1,
                            "apresentacaoId": 1,
                            "posologiaId": 1,
                            "quantidade": 15,
                            "validade": "11-20-18"
                        }]
                    })
                    .end((err, res) => {
                        res.should.have.status(201);
                        res.should.be.json;

                        Log.findOne({ titulo: 'Criar Receita' }, (err, log) => {
                            expect(log).to.not.be.null;
                            done();
                        });
                    });
            });
        });

        describe('Log Update Receita', () => {

            it('it should log the event', (done) => {
                let receita = new Receita({
                    "medico": user[0]._id,
                    "paciente": user[3]._id,
                    "interacoes": [],
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    },
                    {
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                });

                receita.save(function (err) {

                    receita.prescricoes[0].quantidade = 16;

                    chai.request(server)
                        .put('/api/receita/' + receita._id).set('x-acess-token', tokenMedico)
                        .send(receita)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.json;

                            Log.findOne({ titulo: 'Alterar Receita' }, (err, log) => {
                                expect(log).to.not.be.null;
                                done();
                            });
                        });
                });

            });
        });
    });

    describe('/Post CheckInteracaoDanosa', () => {
        afterEach((done) => { //after each test we empty the database
            Receita.remove({}, (err) => {
                Log.remove({}, (err) => {
                    done();
                });
            });
        });
        it('it should not return a list because user its not authenticated', (done) => {
            chai.request(server)
                .post('/api/receita/checkInteracaoDanosa')
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    },
                    {
                        "farmacoId": 2,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('No token provived.');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });
        it('it should not return a list because user its not a medico', (done) => {
            chai.request(server)
                .post('/api/receita/checkInteracaoDanosa').set('x-acess-token', tokenPaciente)
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    },
                    {
                        "farmacoId": 2,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('You have no authorization. You must be a medico');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });
        it('it should return a list with 1 interacao danosa', (done) => {
            chai.request(server)
                .post('/api/receita/checkInteracaoDanosa').set('x-acess-token', tokenMedico)
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    },
                    {
                        "farmacoId": 2,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.length.should.be.eql(1);
                    done();
                });
        });
        it('it should return a list an empty list of interacoes danosas', (done) => {
            chai.request(server)
                .post('/api/receita/checkInteracaoDanosa').set('x-acess-token', tokenMedico)
                .send({
                    "paciente": user[2]._id,
                    "prescricoes": [{
                        "farmacoId": 1,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    },
                    {
                        "farmacoId": 3,
                        "apresentacaoId": 1,
                        "posologiaId": 1,
                        "quantidade": 15,
                        "validade": "11-20-18"
                    }]
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.length.should.be.eql(0);
                    done();
                });
        });
    });

});
