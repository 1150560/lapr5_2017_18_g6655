let mongoose = require("mongoose");

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server;
let should = chai.should();
let Receita = require('../models/receita');
let Log = require('../models/log');
let User = require('../models/user');
var config = require('../config/environment');
let jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
let assert = chai.assert;
let expect = chai.expect;
let app = require('../app');

chai.use(chaiHttp);
let user =
    {
        name: "medico",
        email: "medico@gmail.com",
        password: "testes",
        medico: true,
        farmaceutico: false
    }

describe('Users', () => {

    before(() => {
        server = app.server;
    });


    after(() => {
        app.stop();
    });


    let token;

    describe('/Put AlterarDoisFatores', () => {

        beforeEach(function (done) {
            // remover os tilizadores caso eles existam, so depois criar
            User.remove({}, (err) => {
                User.create(user, (err, us) => {
                    token = jwt.sign({ user: user.email }, config.secret, {
                        expiresIn: 86400
                    });
                    user._id = us._id;
                    done();
                });
            });
        });
        it('it should not change doisFatores because its not login', (done) => {
            chai.request(server)
                .put('/api/users/alterarDoisFatores')
                .send({})
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.should.have.property('auth');
                    res.body.message.should.be.eql('No token provived.');
                    res.body.auth.should.be.eql(false);
                    done();
                });
        });
        it('it should not change doisFatores because its not login', (done) => {
            chai.request(server)
                .put('/api/users/alterarDoisFatores').set('x-acess-token', token)
                .send({})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Configurações alteradas.');
                    done();
                });
        });
    });
    describe('/Put Alterar dados utilizadores', () => {

        beforeEach(function (done) {
            // remover os tilizadores caso eles existam, so depois criar
            User.remove({}, (err) => {
                user.password = bcrypt.hashSync('testes', 8);
                User.create(user, (err, us) => {
                    token = jwt.sign({ user: user.email }, config.secret, {
                        expiresIn: 86400
                    });
                    user._id = us._id;
                    done();
                });
            });
        });
        it('It should change email and name of user', (done) => {
            chai.request(server)
                .put('/api/users/editUser').set('x-acess-token', token)
                .send({ email: 'email@teste', name: 'nameTeste' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Conta editada!');
                    User.findById(user._id, (error, user) => {
                        if (user) {
                            user.name.should.be.eql('nameTeste');
                            done();
                        }
                    })
                });
        });
        it('It should change password of user', (done) => {
            chai.request(server)
                .put('/api/users/changePassword').set('x-acess-token', token)
                .send({ oldPassword: 'testes' , newPassword: 'novaPassword' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Password alterada com sucesso');
                    done();
                });
        });
        it('It should not change password because old password is not valid', (done) => {
            chai.request(server)
                .put('/api/users/changePassword').set('x-acess-token', token)
                .send({ oldPassword: 'passwordTroll', newPassword: 'novaPassword' })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Password antiga inválida!');
                    done();
                });
        });
       
        it('it should dectivate user', (done) => {
            chai.request(server)
                .put('/api/users/deactivate').set('x-acess-token', token)
                .send({})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql('Conta desativada!');
                    User.findById(user._id, (error, user) => {
                        if (user) {
                            user.name.should.be.eql('CONTA DESATIVADA');
                            done();
                        }
                    })
                });
        });
    });
});