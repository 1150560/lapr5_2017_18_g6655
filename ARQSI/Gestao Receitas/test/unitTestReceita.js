let mongoose = require("mongoose");

//Require the dev-dependencies
let chai = require('chai');
let should = chai.should();
let Receita = require('../models/receita');
let User = require('../models/user');
let assert = chai.assert;
let expect = chai.expect;

let users = [{
    name: "medico",
    email: "medico@gmail.com",
    password: "testes",
    medico: true,
    farmaceutico: false
}, {
    name: "farmaceutico",
    email: "farmaceuico@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: true
},
{
    name: "paciente",
    email: "paciente@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: false
},
{
    name: "paciente2",
    email: "paciente2@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: false
},
{
    name: "medico2",
    email: "medico2@gmail.com",
    password: "testes",
    medico: true,
    farmaceutico: false
},
{
    name: "farmaceutico2",
    email: "farmaceutico2@gmail.com",
    password: "testes",
    medico: false,
    farmaceutico: true
}];
//Our parent block
describe('Unit Test of Receita', () => {
    before(function (done) {
        //todos os utilizadores com o seu id
        User.find(users, (err, us) => {
            users = us;
            done();
        });

    }); 
    it('should not allow to register a receita without medico or not existant medicos ', (done) => {
        let receita = new Receita();
        let validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.medico', 'Medico is required');

        receita.medico = "something";
        validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.medico', 'Medico should exists in data base');
        done();
    });
    it('should not allow to register a receita without paciente or not existant pacientes ', (done) => {
        let receita = new Receita();
        let validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.paciente', 'Paciente is required');

        receita.paciente = "something";
        validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.paciente', 'Paciente should exists in data base');

        done();
    });
    it('should not allow to register a receita without prescricoes', (done) => {
        let receita = new Receita();
        let validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.prescricoes', 'Prescricoes is required');
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": "1",
            "farmacoId": 1,
            "quantidade": 5,
            "validade": "11-11-18",
        };
        validator = receita.validateSync();
        assert.notNestedProperty(validator, 'errors.prescricoes', 'Prescricoes should be registered');
        done();
    });
    it('should not allow to register a receita with prescricoes that do not have apresentacaoId', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "posologiaId": "1",
            "farmacoId": 1,
            "quantidade": 5,
            "validade": "11-11-18",
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.apresentacaoId", "Prescricao should have apresentacaoId");
        done();
    });
    it('should not allow to register a receita with prescricoes that do not have posologiaId', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "farmacoId": 1,
            "quantidade": 5,
            "validade": "11-11-18",
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.posologiaId", "Prescricao should have posologiaId");
        done();
    });
    it('should not allow to register a receita with prescricoes that do not have farmacoId', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "quantidade": 5,
            "validade": "11-11-18",
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.farmacoId", "Prescricao should have farmacoId");
        done();
    });
    it('should not allow to register a receita with prescricoes that do not have quantidade', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "validade": "11-11-18",
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.quantidade", "Prescricao should have quantidade");
        done();
    });
    it('should not allow to register a receita with prescricoes that have quantidade less than one', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 0,
            "validade": "11-11-18",
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.quantidade", "Prescricao should have quantidade");
        done();
    });
    it('should not allow to register a receita with prescricoes that do not have validade', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 1,
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.validade", "Prescricao should have validade");
        done();
    });
    it('should not allow to register a receita with prescricoes that do not have a valid validade(must be current date)', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 1,
            "validade": "wrong date"
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.validade", "Prescricao should have a valid validade");

        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 1,
            "validade": "12-01-1997"
        };
        validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.validade", "Prescricao should have a validade (must be current)");

        let today = new Date();
        let validDate = today.setDate(today.getDate() + 1);
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 1,
            "validade": validDate
        };
        validator = receita.validateSync();
        assert.notProperty(validator.errors, "prescricoes.0.validade", "Prescricao should create with a valid date");
        done();
    });
    it('should not allow to register a receita with aviamentos that do not have farmaceutico or not existant farmaceuticos', (done) => {
        let receita = new Receita();
        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 1,
            "validade": "11-12-20",
            "aviamentos": [{}]
        };
        let validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.aviamentos.0.farmaceutico", "Farmaceutico is required in aviamento");

        receita.prescricoes = {
            "apresentacaoId": "1",
            "posologiaId": 1,
            "farmacoId": 1,
            "quantidade": 1,
            "validade": "11-12-20",
            "aviamentos": [{ "farmaceutico": "wrong" }]
        };
        validator = receita.validateSync();
        assert.property(validator.errors, "prescricoes.0.aviamentos.0.farmaceutico",
            "Aviamento should not create with farmaceutico that is not in data base");
        done();
    }); 
});
