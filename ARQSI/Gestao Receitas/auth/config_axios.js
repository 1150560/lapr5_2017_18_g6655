var axios = require('axios');
var account = { Email: "gestaoMedicamentos@isep.ipp.pt", Password: "Teste1!" };

var instance = axios.create({
	baseURL : 'https://apigestaomedicamentos.azurewebsites.net/api'
});

//Tenta criar uma conta mesmo que exista. Apos o registo, faz login guardando nos headers o token
function createUserAccount() {
	return new Promise(function (resolve, reject) {
		axios.post('https://apigestaomedicamentos.azurewebsites.net/api/account', account)
			.then(function (response) {
				resolve(loginUserAccount());
			}).catch(function (error) {
				resolve(loginUserAccount());
			});
	});
}

//Login da conta de gestao de medicamentos
function loginUserAccount() {
	return axios.post('https://apigestaomedicamentos.azurewebsites.net/api/account/token', account);
}

//Interceptor, quando o token expirar faz de novo o login
instance.interceptors.response.use(function (response) {
	return response;
}, function (error) {
	var originalConfig = error.config;
	if (error.response.status == 401) {
		return createUserAccount()
			.then(function(response){
				instance.defaults.headers.common['Authorization'] = "Bearer " + response.data.token;
				originalConfig.headers.Authorization = "Bearer " + response.data.token;
				return axios(originalConfig);
			});;		
	}
	return Promise.reject(error);
});
module.exports = instance;