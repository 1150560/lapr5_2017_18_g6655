var jwt = require('jsonwebtoken');
var config = require('../config/environment');

function verifyToken(req, res, next){
    var token = req.headers['x-acess-token'];
    if(!token){
        return res.status(401).send({
            auth: false, message: 'No token provived.'
        });
    }
        jwt.verify(token, config.secret, function(err, decoded){
             if(err){
                return res.status(500).send({
                    auth:false, message: 'Failed to autenticate token'
                });
            }
            req.user = decoded.user;
            next();
        });
    }
module.exports = verifyToken;