var Receitas = require('./models/receita');
var Users = require('./models/user');
var schedule = require('node-schedule');
var moment = require('moment');
var email = require('./email');
var socket = require('./socket');

var message = "Falta 1 dia para acabar o prazo de 1 prescrição da receita";

/**
 * Verifica se ainda existe medicamentos por aviar
 * @param {aviamentos} aviamentos 
 * @param {quantidade total de medicamentos que se pode aviar} quantidadeTotal 
 */
function verifyIfHasPrescriptions(aviamentos, quantidadeTotal) {
    let quantidadePrescrita = 0;
    let nAviamentos = aviamentos.length;
    for (let i = 0; i < nAviamentos; i++) {
        quantidadePrescrita += aviamentos[i].quantidade;
    }
    if (quantidadePrescrita < quantidadeTotal) {
        return true;
    }
    return false;
}

/**
 * Envia um email a notificar que falta 1 dia para aabar o prazo de uma prescrição
 * @param {id da receita} receitaId 
 * @param {id do user} userId 
 */
function sendEmail(receitaId, userId) {
    Users.findOne({ _id: userId }, function (error, user) {
        if (user) {
            email.sendEmail(user.email, 'Prazo de receitas', message + ' ' + receitaId);
        }
    });
}

/**
 * Envia uma notificação ao utilizador que falta 1 dia para aabar o prazo de uma prescrição
 * @param {id da receita} receitaId 
 * @param {id do user} userId 
 */
function sendNotification(receitaId, userId) {
    socket.notificar(userId, message + ' ' + receitaId);
}

/**
 * Vai verificar a cada 5 minutos se é necessário notificar um utilizador
 */
function notifications() {
    schedule.scheduleJob('*/5 * * * *', function () {
        Receitas.find({}, function (error, receitas) {
            if (receitas) {
                let nReceitas = receitas.length;
                for (let i = 0; i < nReceitas; i++) {
                    let prescricoes = receitas[i].prescricoes;
                    let nPrescricoes = prescricoes.length;
                    for (let j = 0; j < nPrescricoes; j++) {
                        let prescricao = prescricoes[j];
                        if (verifyIfHasPrescriptions(prescricao.aviamentos, prescricao.quantidade)) {
                            var diffDays = moment(prescricao.validade).diff(moment(), 'day');
                            if ((diffDays == 1 || diffDays == 0) && !receitas[i].prescricoes[j].notificado) {
                                sendEmail(receitas[i]._id, receitas[i].paciente);
                                sendNotification(receitas[i]._id, receitas[i].paciente);
                                receitas[i].prescricoes[j].notificado = true;
                                receitas[i].save(function (err) {
                                    if (err) {
                                        receitas[i].prescricoes[j].notificado = false;
                                    }
                                });
                            }
                        }
                    }
                }
            }
        });
    })
}

module.exports = {
    notifications: notifications
}