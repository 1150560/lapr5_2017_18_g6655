var nodemailer = require("nodemailer");
var config = require('./config/environment');

/**
 * Create smtp to send emails with configurations
 */
function createSmtp() {
    return nodemailer.createTransport({
        host: config.hostEmail,
        port: config.portEmail, // 8025, 587 and 25 can also be used. 
        auth: {
            user: config.userEmail,
            pass: config.passEmail
        },
        tls: { rejectUnauthorized: false }
    });
}

/**
 * Sen an email "from - to" with a subject an a text
 * @param {*from} fromEmail 
 * @param {*to} toEmail 
 * @param {*subject} subjectEmail 
 * @param {*text} textEmail 
 */
function sendEmail(toEmail, subjectEmail, textEmail) {
    let smtpTransport = createSmtp();
    smtpTransport.sendMail({
        from: config.fromEmail,
        to: toEmail,
        subject: subjectEmail,
        text: textEmail
    }, function (error, response) {
        if (error) {
            console.log('mensagem nao enviada');
        } else {
            console.log('mensagem enviada');
        }
    });
}

module.exports = {
    sendEmail: sendEmail
}