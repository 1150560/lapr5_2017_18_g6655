import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapaComponent } from 'app/mapa/mapa.component';

const routes: Routes = [
  {
    path: '',
    component: MapaComponent,
    data: {
      title: 'Mapa de Farmacias'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapaRoutingModule { }
