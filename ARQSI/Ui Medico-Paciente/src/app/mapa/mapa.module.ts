import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MapaRoutingModule } from 'app/mapa/mapa-routing.module';
import { MapaComponent } from 'app/mapa/mapa.component';

@NgModule({
  imports: [
    MapaRoutingModule,
  ],
  declarations: [ 
      MapaComponent
 ]
})
export class MapaModule { }
