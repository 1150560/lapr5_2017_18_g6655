import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AuthGuard } from './guards/auth.guards';
import { MedicoGuard } from './guards/medico.guards';
import { UtenteGuard } from './guards/utente.guards';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home/apresentacoes',
    pathMatch: 'full',
  },

  {
    path: 'home',
    component: FullLayoutComponent,
    data: {
      title: 'Lista de Apresentações'
    },
    children: [
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
      },
      {
        path: 'register',
        loadChildren: './register/register.module#RegisterModule'
      },
      {
        path: 'apresentacoes',
        loadChildren: './apresentacoes/apresentacoes.module#ApresentacoesModule'
      },
      {
        path: 'receitas',
        loadChildren: './receitas/receitas.module#ReceitasModule'
      },
      {
        path: 'termos',
        loadChildren: './termos/termos.module#TermosModule'
      },
      {
        path: 'estatisticas',
        loadChildren: './estatisticas/estatisticas.module#EstatisticasModule'
      },
      {
        path: 'mapa',
        loadChildren: './mapa/mapa.module#MapaModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
