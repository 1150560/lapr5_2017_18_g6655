/**
 * Horizontal bar graph
 */
export class Graph {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private gData: any = {
        margin: 40
    };

    private slices: any = [];
    private textSize = 16;
    private info: any;
    private mouse: any;
    readonly colors = [
        { color: '#5cbae6', colorHover: '#97D3EF' },
        { color: '#b6d957', colorHover: '#D0E694' },
        { color: '#fac364', colorHover: '#FBD89C' },
        { color: '#8cd3ff', colorHover: '#B5E3FF' },
        { color: '#d998cb', colorHover: '#E6BDDD' },
        { color: '#f2d249', colorHover: '#F6E28B' },
        { color: '#93b9c6', colorHover: '#BAD2DA' },
        { color: '#ccc5a8', colorHover: '#DEDAC7' },
        { color: '#52bacc', colorHover: '#90D3DE' },
        { color: '#dbdb46', colorHover: '#E8E889' },
        { color: '#98aafb', colorHover: '#BDC8FC' }
    ];


    constructor(canvas: HTMLCanvasElement, info: any, mouse: any) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.info = info;
        this.mouse = mouse;

        // Coordinates of graph bar area
        this.gData.left = this.gData.margin;
        this.gData.right = this.canvas.width - this.gData.margin;
        this.gData.top = this.gData.margin;
        this.gData.bottom = this.canvas.height - this.gData.margin;

        // Usable area
        this.gData.innerSize = this.canvas.height - this.gData.margin * 2;
        this.gData.centerX = this.gData.innerSize / 2 + this.gData.left;
        this.gData.centerY = this.gData.innerSize / 2 + this.gData.top;
        this.gData.radius = this.gData.innerSize / 2;
        this.gData.radiusHole = this.gData.radius * 0.5;

        for (let i = 0; i < this.info.length; i++) {
            let startAng;
            if (i == 0) {
                startAng = 0;
            } else {
                startAng = this.slices[i - 1].endAng;
            }
            let endAng = startAng + this.percentageToRad(this.info[i].percentagem);
            let startX = Math.cos(this.gData.startAng) * this.gData.radius;
            let startY = Math.sin(this.gData.startAng) * this.gData.radius;
            let endX = Math.cos(this.gData.endAng) * this.gData.radius;
            let endY = Math.sin(this.gData.endAng) * this.gData.radius;
            this.slices.push({
                startAng: startAng,
                endAng: endAng,
                percentagem: this.info[i].percentagem,
                quantidade: this.info[i].quantidade,
                nome: this.info[i].nome,
                color: this.colors[i % this.colors.length].color,
                colorHover: this.colors[i % this.colors.length].colorHover,
                startX: startX,
                startY: startY,
                endX: endX,
                endY: endY
            });
        }
    }

    percentageToRad(percentage) {
        return percentage * 2 * Math.PI / 100;
    }

    drawSlices() {
        this.slices.forEach(slice => {
            let hover = this.hover(slice);

            this.ctx.fillStyle = slice.color;
            if (hover) {
                this.ctx.fillStyle = slice.colorHover;
            }
            this.ctx.beginPath();
            this.ctx.moveTo(this.gData.centerX, this.gData.centerY);

            this.ctx.arc(this.gData.centerX, this.gData.centerY, this.gData.radius, slice.startAng, slice.endAng, false);
            this.ctx.strokeStyle = 'white';
            this.ctx.lineWidth = 5;
            this.ctx.fill();
            this.ctx.stroke();
        });
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.centerX, this.gData.centerY);
        this.ctx.arc(this.gData.centerX, this.gData.centerY, this.gData.radius, 0, 0, false);
        this.ctx.fill();
        this.ctx.stroke();
    }

    drawHover() {
        this.slices.forEach(slice => {
            // Draw hover rectangle
            if (this.hover(slice)) {
                let width = 170;
                let height = 80;

                let x = this.mouse.x;
                if (this.mouse.x + width > this.canvas.width) {
                    x = this.mouse.x - width;
                }


                this.ctx.strokeStyle = 'black';
                this.ctx.fillStyle = 'white';
                this.ctx.lineWidth = 1;
                this.ctx.fillRect(x, this.mouse.y - height, width, height);
                this.ctx.strokeRect(x, this.mouse.y - height, width, height);

                this.ctx.beginPath();
                this.ctx.fillStyle = 'black';
                this.ctx.textAlign = 'left';
                this.ctx.textBaseline = 'middle';
                this.ctx.font = this.textSize + 'px Arial';
                this.ctx.fillText('Forma: ' + slice.nome, x + 5, this.mouse.y - height * 3 / 4, width);
                this.ctx.fillText('Quantidade: ' + slice.quantidade, x + 5, this.mouse.y - height * 2 / 4, width);
                this.ctx.fillText('Percentagem: ' + slice.percentagem + '%', x + 5, this.mouse.y - height * 1 / 4, width);
            }
        });
    }

    drawHole() {
        this.ctx.beginPath();
        this.ctx.arc(this.gData.centerX, this.gData.centerY, this.gData.radiusHole, 0, Math.PI * 2, false);
        this.ctx.fillStyle = 'white';
        this.ctx.fill();
        this.ctx.stroke();
    }

    hover(slice) {
        if (!this.mouse) {
            return false;
        }

        let distance = Math.sqrt(Math.pow(this.mouse.x - this.gData.centerX, 2) + Math.pow(this.mouse.y - this.gData.centerY, 2));

        if (distance > this.gData.radius) {
            return false;
        }

        // Distance from hole
        if (distance < this.gData.radiusHole) {
            return false;
        }

        let angulo = this.calcularAngulo();

        if (slice.startAng > angulo || slice.endAng < angulo) {
            return false;
        }

        return true;
    }



    update() {
        this.drawSlices();
        this.drawHole();
        this.drawHover();
    }

    calcularAngulo() {
        // cos A = (b2 + c2 − a2) / 2bc
        if (this.mouse.y == this.gData.centerY) {
            if (this.mouse.x < this.gData.centerX) {
                return Math.PI;
            }
            return 0;
        }

        let distance = Math.sqrt(Math.pow(this.mouse.x - this.gData.centerX, 2) + Math.pow(this.mouse.y - this.gData.centerY, 2));
        let raio = this.gData.radius;
        let oposto = Math.sqrt(Math.pow(this.mouse.x - (this.gData.centerX + raio), 2) + Math.pow(this.mouse.y - this.gData.centerY, 2));

        let angulo = Math.acos((Math.pow(distance, 2) + Math.pow(raio, 2) - Math.pow(oposto, 2)) / (2 * distance * raio));

        if (this.mouse.y < this.gData.centerY) {
            return Math.PI * 2 - angulo;
        }
        return angulo;

    }

}