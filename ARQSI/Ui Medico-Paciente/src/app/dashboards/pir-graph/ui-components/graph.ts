/**
 * Horizontal bar graph
 */
export class Graph {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private gData: any = {
        leftMargin: 20,
        rightMargin: 20,
        topMargin: 20,
        bottomMargin: 40,
    };
    readonly pointRadius = 7;
    private info:any;
    private loading: boolean = true;
    private helperValues;

    readonly textMaxSize = 18;
    readonly colors = [
        '#5cbae6',
        '#b6d957',
        '#fac364'
    ];
    private mouse: any;

    constructor(canvas: HTMLCanvasElement, info: any, mouse: any) {
        this.info = info;
        this.info.values.unshift({nome:"value",valor:0});
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');

        this.loading = false;
        // Coordinates of graph pyramide
        this.gData.left = this.gData.leftMargin;
        this.gData.right = this.canvas.width - this.gData.rightMargin;
        this.gData.top = this.gData.topMargin;
        this.gData.bottom = this.canvas.height - this.gData.bottomMargin;


        // Usable area
        this.gData.innerWidth = this.canvas.width - this.gData.rightMargin - this.gData.leftMargin;
        this.gData.innerHeight = this.canvas.height - this.gData.bottomMargin - this.gData.topMargin;
        this.gData.middle = this.gData.innerWidth / 2 + this.gData.leftMargin;

    }
    drawPyramid() {

        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.bottom);
        this.ctx.lineTo(this.gData.middle, this.gData.top);
        this.ctx.lineTo(this.gData.right, this.gData.bottom);
        this.ctx.closePath();
        this.ctx.lineWidth = 2;
        this.ctx.strokeStyle = 'white';
        this.ctx.stroke();

    }

    calcCoordWithPerc(perc) {
        let y = this.gData.innerHeight * perc / 100;
        let x = y * this.gData.innerWidth / this.gData.innerHeight / 2;

        return { x: x, y: -y + this.gData.topMargin + this.gData.innerHeight }
    }

    drawLines() {
        let soma = 0;
        for (let i = 0; i < this.info.values.length - 1; i++) {
            
            soma += this.info.values[i].valor;
            let coord = this.calcCoordWithPerc(soma);
            this.ctx.beginPath();
            this.ctx.moveTo(coord.x + this.gData.left, coord.y);
            this.ctx.lineTo(this.gData.right - coord.x, coord.y);
            this.ctx.lineTo(this.gData.middle, this.gData.top);
            this.ctx.closePath();
            this.ctx.lineWidth = 2;
            this.ctx.strokeStyle = 'white';
            this.ctx.fillStyle = this.colors[i % this.colors.length];
            this.ctx.fill();
            this.ctx.stroke();

            //arc
            let arcX = this.gData.left + (i + 1.9) * 120 - 120;
            this.ctx.beginPath();
            this.ctx.arc(arcX -40, this.gData.bottom + 15, this.pointRadius, 0, Math.PI * 2, false);
            this.ctx.lineWidth = 3;
            this.ctx.strokeStyle = this.colors[i % this.colors.length];
            this.ctx.stroke();
            this.ctx.fill();

            // TEXT
            this.ctx.beginPath();
            this.ctx.fillStyle = 'black';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.font = 14 + 'px Arial';
            this.ctx.fillText(this.info.values[i + 1].nome + (this.info.values[i + 1].valor).toString()+ "%", arcX + 20, this.gData.bottom + 15);
        }

    }

    update() {
        if (this.loading == false) {
            this.drawPyramid();
            this.drawLines();
        }
    }


}