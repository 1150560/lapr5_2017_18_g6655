

/**
 * Horizontal bar graph
 */
export class Graph {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private gData: any = {
        leftMargin: 100,
        rightMargin: 20,
        topMargin: 80,
        bottomMargin: 80,
    };
    private info: any;
    private bars: any;
    private maxValue: number;
    private mouse: any;
    private barSpacing;

    readonly barToSpace = 0.40;
    readonly barMaxWidth = 50;
    readonly textMaxSize = 18;
    readonly colors = [
        { color: '#5cbae6', colorHover: '#97D3EF' },
        { color: '#b6d957', colorHover: '#D0E694' },
        { color: '#fac364', colorHover: '#FBD89C' },
        { color: '#8cd3ff', colorHover: '#B5E3FF' },
        { color: '#d998cb', colorHover: '#E6BDDD' },
        { color: '#f2d249', colorHover: '#F6E28B' },
        { color: '#93b9c6', colorHover: '#BAD2DA' },
        { color: '#ccc5a8', colorHover: '#DEDAC7' },
        { color: '#52bacc', colorHover: '#90D3DE' },
        { color: '#dbdb46', colorHover: '#E8E889' },
        { color: '#98aafb', colorHover: '#BDC8FC' }
    ];

    readonly nLines = 5;
    readonly speed = 6;
    readonly textSize = 16;

    constructor(canvas: HTMLCanvasElement, info: any, mouse: any) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.info = info;
        this.mouse = mouse;
        // Coordinates of graph bar area
        this.gData.left = this.gData.leftMargin;
        this.gData.right = this.canvas.width - this.gData.rightMargin;
        this.gData.top = this.gData.topMargin;
        this.gData.bottom = this.canvas.height - this.gData.bottomMargin;

        // Usable area
        this.gData.innerWidth = this.canvas.width - this.gData.rightMargin - this.gData.leftMargin;
        this.gData.innerHeight = this.canvas.height - this.gData.bottomMargin - this.gData.topMargin;

        // Dynamic bar width
        let barWidth = (this.gData.innerWidth) / (info.values.length + this.barToSpace * (info.values.length + 1));
        this.barSpacing = barWidth * this.barToSpace;
        let minValue = 0;
        this.maxValue = info.values[0].quantidadeTotal;
        info.values.forEach(item => {
            if (item.quantidadeTotal > this.maxValue) {
                this.maxValue = item.quantidadeTotal;
            }
        });

        if (this.maxValue == 0) {
            this.maxValue = this.nLines;
        }

        if (this.maxValue % this.nLines != 0) {
            this.maxValue = this.maxValue + this.nLines - this.maxValue % this.nLines;
        }

        // Dynamic bar height
        let scale = this.gData.innerHeight / this.maxValue;

        // Create Bars
        this.bars = [];
        for (let i = 0; i < info.values.length; i++) {
            let color = this.colors[i % this.colors.length].color;
            let colorHover = this.colors[i % this.colors.length].colorHover;
            let startPos = this.gData.left + barWidth * i + this.barSpacing * (i + 1);
            let targetHeight = info.values[i].quantidadeTotal * scale;
            this.bars.push({
                startX: startPos,
                barWidth: barWidth,
                targetHeight: targetHeight,
                currentHeight: 0,
                color: color,
                colorHover: colorHover,
                x: startPos + barWidth / 2,
                valorReal: info.values[i].quantidadeTotal,
                nome: info.values[i].nome
            });
        }
    }

    drawAxis() {
        this.ctx.lineWidth = 2;

        // YY AXIS
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.top);
        this.ctx.lineTo(this.gData.left, this.gData.bottom);
        this.ctx.strokeStyle = 'rgba(100, 100, 100, 0.7)';
        this.ctx.stroke();

        // XX AXIS
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.bottom);
        this.ctx.lineTo(this.gData.right, this.gData.bottom);
        this.ctx.strokeStyle = 'rgba(100, 100, 100, 0.7)';
        this.ctx.stroke();
    }

    drawXX() {
        // draw y title
        this.ctx.beginPath();
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';
        this.ctx.font = 'bold ' + this.textSize + 'px Arial';
        this.ctx.fillText(this.info.xTitle, this.gData.left + this.gData.innerWidth / 2 , this.gData.bottom+ 
            this.gData.bottomMargin * 3/4);

        for (let i = 0; i < this.bars.length; i++) {

            this.ctx.beginPath();
            this.ctx.moveTo(this.bars[i].x, this.gData.bottom - 5);
            this.ctx.lineTo(this.bars[i].x, this.gData.bottom + 5);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'black';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.fillStyle = 'black';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.font = this.textSize + 'px Arial';
            this.ctx.fillText(this.bars[i].nome, this.bars[i].x, this.gData.bottom + this.gData.bottomMargin / 4, this.bars[i].barWidth);

        };
    }


    drawYY() {
        // draw y title
        this.ctx.beginPath();
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';
        this.ctx.font = 'bold ' + this.textSize + 'px Arial';
        this.ctx.fillText(this.info.yTitle, this.gData.leftMargin / 2 , this.gData.topMargin / 2);
        
        for (let i = 1; i <= this.nLines; i++) {
            let y = this.gData.bottom - (this.gData.innerHeight * i) / this.nLines;
            let valorReal = (this.maxValue * i) / this.nLines;
            this.ctx.beginPath();
            this.ctx.moveTo(this.gData.left, y);
            this.ctx.lineTo(this.gData.right, y);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'rgba(150, 150, 150, 0.4)';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.moveTo(this.gData.left - 5, y);
            this.ctx.lineTo(this.gData.left + 5, y);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'black';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.fillStyle = 'black';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.font = this.textSize + 'px Arial';
            this.ctx.fillText((valorReal).toString(), this.gData.leftMargin / 2, y, this.gData.leftMargin);


        };
    }

    drawBars() {
        this.bars.forEach(bar => {
            this.ctx.fillStyle = bar.color;
            if (this.hover(bar)) {
                this.ctx.fillStyle = bar.colorHover;
            }
            this.ctx.fillRect(bar.startX, this.gData.bottom, bar.barWidth, -bar.currentHeight);
            if (bar.currentHeight < bar.targetHeight) {
                bar.currentHeight += this.speed;
                if (bar.currentHeight >= bar.targetHeight) {
                    bar.currentHeight = bar.targetHeight;
                }
            }
        });
    }

    hover(bar) {
        if (this.mouse.x < bar.startX || this.mouse.x > bar.startX + bar.barWidth) {
            return false;
        }

        if (this.mouse.y > this.gData.bottom || this.mouse.y < this.gData.bottom - bar.targetHeight) {
            return false;
        }
        return true;
    }

    drawHover() {
        this.bars.forEach(bar => {
            if (this.hover(bar)) {

                let width =120;
                let height = 24;

                this.ctx.strokeStyle = 'black';
                this.ctx.fillStyle = 'white';
                this.ctx.lineWidth = 1;
                this.ctx.fillRect(this.mouse.x, this.mouse.y - height, width, height);
                this.ctx.strokeRect(this.mouse.x, this.mouse.y - height, width, height);

                this.ctx.beginPath();
                this.ctx.fillStyle = 'black';
                this.ctx.textAlign = 'center';
                this.ctx.textBaseline = 'middle';
                this.ctx.font = this.textSize + 'px Arial';
                this.ctx.fillText('Quantidade: ' + bar.valorReal, this.mouse.x + width / 2, this.mouse.y - height / 2, width);
            }
        });
    }

    update() {
        this.drawXX();
        this.drawYY();
        this.drawBars();
        this.drawAxis();
        this.drawHover();

    }

}