/**
 * Horizontal bar graph
 */
export class Graph {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private gData: any = {
        leftMargin: 40,
        rightMargin: 80,
        topMargin: 40,
        bottomMargin: 40,
    };

    private textSize = 16;
    private info: any;
    private points: any = [];
    private maxValue;
    private sectionWidth;
    private mouse: any;
    readonly lineWidth = 5;
    readonly pointRadius = 7;
    readonly color = '#E71D36';
    readonly colorHover = '#F5A5AE';
    readonly nLinhas = 5;

    constructor(canvas: HTMLCanvasElement, info: any, mouse: any) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.info = info;
        this.mouse = mouse;

        // Coordinates of graph bar area
        this.gData.left = this.gData.leftMargin;
        this.gData.right = this.canvas.width - this.gData.rightMargin;
        this.gData.top = this.gData.topMargin;
        this.gData.bottom = this.canvas.height - this.gData.bottomMargin;

        // Usable area
        this.gData.innerWidth = this.canvas.width - this.gData.rightMargin - this.gData.leftMargin;
        this.gData.innerHeight = this.canvas.height - this.gData.bottomMargin - this.gData.topMargin;

        // Point area width
        this.sectionWidth = this.gData.innerWidth / info.values.length;

        this.maxValue = info.values[0].quantidade;
        info.values.forEach(item => {
            if (item.quantidade > this.maxValue) {
                this.maxValue = item.quantidade;
            }
        });
        if (this.maxValue == 0) {
            this.maxValue = this.nLinhas;
        }
        if (this.maxValue % this.nLinhas != 0) {
            this.maxValue = this.maxValue + this.nLinhas - this.maxValue % this.nLinhas;
        }
        let scaleY = this.gData.innerHeight / this.maxValue;

        // Create points
        for (let i = 0; i < info.values.length; i++) {
            let x = this.gData.left + (this.sectionWidth * (i + 1));
            let y = this.gData.bottom - info.values[i].quantidade * scaleY;
            this.points.push({ x: x, y: y, valorReal: info.values[i].quantidade });
        };
    }

    drawAxis() {
        this.ctx.lineWidth = 2;

        // YY AXIS
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.top);
        this.ctx.lineTo(this.gData.left, this.gData.bottom);
        this.ctx.strokeStyle = 'rgba(100, 100, 100, 0.7)';
        this.ctx.stroke();

        // XX AXIS
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.bottom);
        this.ctx.lineTo(this.gData.right, this.gData.bottom);
        this.ctx.strokeStyle = 'rgba(100, 100, 100, 0.7)';
        this.ctx.stroke();
    }

    drawPoints() {
        this.points.forEach(point => {

            let hover = this.hover(point);

            if (hover) {
                this.ctx.fillStyle = this.colorHover;

            } else {
                this.ctx.fillStyle = this.color;
            }

            this.ctx.beginPath();
            this.ctx.arc(point.x, point.y, this.pointRadius, 0, Math.PI * 2, false);
            this.ctx.lineWidth = 3;
            this.ctx.strokeStyle = 'black';
            this.ctx.stroke();
            this.ctx.fill();

            // Draw hover rectangle
            if (hover) {
                let width = 60;
                let height = 24;

                this.ctx.strokeStyle = 'black';
                this.ctx.fillStyle = 'white';
                this.ctx.lineWidth = 1;
                this.ctx.fillRect(this.mouse.x, this.mouse.y - height, width, height);
                this.ctx.strokeRect(this.mouse.x, this.mouse.y - height, width, height);

                this.ctx.beginPath();
                this.ctx.fillStyle = 'black';
                this.ctx.textAlign = 'center';
                this.ctx.textBaseline = 'middle';
                this.ctx.font = this.textSize + 'px Arial';
                this.ctx.fillText('Valor: ' + point.valorReal, this.mouse.x + width / 2, this.mouse.y - height / 2, width);
            }
        });
    }

    hover(point) {
        if (!this.mouse) {
            return false;
        }

        let distance = Math.sqrt(Math.pow(this.mouse.x - point.x, 2) + Math.pow(this.mouse.y - point.y, 2));

        if (distance > this.pointRadius) {
            return false;
        }

        return true;
    }


    drawXX() {
        for (let i = 0; i < this.points.length; i++) {
            this.ctx.beginPath();
            this.ctx.moveTo(this.points[i].x, this.gData.bottom);
            this.ctx.lineTo(this.points[i].x, this.gData.top);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'rgba(150, 150, 150, 0.4)';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.moveTo(this.points[i].x, this.gData.bottom - 5);
            this.ctx.lineTo(this.points[i].x, this.gData.bottom + 5);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'black';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.fillStyle = 'black';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.font = this.textSize + 'px Arial';
            this.ctx.fillText(this.info.values[i].dia, this.points[i].x, this.gData.bottom + this.gData.bottomMargin / 2, this.sectionWidth);

        };
    }


    drawYY() {
        for (let i = 1; i <= this.nLinhas; i++) {
            let y = this.gData.bottom - (this.gData.innerHeight * i) / this.nLinhas;
            let valorReal = (this.maxValue * i) / this.nLinhas;
            this.ctx.beginPath();
            this.ctx.moveTo(this.gData.left, y);
            this.ctx.lineTo(this.gData.right, y);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'rgba(150, 150, 150, 0.4)';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.moveTo(this.gData.left - 5, y);
            this.ctx.lineTo(this.gData.left + 5, y);
            this.ctx.lineWidth = 1;
            this.ctx.strokeStyle = 'black';
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.fillStyle = 'black';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.font = this.textSize + 'px Arial';
            this.ctx.fillText((valorReal).toString(), this.gData.leftMargin / 2, y, this.gData.leftMargin);


        };
    }

    drawLines() {
        for (let i = 0; i < this.points.length - 1; i++) {
            this.ctx.beginPath();
            this.ctx.moveTo(this.points[i].x, this.points[i].y);
            this.ctx.lineTo(this.points[i + 1].x, this.points[i + 1].y);
            this.ctx.lineWidth = this.lineWidth;
            this.ctx.strokeStyle = 'black';
            this.ctx.stroke();
        };
    }

    update() {
        this.drawAxis();
        this.drawXX();
        this.drawYY();
        this.drawLines();
        this.drawPoints();


    }

}