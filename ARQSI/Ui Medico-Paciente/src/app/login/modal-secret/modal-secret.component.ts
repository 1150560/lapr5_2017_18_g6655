import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
  selector: 'app-modal-secret',
  templateUrl: './modal-secret.component.html',
  styleUrls: ['./modal-secret.component.scss']
})
export class ModalSecretComponent implements OnInit {
  submit: boolean = false;
  confirmado: boolean = false;
  title: string = "Secret";
  chave:String;
  loading:boolean=false;

  constructor(public bsModalRef: BsModalRef,
    private authService: AuthenticationService) { }

  ngOnInit() {

  }
  postSecret() {
    this.loading=true;
    this.authService.postSecret(this.chave).subscribe(success => {
      this.submit = true
      this.confirmado = true;
      this.loading=false;
      this.bsModalRef.hide();
    }, error => {
      this.submit = true;
      this.confirmado = false;
      this.loading=false;
      this.bsModalRef.hide();
    });
  }

}
