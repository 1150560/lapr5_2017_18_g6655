import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSecretComponent } from './modal-secret.component';

describe('ModalSecretComponent', () => {
  let component: ModalSecretComponent;
  let fixture: ComponentFixture<ModalSecretComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSecretComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSecretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
