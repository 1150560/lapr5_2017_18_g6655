import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-modal-desativar',
    templateUrl: './modal-desativar.component.html',
    styleUrls: ['./modal-desativar.component.scss']
})
export class ModalDesativarComponent implements OnInit {
    private confirmation: boolean = false;
    constructor(public bsmodal: BsModalRef) { }


    ngOnInit() {
    }

    confirm(){
        this.confirmation = true;
    }

    decline(){
        this.confirmation = false;
    }



}