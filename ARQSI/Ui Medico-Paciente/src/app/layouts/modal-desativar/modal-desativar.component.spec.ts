import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDesativarComponent } from './modal-desativar.component';

describe('ModalDesativarComponent', () => {
  let component: ModalDesativarComponent;
  let fixture: ComponentFixture<ModalDesativarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDesativarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDesativarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});