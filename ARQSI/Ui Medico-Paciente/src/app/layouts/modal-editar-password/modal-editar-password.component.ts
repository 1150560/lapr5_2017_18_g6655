import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-modal-editar-password',
  templateUrl: './modal-editar-password.component.html',
  styleUrls: ['./modal-editar-password.component.scss']
})
export class ModalEditarPasswordComponent implements OnInit {
  public confirmarPassword: boolean = false;
  public oldPassword: string;
  public newPassword: string;
  public repeatPassword: string;
  public errorOldPassword: string;
  public errorNewPassword: string;
  public errorRepeatPassword: string;
  public isToSave: boolean = false;
  constructor(public bsModalRef: BsModalRef) { }


  ngOnInit() {
  }

  validarParaGravar() {
    if (this.oldPassword && this.newPassword && this.repeatPassword && this.newPassword == this.repeatPassword) {
      this.isToSave = true;
    } else {
      this.isToSave = false;
    }
  }

  oldEPassword() {
    if (!this.oldPassword) {
      this.errorOldPassword = "erro";
    } else {
      this.errorOldPassword = null;
    }
  }

  newEPassword() {
    if (!this.newPassword) {
      this.errorNewPassword = "erro";
    } else {
      this.errorNewPassword = null;
    }
  }

  repeatEPassword() {
    if(!this.repeatPassword || this.repeatPassword != this.newPassword){
      this.errorRepeatPassword = "erro";
    } else {
      this.errorRepeatPassword = null;
    }
  }

  fechar() {
    this.confirmarPassword = false;
  }

  guardar() {
    this.confirmarPassword = true;
  }

}
