import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditarPasswordComponent } from './modal-editar-password.component';

describe('ModalEditarPasswordComponent', () => {
  let component: ModalEditarPasswordComponent;
  let fixture: ComponentFixture<ModalEditarPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditarPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditarPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
