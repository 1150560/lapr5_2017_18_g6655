import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-modal-editar',
    templateUrl: './modal-editar.component.html',
    styleUrls: ['./modal-editar.component.scss']
})
export class ModalEditarComponent implements OnInit {
    public confirmationEditar: boolean = false;
    public nome : string;
    public email : string;
    public nomeOriginal : string;
    public emailOriginal : string;
    public isToSave : boolean = false;
    constructor(public bsModalRef: BsModalRef) { }


    ngOnInit() {
    }

    validarParaGravar(){
        if(this.nome && this.nome.length > 0 && this.email && this.email.length > 0 && (this.nome != this.nomeOriginal || this.email != this.emailOriginal)){
            this.isToSave = true;
        } else {
            this.isToSave = false;
        }

    }

    fechar(){

        this.confirmationEditar=false;

    }

    


    guardar(){
        this.confirmationEditar = true;
        
    }





}