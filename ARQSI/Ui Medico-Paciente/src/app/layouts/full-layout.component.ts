import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ToastrService, Toast } from 'ngx-toastr';
import { ModalDesativarComponent } from 'app/layouts/modal-desativar/modal-desativar.component';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalEditarComponent } from 'app/layouts/modal-editar/modal-editar.component';
import { ModalEditarPasswordComponent } from 'app/layouts/modal-editar-password/modal-editar-password.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit, OnDestroy {
  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };
  subscriptionAuth: Subscription;
  userInfo: User;
  mensagens: Array<string> = [];
  subscription: Subscription;
  subscriptionEditar : Subscription;
  subscriptionEditarPassword : Subscription;
  bsModalRef: BsModalRef;
  bsModalRef2: BsModalRef;
  bsModalRef3: BsModalRef;
  private doisFatoresAtivado: boolean;

  constructor(
    private authenticationService: AuthenticationService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) {}

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit() {
    this.userInfo = this.authenticationService.userInfo;
    if (!this.userInfo) {
      this.doisFatoresAtivado = false;
    } else {
      this.authenticationService.getDoisFatoresIsAtivo().subscribe(result => {
        this.doisFatoresAtivado = result.doisFatoresAtivado;
      }, err => {
        this.toastr.error("Erro ao carregar os dois fatores");
        this.doisFatoresAtivado = false;
      })
    }
    this.subscriptionAuth =
      this.authenticationService.authentication.subscribe((userInfo) => {
        this.userInfo = userInfo;
        this.cdr.detectChanges();
      });
    this.subscription = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef && this.bsModalRef.content.confirmation) {
        this.authenticationService.deactivate().subscribe(sucess => {
          this.toastr.success("Conta desativada com sucesso!");
          this.bsModalRef = null;
          this.authenticationService.logout();
          this.router.navigate(['/']);
        }, error => {
          this.bsModalRef = null;
          this.toastr.error("Erro ao desativar a conta!");
        }
        );
      }
    });

    this.subscriptionEditar = this.modalService.onHide.subscribe(reason => {
    if (this.bsModalRef2 && this.bsModalRef2.content.confirmationEditar) {
        this.authenticationService.editUser(this.bsModalRef2.content.nome,this.bsModalRef2.content.email).subscribe(sucess => {
          this.toastr.success("Conta editada com sucesso!");
          this.bsModalRef2 = null;
          this.authenticationService.logout();
          this.router.navigate(['/']);
        }, error => {
          this.bsModalRef2 = null;
          this.toastr.error("Erro ao editar a conta!");
        }
        );
      }
    });

    this.subscriptionEditarPassword = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef3 && this.bsModalRef3.content.confirmarPassword) {
          this.authenticationService.changePassword(this.bsModalRef3.content.oldPassword,this.bsModalRef3.content.newPassword).subscribe(sucess => {
            this.toastr.success("Password alterada com sucesso");
            this.bsModalRef3 = null;
          }, error => {
            this.toastr.error(error.error.message);
            this.bsModalRef3 = null;
          }
          );
        }
      });
  }

  notificar(msg) {
    this.toastr.warning(msg, null, { extendedTimeOut: 1000, timeOut: 10000, progressBar: true });
  }

  logout() {
    this.authenticationService.logout();
    this.mensagens = [];
    this.router.navigate(['/home/apresentacoes']);
  }

  termos() {
    this.router.navigate(['/home/termos']);
  }

  desativar() {
    this.bsModalRef = this.modalService.show(ModalDesativarComponent);
  }



  editar(){
    this.authenticationService.getEmailofLoggedUser().subscribe(success => {
      this.bsModalRef2 = this.modalService.show(ModalEditarComponent);
      this.bsModalRef2.content.nome = this.userInfo.name;
      this.bsModalRef2.content.email = success.email;
      this.bsModalRef2.content.nomeOriginal = this.userInfo.name;
      this.bsModalRef2.content.emailOriginal = success.email;
    }, error => {
      this.toastr.error("Não é possível editar a conta!");
    });
    
  }

  editarPassword(){
    this.bsModalRef3 = this.modalService.show(ModalEditarPasswordComponent);
  }

  alterarDoisFatoroes(event) {
    this.authenticationService.alterarDoisFatores().subscribe(result => {
      this.doisFatoresAtivado = !this.doisFatoresAtivado;
      if (this.doisFatoresAtivado) {
        this.toastr.success("Dois fatores ativado com sucesso");
      } else {
        this.toastr.success("Dois fatores desativado com sucesso");
      }
    }, err => {
      this.toastr.error("Configuração não foi efetuada");
    })
  }

  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
    this.subscription.unsubscribe();
    this.subscriptionEditar.unsubscribe();
  }
}
