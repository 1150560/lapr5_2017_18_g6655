import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ModalTermosComponent } from 'app/register/modal-termos/modal-termos.component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit, OnDestroy {
  model: any = {};
  loading = false;
  error = '';
  checkTermos: boolean = false;
  bsModalRef: BsModalRef;
  subscription: Subscription;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private modalService: BsModalService
  ) { }
  ngOnInit() {
    this.authenticationService.logout();
    this.activatedRoute.params.subscribe(params => {
      if (params['u'] !== undefined) {
        ;
        this.error = 'Your user cannot access lista apresentacoes';
      }
    });

    this.subscription = this.modalService.onHide.subscribe(reason => {
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  register() {
    if (this.model.password == this.model.password2) {
      this.loading = true;
      this.authenticationService.register(this.model.email,
        this.model.password, this.model.username)
        .subscribe(result => {
          this.loading = false;
          if (result === true) {
            this.router.navigate(['/home/login']);
          } else {
            this.error = "Email is already taken";
          }
        });
    }
  }

  abrirModal() {
    this.bsModalRef = this.modalService.show(ModalTermosComponent, { class: 'modal-lg' });
  }


}
