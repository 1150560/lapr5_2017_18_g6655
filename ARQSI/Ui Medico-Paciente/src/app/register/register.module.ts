import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { RegisterComponent } from './register.component';
import { RegisterRoutingModule } from './register-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TermosComponent } from 'app/termos/termos.component';


@NgModule({
  imports: [
    RegisterRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ RegisterComponent /*, TermosComponent */ ]
})
export class RegisterModule { }
