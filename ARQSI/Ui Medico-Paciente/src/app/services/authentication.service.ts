import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import * as jwt_decode from 'jwt-decode';
import { User } from '../models/user';
import { Router } from '@angular/router';

class Token { token: string };

@Injectable()
export class AuthenticationService {
  private authUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/login';
  private registerUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/register';
  private userUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users';
  private deactivateUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/deactivate';
  private alterarDoisFatoresUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/alterarDoisFatores';
  private getDoisFatoresUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/doisFatores/isAtivo';
  private editUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/editUser';
  private loggedUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users/email/loggedUser';
  //verificar o servidor:porta
  public userInfo: User;
  public erro: any;
  authentication: Subject<User> = new Subject<User>();

  constructor(private http: HttpClient, private router: Router, ) {
    this.userInfo = localStorage.userInfo && JSON.parse(localStorage.userInfo);
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }

  login(email: string, password: string): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.http.post<Token>(this.authUrl, {
        email: email,
        password: password
      })
        .subscribe(data => {
          if (data.token) {
            const tokenDecoded = jwt_decode(data.token);
            if (!tokenDecoded.farmaceutico) {
              this.userInfo = {
                id: tokenDecoded.id,
                token: data.token,
                tokenExp: tokenDecoded.exp,
                name: tokenDecoded.name,
                medico: tokenDecoded.medico,
                farmaceutico: tokenDecoded.farmaceutico,
                contaAtivada: tokenDecoded.contaAtivada,
                doisFatoresAtivado: tokenDecoded.doisFatoresAtivado,
              }
              localStorage.userInfo = JSON.stringify(this.userInfo);

              this.authentication.next(this.userInfo);
              observer.next(true);
            } else {
              this.authentication.next(this.userInfo);
              observer.next(false);
            }
          } else {
            this.authentication.next(this.userInfo);
            observer.next(false);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("Client-side error occured.");
          } else {
            console.log("Server-side error occured.");
          }
          console.log(err);
          this.authentication.next(this.userInfo);
          observer.next(false);
        });

    });
  }
  //só se regista como utente
  register(email: string, password: string, username: string): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.http.post(this.registerUrl, {
        name: username,
        email: email,
        password: password,
        medico: false,
        farmaceutico: false,
        contaAtivada: true
      })
        .subscribe(data => {
          observer.next(true);
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
          } else {
          }
          console.log(err);
          this.erro = err.statusText;
          this.authentication.next(this.userInfo);
          observer.next(false);
        });

    });
  }

  deactivate(): Observable<any> {
    return this.http.put<any>(this.deactivateUrl, {}, this.getHeaders());
  }

  editUser( name : string, email : string): Observable<any> {
    return this.http.put<any>(this.editUrl, {
      email : email,
      name : name
    }, this.getHeaders());
  }


  logout() {
    this.userInfo = null;
    localStorage.removeItem('userInfo');
    this.authentication.next(this.userInfo);
  }

  getEmailofLoggedUser(){
    return this.http.get<any>(this.loggedUrl, this.getHeaders());
  }

  getUserByEmail(email: string): Observable<any> {
    const url = `${this.userUrl}/${email}`;
    return this.http.get<any>(url);
  }
  postSecret(segredo: String) {
    const url = `${this.userUrl}/confirmarSecret`;
    return this.http.post<any>(url, {
      secret: segredo
    }, this.getHeaders());
  }

  alterarDoisFatores() {
    return this.http.put<any>(this.alterarDoisFatoresUrl, {}, this.getHeaders());
  }

  getDoisFatoresIsAtivo() {
    return this.http.get<any>(this.getDoisFatoresUrl, this.getHeaders());
  }

  changePassword(oldPassword1: string, newPassword1: string){
    const url = `${this.userUrl}/changePassword`;
    return this.http.put<any>(url, {oldPassword: oldPassword1, newPassword: newPassword1}, this.getHeaders());
  }
}
