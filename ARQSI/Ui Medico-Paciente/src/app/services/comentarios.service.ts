import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from 'app/services/authentication.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ComentariosService {

  private comentariosUrl = 'https://apigestaoreceitas.azurewebsites.net/api/comentarios';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }


  postComentario(comentario: any): Observable<any> {
    return this.http.post<any>(this.comentariosUrl, comentario, this.getHeaders());
  }
  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
