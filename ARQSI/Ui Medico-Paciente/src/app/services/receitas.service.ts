import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Receita } from '../models/receita';
import { AuthenticationService } from './authentication.service';
@Injectable()
export class ReceitasService {
  private receitasUrl = 'https://apigestaoreceitas.azurewebsites.net/api/receita';

  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  getReceitas(): Observable<Receita[]> {
    return this.http.get<Receita[]>(this.receitasUrl,
      this.getHeaders());
  }

  getTopFarmacosVendidos(): Observable<Receita[]> {
    const url = `${this.receitasUrl}/farmacos/chart`;
    return this.http.get<Receita[]>(url);
  }

  getReceita(idReceita: string): Observable<Receita> {
    const url = `${this.receitasUrl}/${idReceita}`;
    return this.http.get<Receita>(url, this.getHeaders());
  }

  postAviamento(idReceita: string, idPrescricao: string, aviamento: any): Observable<any> {
    const url = `${this.receitasUrl}/${idReceita}/prescricao/${idPrescricao}/aviar`;
    return this.http.post<any>(url, aviamento, this.getHeaders());
  }

  getAviamentosOfPrescricao(idReceita: string, idPrescricao: string): Observable<any> {
    const url = `${this.receitasUrl}/${idReceita}/prescricao/${idPrescricao}/aviamentos`;
    return this.http.get<any>(url, this.getHeaders());
  }

  getInteracoesDanosas(receita: any): Observable<any> {
    const url = `${this.receitasUrl}/checkInteracaoDanosa`;
    return this.http.post<any>(url, receita, this.getHeaders());
  }
  postReceita(receita: any): Observable<any> {
    return this.http.post<any>(this.receitasUrl, receita, this.getHeaders());
  }

  postPrescricao(receitaId: any, prescricao: any): Observable<any> {
    const url = `${this.receitasUrl}/${receitaId}/prescricao/`;
    return this.http.post<any>(url, prescricao, this.getHeaders());
  }

  putReceita(receita: any): Observable<any> {
    const url = `${this.receitasUrl}/${receita._id}`;
    return this.http.put<any>(url, receita, this.getHeaders());
  }

  getReceitasPorDiaDaSemana(): Observable<any> {
    const url = `${this.receitasUrl}/PorDiaDaSemana/UltimoAno`;
    return this.http.get<any>(url, this.getHeaders());
  }

  getPrescricoesInfo(): Observable<any> {
    const url = `${this.receitasUrl}/prescricoesInfo/info`;
    return this.http.get<any>(url, this.getHeaders());
  }

  getFormasConsumidas(): Observable<any> {
    const url = `${this.receitasUrl}/formas/topconsumidas`;
    return this.http.get<any>(url, this.getHeaders());
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
