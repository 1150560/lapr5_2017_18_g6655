import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Apresentacao } from '../models/apresentacao';

@Injectable()
export class ApresentacoesService {

  private apresentacoesURL = 'https://apigestaoreceitas.azurewebsites.net/api/apresentacoes';
  //verificar o servidor:porta
  constructor(private http: HttpClient) { }

  getApresentacoes(): Observable<Apresentacao[]> {
    return this.http.get<Apresentacao[]>(this.apresentacoesURL);
  }

  getComentarios(id: any): Observable<any[]> {
    const url = `${this.apresentacoesURL}/${id}/comentarios`;
    return this.http.get<any[]>(url);
  }
}
