import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule, ModalModule, BsDatepickerModule, defineLocale, ptBr } from 'ngx-bootstrap';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';


import { ChartsModule } from 'ng2-charts/ng2-charts';

import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// Routing Module
import { AppRoutingModule } from './app.routing';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';


//services
import { ReceitasService } from './services/receitas.service';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './guards/auth.guards';
import { MedicoGuard } from './guards/medico.guards';
import { UtenteGuard } from './guards/utente.guards';
import { ApresentacoesService } from './services/apresentacoes.service';
import { FarmacosService } from './services/farmacos.service';
import { ModalReceitaComponent } from 'app/receitas/medico/modal-receita/modal-receita.component';
import { ModalComentarioComponent } from 'app/receitas/medico/modal-comentario/modal-comentario.component';
import { ComentariosService } from 'app/services/comentarios.service';
import { ModalListaComentariosComponent } from 'app/receitas/medico/modal-lista-comentarios/modal-lista-comentarios.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalListaPosologiasComponent } from 'app/receitas/paciente/modal-lista-posologias/modal-lista-posologias.component';
import { SharedModule } from 'app/shared/shared.module';
import { ModalTermosComponent } from 'app/register/modal-termos/modal-termos.component';
import { ModalSecretComponent } from 'app/login/modal-secret/modal-secret.component';
import { ModalDesativarComponent } from 'app/layouts/modal-desativar/modal-desativar.component';
import { EncryptionInterceptor } from 'app/interceptors/encryption.interceptor';
import { ModalEditarComponent } from 'app/layouts/modal-editar/modal-editar.component';
import { ModalInteracoesDanosasComponent } from 'app/receitas/medico/modal-interacoes-danosas/modal-interacoes-danosas.component';
import { ModalEditarPasswordComponent } from 'app/layouts/modal-editar-password/modal-editar-password.component';

defineLocale('en', ptBr);
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    ModalReceitaComponent,
    ModalComentarioComponent,
    ModalListaComentariosComponent,
    ModalListaPosologiasComponent,
    ModalTermosComponent,
    ModalSecretComponent,
    ModalDesativarComponent,
    ModalEditarComponent,
    ModalInteracoesDanosasComponent,
    ModalEditarPasswordComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: EncryptionInterceptor,
    multi: true,
  },
    AuthGuard,
    MedicoGuard,
    UtenteGuard,
    AuthenticationService,
    ReceitasService,
    ApresentacoesService,
    FarmacosService,
    ComentariosService],
  entryComponents: [ModalReceitaComponent, ModalComentarioComponent,
    ModalListaComentariosComponent, ModalListaPosologiasComponent,
    ModalTermosComponent, ModalSecretComponent, ModalDesativarComponent,
    ModalEditarComponent, ModalInteracoesDanosasComponent, ModalEditarPasswordComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
