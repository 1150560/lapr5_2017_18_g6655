import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { TermosComponent } from 'app/termos/termos.component';

const routes: Routes = [
  {
    path: '',
    component: TermosComponent,
    data: {
      title: 'Termos'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermosRoutingModule {}
