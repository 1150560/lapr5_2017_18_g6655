import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { TermosComponent } from 'app/termos/termos.component';
import { SharedModule } from '../shared/shared.module';
import { TermosRoutingModule } from 'app/termos/termos-routing.module';

@NgModule({
  imports: [

    TermosRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ TermosComponent ]
})
export class TermosModule { }
