import * as CryptoJS from 'crypto-js';
import { environment } from '../../environments/environment';

 class EncryptionHandler {
    /**
     * Encrypt message using AES256 algorithm (key is in environment) 
     * @param message message to be encrypted
     */
    encryptMessage(message: string) {
        return CryptoJS.AES.encrypt(message, environment.key).toString();
    }
    /**
     * Decrypt message using AES256 algorithm (key is in environment)
     * @param message message to be decrypted
     */
    decryptMessage(message: string) {
        return CryptoJS.AES.decrypt(message, environment.key).toString(CryptoJS.enc.Utf8);
    }
}

export { EncryptionHandler };