import { Component, OnInit } from '@angular/core';
import { ApresentacoesService } from '../services/apresentacoes.service';
import { Apresentacao } from '../models/apresentacao';

@Component({
  selector: 'app-apresentacoes',
  templateUrl: './apresentacoes.component.html',
})
export class ApresentacoesComponent implements OnInit {
  apresentacoes: Apresentacao[] = [];
  constructor(private apresentacoesService: ApresentacoesService) { }

  ngOnInit() {
    this.apresentacoesService.getApresentacoes().subscribe(apresentacoes => {
      this.apresentacoes = apresentacoes;
    })
  }

}
