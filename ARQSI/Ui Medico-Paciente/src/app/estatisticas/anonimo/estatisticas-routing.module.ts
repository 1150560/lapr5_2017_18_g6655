import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnonimoComponent } from './anonimo.component';

const routes: Routes = [
  {
    path: '',
    component: AnonimoComponent,
    data: {
      title: 'Estatisticas'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstatisticasAnonimoRoutingModule { }
