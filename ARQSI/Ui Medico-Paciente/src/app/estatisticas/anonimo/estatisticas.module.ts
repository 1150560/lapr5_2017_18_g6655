import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { EstatisticasAnonimoRoutingModule } from './estatisticas-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    EstatisticasAnonimoRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ 
 ]
})
export class EstatisticasAnonimoModule { }
