import { Component, OnInit } from '@angular/core';
import { ReceitasService } from 'app/services/receitas.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-anonimo',
  templateUrl: './anonimo.component.html',
  styleUrls: ['./anonimo.component.scss']
})
export class AnonimoComponent implements OnInit {

  public infoColumnGraph: any;

  constructor(private receitaService: ReceitasService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.receitaService.getTopFarmacosVendidos().subscribe(success => {
      this.infoColumnGraph = {
        xTitle: 'Farmacos',
        yTitle: 'Quantidade',
        values: success

      }
    }, error => {
      this.toastr.error("Erro a obter informações");

    });
  }

}
