import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { EstatisticasMedicoComponent } from './estatisticas.component';
import { EstatisticasMedicoRoutingModule } from './estatisticas-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { LineGraphComponent } from 'app/dashboards/line-graph/line-graph.component';
@NgModule({
  imports: [
    EstatisticasMedicoRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ 
      EstatisticasMedicoComponent,
      LineGraphComponent
 ]
})
export class EstatisticasMedicoModule { }
