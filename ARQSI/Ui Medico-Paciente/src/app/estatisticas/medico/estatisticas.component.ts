import { Component, OnInit } from '@angular/core';
import { ReceitasService } from 'app/services/receitas.service';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-estatisticas',
  templateUrl: './estatisticas.component.html',
  styleUrls: ['./estatisticas.component.scss']
})
export class EstatisticasMedicoComponent implements OnInit {

  public infoLineGraph: any;

  constructor(private receitaService: ReceitasService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.receitaService.getReceitasPorDiaDaSemana().subscribe(success => {
      this.infoLineGraph = {
        xTitle: 'Dias da Semana',
        yTitle: 'Quantidade',
        values: success
      }
    }, error => {
      this.toastr.error("Erro a obter informações");

    });
  }

}
