import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstatisticasMedicoComponent } from './estatisticas.component';
import { LineGraphComponent } from 'app/dashboards/line-graph/line-graph.component';
const routes: Routes = [
  {
    path: '',
    component: EstatisticasMedicoComponent,
    data: {
      title: 'Estatisticas'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstatisticasMedicoRoutingModule { }
