import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstatisticasMedicoComponent } from './estatisticas.component';

describe('EstatisticasComponent', () => {
  let component: EstatisticasMedicoComponent;
  let fixture: ComponentFixture<EstatisticasMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstatisticasMedicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstatisticasMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
