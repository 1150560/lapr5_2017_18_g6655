import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SharedModule } from '../shared/shared.module';

import { EstatisticasRoutingModule } from 'app/estatisticas/estatisticas-routing.module';

@NgModule({
  imports: [
    EstatisticasRoutingModule,
    SharedModule
  ],
  declarations: []
})
export class EstatisticasModule { }
