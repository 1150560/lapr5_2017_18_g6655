import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { AuthGuard } from 'app/guards/auth.guards';
import { MedicoGuard } from 'app/guards/medico.guards';
import { UtenteGuard } from 'app/guards/utente.guards';

export const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Estatisticas'
    },
    children: [ 
      {
        path: 'medico',
        loadChildren: './medico/estatisticas.module#EstatisticasMedicoModule',
        canActivate: [AuthGuard, MedicoGuard]
      },
      {
        path: 'paciente',
        loadChildren: './paciente/estatisticas.module#EstatisticasPacienteModule',
        canActivate: [AuthGuard, UtenteGuard]
      },
      {
        path: 'anonimo',
        loadChildren: './anonimo/estatisticas.module#EstatisticasAnonimoModule',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstatisticasRoutingModule { }
