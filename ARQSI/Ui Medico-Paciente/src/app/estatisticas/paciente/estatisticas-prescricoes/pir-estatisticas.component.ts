import { Component, OnInit } from '@angular/core';
import { PirGraphComponent } from 'app/dashboards/pir-graph/pir-graph.component';
import { ReceitasService } from 'app/services/receitas.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pir-estatisticas',
  templateUrl: './pir-estatisticas.component.html'
})
export class PirEstatisticasComponent implements OnInit {

  public infoGraph: any;
  public canContinue: boolean = false;

  constructor(
    private receitaService: ReceitasService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {

    this.receitaService.getPrescricoesInfo().subscribe(success => {
      success.forEach(element => {
        if (element.valor > 0) {
          this.canContinue = true;
        }
      });
      this.infoGraph = {
        values: success
      }
    }, error => {

      this.toastr.error('Erro a obter dados estatísticos');
    });

  }

}
