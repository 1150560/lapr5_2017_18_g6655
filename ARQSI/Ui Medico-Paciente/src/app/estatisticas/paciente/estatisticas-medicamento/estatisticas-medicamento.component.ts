import { Component, OnInit } from '@angular/core';
import { ReceitasService } from 'app/services/receitas.service';

@Component({
  selector: 'app-estatisticas-medicamento',
  templateUrl: './estatisticas-medicamento.component.html',
  styleUrls: ['./estatisticas-medicamento.component.scss']
})
export class EstatisticasMedicamentoComponent implements OnInit {

  public info: any = [];

  constructor(private receitaService: ReceitasService) { }

  ngOnInit() {
    this.receitaService.getFormasConsumidas().subscribe(success => {
      this.info = success;
    }, error =>{
      this.info = [];
    });
  }

}
