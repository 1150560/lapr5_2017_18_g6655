import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstatisticasMedicamentoComponent } from './estatisticas-medicamento.component';

describe('EstatisticasMedicamentoComponent', () => {
  let component: EstatisticasMedicamentoComponent;
  let fixture: ComponentFixture<EstatisticasMedicamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstatisticasMedicamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstatisticasMedicamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
