import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstatisticasPacienteComponent } from './estatisticas.component';

describe('PacienteComponent', () => {
  let component: EstatisticasPacienteComponent;
  let fixture: ComponentFixture<EstatisticasPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstatisticasPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstatisticasPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
