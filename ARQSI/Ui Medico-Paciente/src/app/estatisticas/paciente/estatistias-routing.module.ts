import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstatisticasPacienteComponent } from './estatisticas.component';
import { DonutGraphComponent } from 'app/dashboards/donut-graph/donut-graph.component';

const routes: Routes = [
  {
    path: '',
    component: EstatisticasPacienteComponent,
    data: {
      title: 'Estatisticas'
    }
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstatisticasPacienteRoutingModule { }
