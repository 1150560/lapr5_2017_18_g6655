import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { EstatisticasPacienteComponent } from './estatisticas.component';
import { SharedModule } from '../../shared/shared.module';
import { PirEstatisticasComponent } from 'app/estatisticas/paciente/estatisticas-prescricoes/pir-estatisticas.component';
import { EstatisticasPacienteRoutingModule } from 'app/estatisticas/paciente/estatistias-routing.module';
//import { PirEstatisticasRoutingModule } from 'app/estatisticas/paciente/estatisticas-prescricoes/pir-estatisticas-routing.module';
@NgModule({
  imports: [
    EstatisticasPacienteRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ 
      EstatisticasPacienteComponent
 ]
})
export class EstatisticasPacienteModule { }
