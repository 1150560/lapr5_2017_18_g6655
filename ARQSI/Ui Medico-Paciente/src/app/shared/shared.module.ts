import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormateDatePipe } from 'app/formate-date.pipe';
import { HideMessagePipe } from 'app/hide-message.pipe';
import { EstatisticasMedicamentoComponent } from 'app/estatisticas/paciente/estatisticas-medicamento/estatisticas-medicamento.component';
import { DonutGraphComponent } from 'app/dashboards/donut-graph/donut-graph.component';
import { PirEstatisticasComponent } from 'app/estatisticas/paciente/estatisticas-prescricoes/pir-estatisticas.component';
import { PirGraphComponent } from 'app/dashboards/pir-graph/pir-graph.component';
import { AnonimoComponent } from 'app/estatisticas/anonimo/anonimo.component';
import { ColumnGraphComponent } from 'app/dashboards/column-graph/column-graph.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [FormateDatePipe, HideMessagePipe,EstatisticasMedicamentoComponent,DonutGraphComponent,PirEstatisticasComponent,PirGraphComponent, AnonimoComponent, ColumnGraphComponent],
  exports: [CommonModule, FormsModule, FormateDatePipe, HideMessagePipe,EstatisticasMedicamentoComponent,DonutGraphComponent,PirEstatisticasComponent, PirGraphComponent, AnonimoComponent, ColumnGraphComponent]
})
export class SharedModule { }
