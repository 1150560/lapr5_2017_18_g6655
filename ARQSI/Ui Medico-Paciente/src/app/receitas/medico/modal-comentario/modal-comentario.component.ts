import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-modal-comentario',
  templateUrl: './modal-comentario.component.html',
  styleUrls: ['./modal-comentario.component.scss']
})
export class ModalComentarioComponent implements OnInit {
  comentario: string;
  title: string;
  isSubmit: boolean = false;
  enviar: boolean = false;
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  guardar() {
    this.isSubmit = true;
    
  }

  fechar() {
    this.isSubmit = false;
    this.enviar = false;
  }
}
