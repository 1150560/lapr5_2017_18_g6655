import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ModalReceitaComponent } from 'app/receitas/medico/modal-receita/modal-receita.component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Rx';
import { AuthenticationService } from 'app/services/authentication.service';
import { ReceitasService } from 'app/services/receitas.service';
import { ToastrService } from 'ngx-toastr';
import { ModalInteracoesDanosasComponent } from 'app/receitas/medico/modal-interacoes-danosas/modal-interacoes-danosas.component';


@Component({
  selector: 'app-criar',
  templateUrl: './criar.component.html',
  styleUrls: ['./criar.component.scss']
})

export class CriarComponent implements OnInit, OnDestroy {

  receita: any = {};
  email: string;
  bsModalRef: BsModalRef;
  bsModalRef2: BsModalRef;
  subscription: Subscription;
  subscription2: Subscription;
  prescricoes: any = [];
  prescricoesToPost: any = [];
  sucesso: any;
  erroPrescricao: any;
  erroUser: any;
  index: number;
  constructor(private modalService: BsModalService,
    private authenticationService: AuthenticationService,
    private receitaService: ReceitasService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.subscription = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef && this.bsModalRef.content.isSubmit) {
        this.prescricoes.push(this.bsModalRef.content.prescricao);
        this.prescricoesToPost.push(this.bsModalRef.content.prescricaoToPost);
        this.bsModalRef = null;
      }
    });

    this.subscription2 = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef2 && this.bsModalRef2.content.confirmation) {
        this.receita.interacoes = this.bsModalRef2.content.interacoes;
        this.postReceita();
        this.bsModalRef2 = null;
      }
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }
  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalReceitaComponent, { class: 'modal-lg-success' });

  }

  eliminar(prescricao: any) {
    this.index = this.prescricoes.indexOf(prescricao);
    if (this.index >= 0) {
      this.prescricoes.splice(this.index, 1);
      this.prescricoesToPost.splice(this.index, 1);
    }
  }

  criarReceita() {
    this.receita.prescricoes = this.prescricoesToPost;
    this.receitaService.getInteracoesDanosas(this.receita)
      .subscribe(interacoes => {
        if (interacoes.message.length > 0) {
          this.bsModalRef2 = this.modalService.show(ModalInteracoesDanosasComponent, { class: 'modal-warning' });
          this.bsModalRef2.content.interacoes = interacoes.message;
        } else {
          this.receita.interacoes = [];
          this.postReceita();
        }
      }, error => {
        this.sucesso = null;
        this.erroPrescricao = error;
        this.toastr.error(" Receita não foi criada com sucesso: erro do lado do servidor");
        this.erroUser = null;
      });
  }

  postReceita() {
    
    this.authenticationService.getUserByEmail(this.email)
      .subscribe(userId => {
        this.receita.paciente = userId.id;
        this.receita.prescricoes = this.prescricoesToPost;
        this.receita.emailPaciente = this.email;
        this.receitaService.postReceita(this.receita)
          .subscribe(sucesso => {
            this.sucesso = sucesso;
            this.toastr.success("Receita foi criada com sucesso");
            this.erroPrescricao = null;
            this.erroUser = null;
          }, error => {
            this.sucesso = null;
            this.erroPrescricao = error;
            this.toastr.error(" Receita não foi criada com sucesso: Uma das prescrições é inválida");
            this.erroUser = null;
          });
      }, error => {
        this.sucesso = null;
        this.erroUser = error;
        this.toastr.error(" Receita não foi criada com sucesso: O email inserido é inválido");
        this.erroPrescricao = null;
      });
  }

  novaReceita() {
    this.receita = {};
    this.email = null;
    this.prescricoes = [];
    this.prescricoesToPost = [];
    this.sucesso = null;
    this.erroPrescricao = null;
    this.erroUser = null;
  }

}
