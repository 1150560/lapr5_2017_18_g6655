import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CriarComponent } from './criar.component';
import { SharedModule } from '../../../shared/shared.module';
import { CriarRoutingModule } from './criar-routing.module';
import { ModalReceitaComponent } from '../modal-receita/modal-receita.component';


@NgModule({
  imports: [
    CriarRoutingModule,
    SharedModule
  ],
  declarations: [CriarComponent],
})
export class CriarModule { }
