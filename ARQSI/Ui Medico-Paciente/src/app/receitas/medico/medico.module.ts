import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MedicoRoutingModule } from './medico-routing.module';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    MedicoRoutingModule,
    SharedModule
  ],
  declarations: []
})
export class MedicoModule { }
