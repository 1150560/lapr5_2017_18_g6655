import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FarmacosService } from '../../../services/farmacos.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-modal-receita',
  templateUrl: './modal-receita.component.html',
  styleUrls: ['./modal-receita.component.scss']
})
export class ModalReceitaComponent implements OnInit {
  posologiaId: any = 0;
  farmacoId: any = 0;
  apresentacaoId: any = 0;
  date: string;
  isSubmit: boolean;
  isToSave: boolean = false;
  apresentacao: any = {};
  posologia: any = {};
  farmaco: any = {};
  posologias: any = [];
  farmacos: any = [];
  apresentacoes: any = [];
  quantidade: number;
  prescricao: any = {};
  prescricaoToPost: any = {};
  constructor(public bsModalRef: BsModalRef,
    private farmacoService: FarmacosService) { }

  ngOnInit() {
    this.carregarFarmacos();
  }

  carregarObjetos() {
    for (let posologia of this.posologias) {
      if (posologia.id == this.posologiaId) {
        this.posologia = posologia;
      }
    }
    for (let farmaco of this.farmacos) {
      if (farmaco.id == this.farmacoId) {
        this.farmaco = farmaco;
      }
    }
    for (let apresentacao of this.apresentacoes) {
      if (apresentacao.id == this.apresentacaoId) {
        this.apresentacao = apresentacao;
      }
    }
  }

  guardarPrescricao() {
    this.isSubmit = true;
    this.carregarObjetos();
    this.prescricao.posologia = this.posologia;
    this.prescricao.farmaco = this.farmaco;
    this.prescricao.apresentacao = this.apresentacao;
    this.prescricao.quantidade = this.quantidade;
    this.prescricao.validade = this.date;
    this.prescricao.aviamentos = [];

    this.prescricaoToPost.validade = this.date;
    this.prescricaoToPost.farmacoId = this.farmacoId;
    this.prescricaoToPost.farmaco = this.farmaco.nome;
    this.prescricaoToPost.apresentacaoId = this.apresentacaoId;
    this.prescricaoToPost.quantidade = this.quantidade;
    this.prescricaoToPost.posologiaId = this.posologiaId;
  }

  validarParaGravar() {
    if (this.farmacoId > 0 &&
      this.apresentacaoId > 0 &&
      this.posologiaId > 0 &&
      this.date != undefined &&
      this.quantidade > 0) {
      this.isToSave = true;
    } else {
      this.isToSave = false;
    }
  }


  fechar() {
    this.isSubmit = false;
  }

  carregarFarmacos() {
    this.farmacoService.getFarmacos()
      .subscribe(farmacos => {
        this.farmacos = farmacos;
      }, error => {
        this.farmacos = [];
        // É necessário fazer 1 página para o erro
        console.log(error);
      });
  }

  carregarApresentacoes() {
    if (this.farmacoId != 0) {
      this.farmacoService.getApresentacoesOfFarmaco(this.farmacoId)
        .subscribe(apresentacoes => {
          this.apresentacoes = apresentacoes;
        }, error => {
          this.apresentacoes = [];
          // É necessário fazer 1 página para o erro
          console.log(error);
        });
    } else {
      this.apresentacoes = [];
    }
  }

  carregarPosologias() {
    if (this.farmacoId != 0) {
      this.farmacoService.getPosologiasOfFarmaco(this.farmacoId)
        .subscribe(posologias => {
          this.posologias = posologias;
        }, error => {
          this.posologias = [];
          // É necessário fazer 1 página para o erro
          console.log(error);
        });
    } else {
      this.posologias = [];
    }
  }

  onChange(event) {
    this.date = event;
    this.validarParaGravar();
  }
}