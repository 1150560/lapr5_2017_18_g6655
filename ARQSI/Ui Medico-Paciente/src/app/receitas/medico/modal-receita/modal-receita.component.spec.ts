import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalReceitaComponent } from './modal-receita.component';

describe('ModalReceitaComponent', () => {
  let component: ModalReceitaComponent;
  let fixture: ComponentFixture<ModalReceitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReceitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReceitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
