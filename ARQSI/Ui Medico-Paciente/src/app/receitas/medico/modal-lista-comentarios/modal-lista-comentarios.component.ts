import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ApresentacoesService } from 'app/services/apresentacoes.service';

@Component({
  selector: 'app-modal-lista-comentarios',
  templateUrl: './modal-lista-comentarios.component.html',
  styleUrls: ['./modal-lista-comentarios.component.scss']
})
export class ModalListaComentariosComponent implements OnInit {
  apresentacaoId: any;
  comentarios: any = [];
  title: string = "Lista Cometários";
  constructor(public bsModalRef: BsModalRef,
  private apresentacaoService: ApresentacoesService ) { }

  ngOnInit() {
    
  }

  getComentarios(){
   this.apresentacaoService.
    getComentarios(this.apresentacaoId).
    subscribe(comentarios => {
      this.comentarios = comentarios;
    }, erro => {
      this.comentarios = [];
    })
  }


}
