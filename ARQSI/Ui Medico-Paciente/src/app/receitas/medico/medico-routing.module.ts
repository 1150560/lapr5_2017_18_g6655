import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Medico'
    },
    children: [
      {
        path: 'criar',
        loadChildren: './criar/criar.module#CriarModule'
      },
      {
        path: 'editar',
        loadChildren: './editar/editar.module#EditarModule'
      },
      {
        path: 'comentario',
        loadChildren: './comentarios/comentarios.module#ComentariosModule'
      }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicoRoutingModule {}
