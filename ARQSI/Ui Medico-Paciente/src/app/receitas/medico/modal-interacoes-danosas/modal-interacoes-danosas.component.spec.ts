import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInteracoesDanosasComponent } from './modal-interacoes-danosas.component';

describe('ModalInteracoesDanosasComponent', () => {
  let component: ModalInteracoesDanosasComponent;
  let fixture: ComponentFixture<ModalInteracoesDanosasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInteracoesDanosasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInteracoesDanosasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
