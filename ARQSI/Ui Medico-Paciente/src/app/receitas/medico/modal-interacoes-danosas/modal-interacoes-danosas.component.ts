import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-modal-interacoes-danosas',
  templateUrl: './modal-interacoes-danosas.component.html',
  styleUrls: ['./modal-interacoes-danosas.component.scss']
})
export class ModalInteracoesDanosasComponent implements OnInit {

  public confirmation: boolean = false;
  public interacoes: any = [];
    constructor(public bsmodal: BsModalRef) { }


    ngOnInit() {
    }

    confirm(){
        this.confirmation = true;
    }

    decline(){
        this.confirmation = false;
    }

}
