import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditarComponent } from './editar.component';
import { SharedModule } from 'app/shared/shared.module';
import { EditarRoutingModule } from './editar-routing.module';

@NgModule({
  imports: [
    CommonModule, 
    SharedModule,
     EditarRoutingModule
  ],
  declarations: [EditarComponent]
})
export class EditarModule { }
