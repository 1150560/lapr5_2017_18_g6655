import { Component, OnInit } from '@angular/core';
import { ReceitasService } from '../../../services/receitas.service';
import { Receita } from '../../../models/receita';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Rx';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ModalReceitaComponent } from 'app/receitas/medico/modal-receita/modal-receita.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit, OnDestroy {

  bsModalRef: BsModalRef;
  subscription: Subscription;
  receita: any;
  receitas: any = [];
  receitaId: string;
  prescricoes: any = [];
  prescricao: any;
  aviamento: any = {};
  index: number;
  mostrarPrescricoes: boolean = true;
  erro: any;
  sucesso: any;
  erroPrescricao: any;
  sucessoPrescricao: any;
  prescricaoEditada: any = {};
  isAdicionar: boolean = false;

  constructor(private modalService: BsModalService,
    private receitasService: ReceitasService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getAllReceitas();

    this.subscription = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef.content.isSubmit) {
        this.receita.isEdit = true;
        if (this.isAdicionar) {
          this.receitasService.postPrescricao(this.receita._id, this.bsModalRef.content.prescricaoToPost).subscribe(sucesso => {
            this.sucessoPrescricao = sucesso;
            this.toastr.success("Prescricao criada com sucesso");
            this.erroPrescricao = null;
            this.sucesso = null;
            this.erro = null;
            this.getAllReceitas();
          }, error => {
            this.erroPrescricao = error;
            this.toastr.error("Prescricao não foi criada com sucesso");
            this.sucessoPrescricao = null;
            this.sucesso = null;
            this.erro = null;
          });
        } else {
          this.index = this.receita.prescricoes.indexOf(this.prescricao);
          if (this.index >= 0) {
            this.editarCamposPrescricao(this.bsModalRef.content.prescricao);
            this.receita.prescricoes[this.index] = this.prescricaoEditada;
          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllReceitas() {
    this.receitasService.getReceitas().subscribe(receitas => {
      this.receitas = receitas;
      this.prescricoes = [];
      this.mostrarPrescricoes = true;

    }, error => {
      this.receitas = [];
      this.prescricoes = [];
      this.mostrarPrescricoes = true;
    });
  }

  editarCamposPrescricao(prescricao: any) {
    this.prescricaoEditada = {};
    this.prescricaoEditada.apresentacao = prescricao.apresentacao;
    this.prescricaoEditada.apresentacaoId = prescricao.apresentacao.id;
    this.prescricaoEditada.aviamentos = prescricao.aviamentos;
    this.prescricaoEditada.farmaco = prescricao.farmaco.nome;
    this.prescricaoEditada.farmacoId = prescricao.farmaco.id;
    this.prescricaoEditada.posologiaId = prescricao.posologia.id;
    this.prescricaoEditada.posologiaPrescrita = prescricao.posologia.frequencia;
    this.prescricaoEditada.quantidade = prescricao.quantidade;
    this.prescricaoEditada.validade = prescricao.validade;
    if (!this.isAdicionar) {
      this.prescricaoEditada._id = this.prescricao._id;
    }
  }

  showPrescricoes(receita: any) {
    if (this.mostrarPrescricoes) {
      this.receita = receita;
      this.receita.havePrescricoes = true;
      this.prescricoes = this.receita.prescricoes;
      this.mostrarPrescricoes = false;
    }
  }

  dontShowPrescricoes(receita: any) {
    this.receita = receita;
    this.receita.havePrescricoes = false;
    this.prescricoes = [];
    this.mostrarPrescricoes = true;
    this.receita = null;
  }

  eliminar(prescricao: any) {
    this.index = this.receita.prescricoes.indexOf(prescricao);
    this.receita.isEdit = true;
    if (this.index >= 0) {
      this.receita.prescricoes.splice(this.index, 1);
    }
  }

  adicionar(receita: any) {
    this.receita = receita;
    this.isAdicionar = true;
    this.openModal();
  }

  editar(prescricao: any) {
    this.isAdicionar = false;
    this.prescricao = prescricao;
    this.openModal();
  }

  openModal() {
    this.bsModalRef = this.modalService.show(ModalReceitaComponent, { class: 'modal-lg' });
  }
  guardarReceita(receita: any) {
    if (receita.isEdit) {
      this.receitasService.putReceita(receita).subscribe(sucesso => {
        this.toastr.success(" Receita foi alterada com sucesso");
        this.sucesso = sucesso;
        this.erro = null;
        this.sucessoPrescricao = null;
        this.erroPrescricao = null;
      }, error => {
        this.erro = error;
        this.toastr.error("Receita não foi alterada com sucesso: Uma das prescrições é inválida");
        this.sucesso = null;
        this.sucessoPrescricao = null;
        this.erroPrescricao = null;
      })
    }
    receita.isEdit = false;
  }
}

