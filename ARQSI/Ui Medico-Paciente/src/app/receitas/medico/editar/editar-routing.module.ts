import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarComponent} from './editar.component';


const routes: Routes = [
  {
    path: '',
    component: EditarComponent,
    data: {
      title: 'Editar Receita'
    },
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarRoutingModule {}
