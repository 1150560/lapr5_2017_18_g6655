import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalComentarioComponent } from 'app/receitas/medico/modal-comentario/modal-comentario.component';
import { Apresentacao } from 'app/models/apresentacao';
import { ApresentacoesService } from 'app/services/apresentacoes.service';
import { ComentariosService } from 'app/services/comentarios.service';
import { Subscription } from 'rxjs/Subscription';
import { ModalListaComentariosComponent } from 'app/receitas/medico/modal-lista-comentarios/modal-lista-comentarios.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.scss']
})
export class ComentariosComponent implements OnInit {
  apresentacoes: Apresentacao[] = [];
  bsModalRef: BsModalRef;
  comentario: string;
  subscription: Subscription;
  subscricaoComentario: Subscription;
  err: any = null;
  success: any = null;
  comentarios: any = [];
  apresentacaoId: any;
  constructor(private modalService: BsModalService,
    private comentarioService: ComentariosService,
    private apresentacoesService: ApresentacoesService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.apresentacoesService.getApresentacoes().subscribe(apresentacoes => {
      this.apresentacoes = apresentacoes;
    })

    this.subscription = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef.content.isSubmit) {
        this.guardarComentario(this.bsModalRef.content.comentario);
      }
    });

    this.subscricaoComentario = this.modalService.onHide.subscribe(reason => {
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscricaoComentario.unsubscribe();
  }

  openModalWithComponent(apresentacaoId: any) {
    this.apresentacaoId = apresentacaoId;
    this.bsModalRef = this.modalService.show(ModalComentarioComponent, { class: 'modal-lg' });
    this.bsModalRef.content.title = 'Lista Comentários';

  }

  openModalComentariosWithComponent(apresentacaoId: any) {
    this.apresentacaoId = apresentacaoId;
    this.bsModalRef = this.modalService.show(ModalListaComentariosComponent, { class: 'modal-lg' });
    this.bsModalRef.content.apresentacaoId = apresentacaoId;
    this.bsModalRef.content.getComentarios();
  }

  guardarComentario(comentario: any) {

    this.comentarioService.postComentario({ apresentacaoId: this.apresentacaoId, descricao: comentario })
      .subscribe(success => {
        this.success = success;
        this.toastr.success("Comentário enviado com sucesso");
        this.err = null;
      }, err => {
        this.err = err;
        this.toastr.error("Erro ao enviar comentário. Tente Novamente");
        this.success = null;
      });
  }

  getComentariosOfApresentacao(apresentacaoId: any) {
    this.apresentacoesService.getComentarios(apresentacaoId).subscribe(comentarios => {
      this.comentarios = comentarios;
    }, erro => {
      this.comentarios = [];
    }
    )
  }

}
