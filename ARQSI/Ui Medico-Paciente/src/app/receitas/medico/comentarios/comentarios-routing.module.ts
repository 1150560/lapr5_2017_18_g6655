import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComentariosComponent } from 'app/receitas/medico/comentarios/comentarios.component';


const routes: Routes = [
  {
    path: '',
    component: ComentariosComponent,
    data: {
      title: 'Comentario'
    },
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComentarioRoutingModule {}
