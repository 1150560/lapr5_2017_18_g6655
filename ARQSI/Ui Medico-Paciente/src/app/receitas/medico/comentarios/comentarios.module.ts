import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { ComentariosComponent } from 'app/receitas/medico/comentarios/comentarios.component';
import { ComentarioRoutingModule } from 'app/receitas/medico/comentarios/comentarios-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ComentarioRoutingModule
  ],
  declarations: [ComentariosComponent]
})
export class ComentariosModule { }
