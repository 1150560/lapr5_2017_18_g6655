import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { PacienteComponent } from './paciente.component';

const routes: Routes = [
  {
    path: '',
    component: PacienteComponent,
    data: {
      title: 'Paciente'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacienteRoutingModule {}
