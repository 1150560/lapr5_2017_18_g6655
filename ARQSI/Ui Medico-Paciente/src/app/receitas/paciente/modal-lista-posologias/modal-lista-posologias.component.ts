import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FarmacosService } from 'app/services/farmacos.service';

@Component({
  selector: 'app-modal-lista-posologias',
  templateUrl: './modal-lista-posologias.component.html',
  styleUrls: ['./modal-lista-posologias.component.scss']
})
export class ModalListaPosologiasComponent implements OnInit {

  farmacoId: any;
  posologias: any = [];
  title: string = "Lista de Posologias";
  constructor(public bsModalRef: BsModalRef,
  private farmacosService: FarmacosService ) { }

  ngOnInit() {
    
  }

  getPosologias(){
   this.farmacosService.
    getPosologiasOfFarmaco(this.farmacoId).
    subscribe(posologias => {
      this.posologias = posologias;
    }, erro => {
      this.posologias = [];
    })
  }



}
