import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalListaPosologiasComponent } from './modal-lista-posologias.component';

describe('ModalListaPosologiasComponent', () => {
  let component: ModalListaPosologiasComponent;
  let fixture: ComponentFixture<ModalListaPosologiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListaPosologiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListaPosologiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
