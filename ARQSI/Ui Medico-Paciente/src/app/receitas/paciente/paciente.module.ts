import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { PacienteComponent } from './paciente.component';
import { PacienteRoutingModule } from './paciente-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    PacienteRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ PacienteComponent]
})
export class PacienteModule { }
