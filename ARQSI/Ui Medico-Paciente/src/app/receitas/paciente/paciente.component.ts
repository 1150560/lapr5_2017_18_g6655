import { Component, OnInit } from '@angular/core';
import { ReceitasService } from '../../services/receitas.service';
import { Receita } from '../../models/receita';
import * as moment from 'moment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ModalListaPosologiasComponent } from 'app/receitas/paciente/modal-lista-posologias/modal-lista-posologias.component';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.scss']
})
export class PacienteComponent implements OnInit, OnDestroy {
  receita: any;
  receitas: any = [];
  receitaId: string;
  prescricoes: any = [];
  prescricao: any;
  farmacoId: any;
  bsModalRef: BsModalRef;
  subscription: Subscription;
  mostrarPrescricoes: boolean = true;

  constructor(private receitasService: ReceitasService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.getAllReceitas();
    this.subscription = this.modalService.onHide.subscribe(reason => { });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  openModalComentariosWithComponent(farmacoId: any) {
    this.farmacoId = farmacoId;
    this.bsModalRef = this.modalService.show(ModalListaPosologiasComponent, { class: 'modal-lg' });
    this.bsModalRef.content.farmacoId = farmacoId;
    this.bsModalRef.content.getPosologias();
  }

  getAllReceitas() {
    this.receitasService.getReceitas().subscribe(receitas => {
      this.receitas = receitas;
      this.prescricoes = [];
      this.mostrarPrescricoes = true;

    }, error => {
      this.receitas = [];
      this.prescricoes = [];
      this.mostrarPrescricoes = true;
    });
  }

  showPrescricoes(receita: any) {
    if (this.mostrarPrescricoes) {
      this.receita = receita;
      this.receita.havePrescricoes = true;
      this.prescricoes = this.receita.prescricoes;
      this.mostrarPrescricoes = false;
    }
  }

  dontShowPrescricoes(receita: any) {
    this.receita = receita;
    this.receita.havePrescricoes = false;
    this.prescricoes = [];
    this.mostrarPrescricoes = true;
    this.receita = null;
  }
}

