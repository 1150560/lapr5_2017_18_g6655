import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { AuthGuard } from 'app/guards/auth.guards';
import { MedicoGuard } from 'app/guards/medico.guards';
import { UtenteGuard } from 'app/guards/utente.guards';

export const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Receitas'
    },
    children: [ 
      {
        path: 'medico',
        loadChildren: './medico/medico.module#MedicoModule',
        canActivate: [AuthGuard, MedicoGuard]
      },
      {
        path: 'paciente',                         
        loadChildren: './paciente/paciente.module#PacienteModule',
        canActivate: [AuthGuard, UtenteGuard]
      }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceitasRoutingModule { }
