export class User {
    id: string;
    token: string;
    tokenExp: number;
    name: string;
    medico: boolean;
    farmaceutico: boolean;
    doisFatoresAtivado: boolean;
    tokenGdr: any;
}