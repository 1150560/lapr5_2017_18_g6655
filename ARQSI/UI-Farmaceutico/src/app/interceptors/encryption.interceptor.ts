import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { EncryptionHandler } from '../encryption/encryption';


@Injectable()
export class EncryptionInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let encryption = new EncryptionHandler();
    if (req.method === 'POST' || req.method === 'PUT') {
      let msg = JSON.stringify(req.body);
      req = req.clone({ body: { messageEncrp: encryption.encryptMessage(msg) } });
    }
    return next
      .handle(req)
      .map(event => {
        if (event instanceof HttpResponse) {
          if (event.body.messageEncrp) {
            event = event.clone({ body: JSON.parse(encryption.decryptMessage(event.body.messageEncrp)) });
          }
          return event;
        }
      })
      .catch((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.error.messageEncrp) {
            let error = JSON.parse(encryption.decryptMessage(err.error.messageEncrp));
            let message = error.message;
            let er = new HttpErrorResponse({
              error: { message, status: err.status },
              headers: err.headers,
              status: err.status,
              statusText: err.statusText,
              url: err.url || undefined,
            });
            return Observable.throw(er);
          } else {
            return Observable.throw(err);
          }
        }
      });
  }
}