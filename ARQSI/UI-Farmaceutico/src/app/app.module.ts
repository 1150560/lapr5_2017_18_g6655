import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule, ModalModule, TooltipModule, BsDatepickerModule, defineLocale, ptBr } from 'ngx-bootstrap';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';


import { ChartsModule } from 'ng2-charts/ng2-charts';

import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// Routing Module
import { AppRoutingModule } from './app.routing';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { LoginComponent } from './login/login.component';


//services
import { ReceitasService } from './services/receitas.service';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './guards/auth.guards';
import { FarmaceuticoGuard } from './guards/farmaceutico.guards';
import { ApresentacoesService } from './services/apresentacoes.service';
import { FarmacosService } from './services/farmacos.service';
import { ComentariosService } from 'app/services/comentarios.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'app/shared/shared.module';
import { ModalAviarComponent } from 'app/receitas/farmaceutico/modal-aviar/modal-aviar.component';
import { MedicamentosService } from 'app/services/medicamentos.service';
import { StocksService } from 'app/services/stocks.service';
import { OrdensEncomendaService } from 'app/services/ordens-encomenda.service';
import { ModalEditarStockComponent } from 'app/stocks/modal-editar-stock/modal-editar-stock.component';
import { ModalAdicionarStockComponent } from 'app/stocks/modal-adicionar-stock/modal-adicionar-stock.component';
import { ModalSecretComponent } from 'app/login/modal-secret/modal-secret.component';
import { EncryptionInterceptor } from 'app/interceptors/encryption.interceptor';

defineLocale('en', ptBr);
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    TooltipModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    ModalAviarComponent,
    ModalEditarStockComponent,
    ModalAdicionarStockComponent,
    ModalSecretComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: EncryptionInterceptor,
    multi: true,
  },
    AuthGuard,
    FarmaceuticoGuard,
    AuthenticationService,
    ReceitasService,
    ApresentacoesService,
    FarmacosService,
    ComentariosService,
    MedicamentosService,
    StocksService,
    OrdensEncomendaService],
  entryComponents: [ModalAviarComponent, ModalEditarStockComponent, ModalAdicionarStockComponent,ModalSecretComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
