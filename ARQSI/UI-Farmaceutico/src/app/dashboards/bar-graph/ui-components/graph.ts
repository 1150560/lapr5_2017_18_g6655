import { Bar } from "./bar";

/**
 * Horizontal bar graph
 */
export class Graph {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private gData: any = {
        leftMargin: 200,
        rightMargin: 20,
        topMargin: 40,
        bottomMargin: 80,
    };
    private info: any;
    private bars: any;

    private helperValues;

    readonly barMaxWidth = 50;
    readonly barSpacing = 10;
    readonly textMaxSize = 18;
    readonly colors = [
        { color: '#5cbae6', colorHover: '#97D3EF' },
        { color: '#b6d957', colorHover: '#D0E694' },
        { color: '#fac364', colorHover: '#FBD89C' },
        { color: '#8cd3ff', colorHover: '#B5E3FF' },
        { color: '#d998cb', colorHover: '#E6BDDD' },
        { color: '#f2d249', colorHover: '#F6E28B' },
        { color: '#93b9c6', colorHover: '#BAD2DA' },
        { color: '#ccc5a8', colorHover: '#DEDAC7' },
        { color: '#52bacc', colorHover: '#90D3DE' },
        { color: '#dbdb46', colorHover: '#E8E889' },
        { color: '#98aafb', colorHover: '#BDC8FC' }
    ];

    constructor(canvas: HTMLCanvasElement, info: any, mouse: any) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.info = info;

        // Coordinates of graph bar area
        this.gData.left = this.gData.leftMargin;
        this.gData.right = this.canvas.width - this.gData.rightMargin;
        this.gData.top = this.gData.topMargin;
        this.gData.bottom = this.canvas.height - this.gData.bottomMargin;

        // Usable area
        this.gData.innerWidth = this.canvas.width - this.gData.rightMargin - this.gData.leftMargin;
        this.gData.innerHeight = this.canvas.height - this.gData.bottomMargin - this.gData.topMargin;

        // Dynamic bar width
        let barWidth = (this.gData.innerHeight - (1 + info.values.length) * this.barSpacing) / info.values.length;

        let minValue = 0;
        let maxValue = info.values[0].quantidade;
        info.values.forEach(item => {
            if (item.quantidade > maxValue) {
                maxValue = item.quantidade;
            }
        });

        // Graph must be multiple of 4
        maxValue = maxValue + 4 - maxValue % 4;

        // Dynamic bar height
        let scale = this.gData.innerWidth / maxValue;

        this.helperValues = [];
        let middleValue = (maxValue - minValue) / 2;
        this.helperValues.push({ realValue: minValue, scaleValue: minValue * scale });
        this.helperValues.push({ realValue: middleValue, scaleValue: middleValue * scale });
        this.helperValues.push({ realValue: middleValue / 2, scaleValue: middleValue / 2 * scale });
        this.helperValues.push({ realValue: middleValue / 2 + middleValue, scaleValue: (middleValue / 2 + middleValue) * scale });
        this.helperValues.push({ realValue: maxValue, scaleValue: maxValue * scale });

        // Create Bars
        this.bars = [];
        for (let i = 0; i < info.values.length; i++) {
            let color = this.colors[i % this.colors.length].color;
            let colorHover = this.colors[i % this.colors.length].colorHover;
            let startPos = this.gData.top + barWidth * i + this.barSpacing * (i + 1);
            let endPos = barWidth;
            let targetHeight = info.values[i].quantidade * scale;

            let bar = new Bar(this.canvas, this.gData, startPos, endPos, info.values[i], targetHeight, color, colorHover, mouse);
            this.bars.push(bar);
        }
    }

    drawAxis() {
        this.ctx.lineWidth = 2;

        // YY AXIS
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.top);
        this.ctx.lineTo(this.gData.left, this.gData.bottom);
        this.ctx.strokeStyle = 'rgba(100, 100, 100, 0.7)';
        this.ctx.stroke();

        // XX AXIS
        this.ctx.beginPath();
        this.ctx.moveTo(this.gData.left, this.gData.bottom);
        this.ctx.lineTo(this.gData.right, this.gData.bottom);
        this.ctx.strokeStyle = 'rgba(100, 100, 100, 0.7)';
        this.ctx.stroke();
    }

    drawAxisInfo() {
        this.helperValues.forEach(helper => {
            // Helper lines
            this.ctx.beginPath();
            this.ctx.moveTo(this.gData.left + helper.scaleValue, this.gData.top);
            this.ctx.lineTo(this.gData.left + helper.scaleValue, this.gData.bottom);
            this.ctx.strokeStyle = 'rgba(150, 150, 150, 0.4)';
            this.ctx.stroke();

            // Helper text
            this.ctx.fillStyle = 'black';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.font = 14 + 'px Arial';
            this.ctx.fillText(helper.realValue, this.gData.left + helper.scaleValue, this.gData.bottom + (this.gData.bottomMargin / 4)); // In the middle of the margin
        });

        // Titles
        // XX
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';
        this.ctx.font = 'bold ' + 14 + 'px Arial';
        this.ctx.fillText(this.info.xTitle, this.gData.left + this.gData.innerWidth / 2, this.gData.bottom + (this.gData.bottomMargin * 3 / 4)); // In the middle of the margin

        // YY
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';
        this.ctx.font = 'bold ' + 14 + 'px Arial';
        this.ctx.fillText(this.info.yTitle, this.gData.left / 2, this.gData.top - (this.gData.topMargin / 2)); // In the middle of the margin
    }

    update() {

        this.drawAxisInfo();

        this.bars.forEach(bar => {
            bar.update();
        });

        this.drawAxis();
    }

}