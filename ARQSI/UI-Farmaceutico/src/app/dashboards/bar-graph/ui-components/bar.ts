/**
 * Horizontal bars. So height and width must switch.
 */
export class Bar {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    readonly gData;
    private height = 0;
    private textSize;
    readonly start;
    readonly barWidth;
    readonly targetHeight;
    readonly info;
    readonly speed = 6;
    readonly color;
    readonly colorHover;
    readonly maxTextSize = 18;
    private mouse;

    constructor(canvas: HTMLCanvasElement, gData, start, barWidth, info, targetHeight, color, colorHover, mouse) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.gData = gData;
        this.start = start;
        this.barWidth = barWidth;
        this.targetHeight = targetHeight;
        this.info = info;
        this.color = color;
        this.colorHover = colorHover;
        this.mouse = mouse;

        if (this.barWidth > this.maxTextSize) {
            this.textSize = this.maxTextSize;
        } else {
            this.textSize = this.barWidth;
        }
    }

    draw() {

        let hover = this.hover();

        // Bar
        this.ctx.fillStyle = this.color;
        if (hover) {
            this.ctx.fillStyle = this.colorHover;
        }
        
        this.ctx.fillRect(this.gData.left, this.start, this.height, this.barWidth);

        if (hover){
            this.drawHover();
        }

        // Bar text
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = 'right';
        this.ctx.textBaseline = 'middle';
        this.ctx.font = this.textSize - 4 + 'px Arial'; // Text must match bar width 
        this.ctx.fillText(this.info.nome, this.gData.left - 10, this.start + this.barWidth / 2, this.gData.left - 10); // Text in the middle of the bar
    }

    update() {
        if (this.height < this.targetHeight) {
            this.height += this.speed;

            if (this.height > this.targetHeight){
                this.height = this.targetHeight;
            }
        }

        this.draw();
    }

    hover() {
        if (!this.mouse) {
            return false;
        }

        // Checks if mouse is hovering bar

        // Horizontal
        if (this.mouse.x < this.gData.left || this.mouse.x > this.gData.left + this.targetHeight) {
            return false;
        }

        // Vertical
        if (this.mouse.y < this.start || this.mouse.y > this.start + this.barWidth) {
            return false;
        }

        return true;
    }

    drawHover() {
        // RECTANGLE
        let width = 60;
        let height = 24;

        this.ctx.strokeStyle = 'black';
        this.ctx.fillStyle = 'white';
        this.ctx.lineWidth = 1;
        this.ctx.fillRect(this.mouse.x, this.mouse.y - height, width, height);
        this.ctx.strokeRect(this.mouse.x, this.mouse.y - height, width, height);

        // TEXT
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';
        this.ctx.font = 14 + 'px Arial';
        this.ctx.fillText('Valor: ' + this.info.quantidade, this.mouse.x + width / 2, this.mouse.y - height / 2, width);
    }
}