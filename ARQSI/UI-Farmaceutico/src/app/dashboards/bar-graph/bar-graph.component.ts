import { Component, OnInit, ViewChild, ElementRef, NgZone, Input } from '@angular/core';
import { Graph } from './ui-components/graph';

@Component({
  selector: 'app-bar-graph',
  templateUrl: './bar-graph.component.html',
  styleUrls: ['./bar-graph.component.scss']
})
export class BarGraphComponent implements OnInit {
  @Input() info: any;
  @ViewChild('myCanvas') canvasRef: ElementRef;

  private running: boolean;
  private ctx: CanvasRenderingContext2D;
  private canvas: HTMLCanvasElement;
  private graph: Graph;
  private mouse: any = { x: 0, y: 0 };

  constructor(private ngZone: NgZone) { }

  ngOnInit() {
    this.running = true;
    this.ngZone.runOutsideAngular(() => {
      this.canvas = this.canvasRef.nativeElement;
      this.canvas.width = 1000;
      this.canvas.height = 500;

      this.ctx = this.canvas.getContext('2d');

      this.canvas.addEventListener('mousemove', this.onMousemove.bind(this));

      this.graph = new Graph(this.canvas, this.info, this.mouse);

      this.animate();
    });
  }

  ngOnDestroy() {
    this.running = false;
  }

  private onMousemove(ev) {
    let rect = this.canvas.getBoundingClientRect();
    this.mouse.x = ev.clientX - rect.left;
    this.mouse.y = ev.clientY - rect.top;
  }

  private animate() {

    // Check that we're still running.
    if (!this.running) {
      return;
    }

    // Schedule next
    requestAnimationFrame(() => this.animate());

    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.graph.update();
  }

}
