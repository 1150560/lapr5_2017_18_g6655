import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MedicamentosService } from 'app/services/medicamentos.service';

@Component({
  selector: 'app-modal-adicionar-stock',
  templateUrl: './modal-adicionar-stock.component.html',
  styleUrls: ['./modal-adicionar-stock.component.scss']
})
export class ModalAdicionarStockComponent implements OnInit {

  isSubmit: boolean = false;
  isToSave: boolean = false;
  title: string;
  stock: any = {};
  medicamentos: any = [];
  constructor(
    public bsModalRef: BsModalRef,
    private medicamentosService: MedicamentosService
  ) { }

  ngOnInit() {
    this.medicamentosService.getAvailableMedicamentos()
      .subscribe(medicamentos => {
        this.medicamentos = medicamentos;
      }, error => {
        this.medicamentos = [];
      })
  }

  /**
   * Verificar se pode clocar no botao de guardar
   */
  validarParaGravar() {
    if (
      this.stock.quantidadeMinima > 0 && this.stock.quantidadeLote > 0
      && this.stock.quantidadeLote > this.stock.quantidadeMinima
      && this.stock.quantidadeMinima <= this.stock.quantidadeAtual
      && this.stock.idMedicamento > 0) {
      this.isToSave = true;
    } else {
      this.isToSave = false;
    }
  }
  /**
   * Variavel para saber que o utilizador quis guardar o stock
   */
  guardarStock() {
    this.isSubmit = true;
    this.stock.medicamento = this.medicamentos.find(medicamento => medicamento.id == this.stock.idMedicamento);
  }
  /**
   * Fechar a modal
   */
  fechar() {
    this.isSubmit = false;
  }

}
