import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAdicionarStockComponent } from './modal-adicionar-stock.component';

describe('ModalAdicionarStockComponent', () => {
  let component: ModalAdicionarStockComponent;
  let fixture: ComponentFixture<ModalAdicionarStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAdicionarStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAdicionarStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
