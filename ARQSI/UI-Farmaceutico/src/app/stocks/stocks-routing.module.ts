import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { AuthGuard } from 'app/guards/auth.guards';
import { FarmaceuticoGuard } from 'app/guards/farmaceutico.guards';
import { StocksComponent } from 'app/stocks/stocks.component';

export const routes: Routes = [
  {
    path: '',
    component: StocksComponent,
    data: {
      title: 'Stocks'
    },
    canActivate: [AuthGuard, FarmaceuticoGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StocksRoutingModule { }
