import { Component, OnInit } from '@angular/core';
import { StocksService } from 'app/services/stocks.service';
import { ModalEditarStockComponent } from 'app/stocks/modal-editar-stock/modal-editar-stock.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { Subscription } from 'rxjs/Subscription';
import { ToastrService } from 'ngx-toastr';
import { ModalAdicionarStockComponent } from 'app/stocks/modal-adicionar-stock/modal-adicionar-stock.component';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.scss']
})
export class StocksComponent implements OnInit {

  stocks: any = [];
  loading: boolean = false;
  savingStock: boolean =  false;
  bsModalRef: BsModalRef;
  bsModalRefCriarStock: BsModalRef;
  subscription: Subscription;
  constructor(
    private stocksService: StocksService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loading = true;
    //load stocks
    this.loadStocks();


    this.subscription = this.modalService.onHide.subscribe(reason => {
      //modal edit

      if (this.bsModalRef && this.bsModalRef.content.isSubmit) {
        let stock = this.bsModalRef.content.stock;
        let stocks = this.stocks.map((stock2) => {
          if (stock2._id == stock._id) {
            stock2.editado = true;
            stock2.quantidadeMinima = stock.quantidadeMinima;
            stock2.quantidadeLote = stock.quantidadeLote;
          }
          return stock2;
        });
        this.stocks = stocks;
        this.bsModalRef = null;
      }
      //modal create
      if (this.bsModalRefCriarStock && this.bsModalRefCriarStock.content.isSubmit) {
        this.savingStock =  true;
        let stock = this.bsModalRefCriarStock.content.stock;
        this.stocksService.postStock(stock).subscribe(data => {
          stock._id = data.id;
          this.savingStock = false;
          this.toastr.success("Stock adicionado com sucesso");
          this.stocks.push(stock);
          this.bsModalRefCriarStock = null;
        }, error => {
          this.toastr.error(error.error.message);
          this.bsModalRefCriarStock = null;
        })
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /**
   * Carrega a lista de stocks
   */
  loadStocks() {
    this.stocksService.getStocks().subscribe(stocks => {
      this.loading = false;
      this.stocks = stocks.map((stock) => {
        stock.podeEliminar = this.podeEliminar(stock);
        //stock.eliminado = false;
        return stock;
      });
    }, error => {
      this.loading = false;
      this.stocks = [];
    })
  }
  /**
   * 
   * @param stock Guarda as alteracoes feitas ao stock
   */
  guardar(stock) {
    stock.isSaving = true;
    if (stock.eliminado) {
      this.stocksService.deleteStock(stock).subscribe(response => {
        stock.isSaving = false;
        this.toastr.success("Stock eliminado com sucesso");
        let index = this.stocks.indexOf(stock);
        if (index > -1) {
          this.stocks.splice(index, 1);
        }
      }, error => {
        stock.isSaving = false;
        this.toastr.error(error.error.message);
      })
    } else {
      this.stocksService.putStock(stock).subscribe(response => {
        stock.isSaving = false;
        this.toastr.success("Stock alterado com sucesso");
        let stocks = this.stocks.map((stock2) => {
          if (stock2._id == stock._id) {
            stock2.editado = false;
          }
          return stock2;
        });
        this.stocks = stocks;
      }, error => {
        stock.isSaving = false;
        this.toastr.error(error.error.message);
      })
    }
  }
  /**
   * Stock eliminado
   * @param stock - stock que se quer eliminar
   */
  eliminar(stock) {
    let stocks = this.stocks.map((stock2) => {
      if (stock2._id == stock._id) {
        stock2.eliminado = true;
        stock2.editado = true;
      }
      return stock2;
    });
    this.stocks = stocks;
  }

  /**
   * Verificar se pode eliminar stock
   * @param stock stock a verificar
   */
  podeEliminar(stock) {
    if (stock.quantidadeAtual > 0 || stock.isEncomendado) {
      return false;
    }
    return true;
  }
  /**
   * 
   * @param stock - stock que se quer editar
   */
  editar(stock) {
    this.openModalWithComponentEditar(stock);
  }

  /**
   * Criar um stock
   */
  addStock() {
    this.openModalWithComponentCreate();
  }


  openModalWithComponentCreate() {
    this.bsModalRefCriarStock = this.modalService.show(ModalAdicionarStockComponent, { class: 'modal-lg' });
    this.bsModalRefCriarStock.content.title = "Criar Stock";
  }

  openModalWithComponentEditar(stock) {
    this.bsModalRef = this.modalService.show(ModalEditarStockComponent, { class: 'modal-lg' });
    this.bsModalRef.content.title = "Editar Stock";
    this.bsModalRef.content.stock._id = stock._id;
    this.bsModalRef.content.stock.quantidadeMinima = stock.quantidadeMinima;
    this.bsModalRef.content.stock.quantidadeLote = stock.quantidadeLote;
    this.bsModalRef.content.stock.quantidadeAtual = stock.quantidadeAtual;
  }
}
