import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditarStockComponent } from './modal-editar-stock.component';

describe('ModalEditarStockComponent', () => {
  let component: ModalEditarStockComponent;
  let fixture: ComponentFixture<ModalEditarStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditarStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditarStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
