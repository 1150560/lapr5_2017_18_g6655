import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-editar-stock',
  templateUrl: './modal-editar-stock.component.html',
  styleUrls: ['./modal-editar-stock.component.scss']
})
export class ModalEditarStockComponent implements OnInit {

  isSubmit: boolean = false;
  isToSave: boolean = false;
  title: string;
  stock: any = {};
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {

  }

  /**
   * Variavel necessaria para notificar outra componente que se quer aviar
   */
  guardarAviamento() {
    this.isSubmit = true;
  }

  /**
   * Verificar se pode clocar no botao de guardar
   */
  validarParaGravar() {
    if (
      this.stock.quantidadeMinima >= 0 && this.stock.quantidadeLote >= 0
      && this.stock.quantidadeLote >= this.stock.quantidadeMinima
      && this.stock.quantidadeMinima <= this.stock.quantidadeAtual) {
      this.isToSave = true;
    } else {
      this.isToSave = false;
    }
  }
  /**
   * Variavel para saber que o utilizador quis guardar o stock
   */
  guardarStock() {
    this.isSubmit = true;
  }
  /**
   * Fechar a modal
   */
  fechar() {
    this.isSubmit = false;
  }

}
