import { NgModule } from '@angular/core';
import { StocksComponent } from 'app/stocks/stocks.component';
import { SharedModule } from 'app/shared/shared.module';
import { StocksRoutingModule } from 'app/stocks/stocks-routing.module';


@NgModule({
  imports: [
    StocksRoutingModule,
    SharedModule
  ],
  declarations: [StocksComponent]
})
export class StocksModule { }
