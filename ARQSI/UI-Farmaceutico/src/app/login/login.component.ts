import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ModalSecretComponent } from 'app/login/modal-secret/modal-secret.component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit, OnDestroy {
  model: any = {};
  loading = false;
  error = '';
  bsModalRef: BsModalRef;
  subscription: Subscription;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private modalService: BsModalService,
    private toastr: ToastrService) { }
  ngOnInit() {
    this.authenticationService.logout();
    this.activatedRoute.params.subscribe(params => {
      if (params['u'] !== undefined) {
        ;
        this.error = 'Your user cannot access receitas';
      }
    });
    this.subscription = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef.content.submit && this.bsModalRef.content.confirmado) {
        this.router.navigate(['/home/apresentacoes']);
      } else if (this.bsModalRef.content.submit && !this.bsModalRef.content.confirmado) {
        this.authenticationService.logout();
        this.toastr.error("Secret is invalid!");
      } else if (!this.bsModalRef.content.submit) {
        this.authenticationService.logout();
      }
    });
  }
  login() {
    this.loading = true;
    this.authenticationService.login(this.model.email,
      this.model.password)
      .subscribe(result => {

        this.loading = false;
        if (result === true) {
          if (this.authenticationService.userInfo.doisFatoresAtivado) {
            this.openModalWithComponent();
          } else {
            this.router.navigate(['/home/apresentacoes']);
          }
        } else {
          this.toastr.error("Username or password is incorrect");;
        }
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalSecretComponent);
  }
}
