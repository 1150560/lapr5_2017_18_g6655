import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { EstatisticasComponent } from './estatisticas.component';

const routes: Routes = [
  {
    path: '',
    component: EstatisticasComponent,
    data: {
      title: 'Estatisticas'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstatisticasRoutingModule {}
