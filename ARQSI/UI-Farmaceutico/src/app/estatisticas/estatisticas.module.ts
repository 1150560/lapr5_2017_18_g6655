import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { EstatisticasComponent } from './estatisticas.component';
import { EstatisticasRoutingModule } from './estatisticas-routing.module';
import { SharedModule } from '../shared/shared.module';
import { BarGraphComponent } from 'app/dashboards/bar-graph/bar-graph.component';

@NgModule({
  imports: [

    EstatisticasRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ 
      EstatisticasComponent,
      BarGraphComponent
 ]
})
export class EstatisticasModule { }
