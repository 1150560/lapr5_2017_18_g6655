import { Component, OnInit } from '@angular/core';
import { BarGraphComponent } from 'app/dashboards/bar-graph/bar-graph.component';
import { StocksService } from 'app/services/stocks.service';

@Component({
  selector: 'app-estatisticas',
  templateUrl: './estatisticas.component.html',
  styleUrls: ['./estatisticas.component.scss']
})
export class EstatisticasComponent implements OnInit {

  public info: any;

  constructor(private stockService: StocksService) { }

  ngOnInit() {

    this.stockService.getStocksChart().subscribe(stats => {
      
      // Sort by name
      stats.stocks.sort(function (a, b) {
        return a.nome.localeCompare(b.nome);
      });

      this.info = {
        xTitle: 'Quantidade encomendada nos últimos 15 dias',
        yTitle: 'Nome do Medicamento',
        values: stats.stocks
      }

    });

  }
}
