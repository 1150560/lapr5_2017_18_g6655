import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ApresentacoesComponent } from './apresentacoes.component';
import { ApresentacoesRoutingModule } from './apresentacoes-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [

    ApresentacoesRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ ApresentacoesComponent ]
})
export class ApresentacoesModule { }
