import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ReceitasRoutingModule } from './receitas-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    ReceitasRoutingModule,
    SharedModule
  ],
  declarations: []
})
export class ReceitasModule { }
