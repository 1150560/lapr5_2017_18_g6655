import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { AuthGuard } from 'app/guards/auth.guards';
import { FarmaceuticoGuard } from 'app/guards/farmaceutico.guards';

export const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Receitas'
    },

    children: [ 
      {
        path: 'aviar',
        loadChildren: './farmaceutico/farmaceutico.module#FarmaceuticoModule',
        canActivate: [AuthGuard, FarmaceuticoGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceitasRoutingModule { }
