import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FarmacosService } from '../../../services/farmacos.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { MedicamentosService } from 'app/services/medicamentos.service';

@Component({
  selector: 'app-modal-aviar',
  templateUrl: './modal-aviar.component.html',
  styleUrls: ['./modal-aviar.component.scss']
})
export class ModalAviarComponent implements OnInit {
  isSubmit: boolean;
  isToSave: boolean = false;
  medicamentos: any = [];
  medicamentoId: number = 0;
  quantidade: number = 0;
  apresentacaoId: string;
  title: string;
  constructor(public bsModalRef: BsModalRef,
    private medicamentosService: MedicamentosService) { }

  ngOnInit() {

  }

  /**
   * Variavel necessaria para notificar outra componente que se quer aviar
   */
  guardarAviamento() {
    this.isSubmit = true;
  }

  /**
   * Verificar se pode clocar no botao de aviar
   */
  validarParaGravar() {
    if (this.medicamentoId > 0 &&
      this.quantidade > 0) {
      this.isToSave = true;
    } else {
      this.isToSave = false;
    }
  }

    /**
   * Carregar Medicamentos de uma apresentacao
   */
  carregarMedicamentos(apresentacaoId) {
    this.medicamentosService.getMedicamentosOfApresentacao(apresentacaoId)
      .subscribe(medicamentos => {
        this.medicamentos = medicamentos;
      }, error => {
        this.medicamentos = [];
      })
  }
  /**
   * Fechar a modal
   */
  fechar() {
    this.isSubmit = false;
    this.medicamentos = [];
    this.medicamentoId = 0;
    this.quantidade  = 0;
    this.apresentacaoId = null;
  }

}