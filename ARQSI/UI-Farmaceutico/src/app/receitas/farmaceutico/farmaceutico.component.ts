import { Component, OnInit } from '@angular/core';
import { ReceitasService } from '../../services/receitas.service';
import { Receita } from '../../models/receita';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs/Subscription';
import { ModalAviarComponent } from 'app/receitas/farmaceutico/modal-aviar/modal-aviar.component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { MedicamentosService } from 'app/services/medicamentos.service';

@Component({
  selector: 'app-farmaceutico',
  templateUrl: './farmaceutico.component.html',
  styleUrls: ['./farmaceutico.component.scss']

})
export class FarmaceuticoComponent implements OnInit, OnDestroy {

  receita: Receita;
  receitaId: string;
  prescricoes: any = [];
  prescricao: any = {};
  aviamento: any = {};
  medicamentos: any = [];
  mostrarAviamentos: boolean = true;
  unidades: number = 0;
  medicamentoId: string;
  erro: any;
  loading: boolean = false;
  loadingAviamentos: boolean = false;
  bsModalRef: BsModalRef
  subscription: Subscription;;
  constructor(private receitasService: ReceitasService,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private medicamentosService: MedicamentosService) { }

  ngOnInit() {
    this.subscription = this.modalService.onHide.subscribe(reason => {
      if (this.bsModalRef.content.isSubmit) {
        this.unidades = this.bsModalRef.content.quantidade;
        this.medicamentoId = this.bsModalRef.content.medicamentoId;
        this.createAviamento();
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  search() {
    this.loading = true;
    this.receitasService.getReceita(this.receitaId)
      .subscribe(receitaNova => {
        this.receita = receitaNova;
        this.prescricoes = [];
        this.dontShowAviamentos();
        this.mostrarAviamentos = true;
        this.unidades = 0;
        this.loading = false;
      }, error => {
        this.receitaId = null;
        this.receita = null;
        this.prescricoes = [];
        this.prescricao = {};
        this.mostrarAviamentos = true;
        this.unidades = 0;
        this.loading = false;
        this.toastr.error('Receita não encontrada');
      }
      , );
  }

  showPrescricoes() {
    this.prescricoes = this.receita.prescricoes;
  }

  dontShowPrescricoes() {
    this.prescricoes = [];
    this.dontShowAviamentos();
  }

  showAviamentos(prescricao: any) {
    this.erro = null;
    this.prescricao = prescricao;
    if (this.mostrarAviamentos) {
      this.prescricao = prescricao;
      this.prescricao.haveAviamentos = true;
      this.mostrarAviamentos = false;
    }
  }

  dontShowAviamentos() {
    this.prescricao.haveAviamentos = false;
    this.mostrarAviamentos = true;
    this.unidades = 0;
    this.erro = null;
  }

  createAviamento() {
    this.loadingAviamentos = true;
    this.aviamento.quantidade = this.unidades;
    this.receitasService.putAviamento(this.receitaId,
      this.prescricao._id, this.aviamento, this.medicamentoId)
      .subscribe(aviamentoNovo => {
        this.loadingAviamentos = false;
        this.toastr.success('Aviado com sucesso');
        this.getAviamentosOfPrescricao();
      }, error => {
        this.loadingAviamentos = false;
        this.erro = error.error.message;
        this.toastr.error(this.erro);
      })
  }

  getAviamentosOfPrescricao() {
    this.receitasService.getAviamentosOfPrescricao(this.receitaId,
      this.prescricao._id)
      .subscribe(aviamentos => {
        this.prescricao.aviamentos = aviamentos;
        this.dontShowAviamentos();
        this.showAviamentos(this.prescricao);
      }, error => {
        console.log(error);
      })
  }

  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalAviarComponent, { class: 'modal-lg' });
    this.bsModalRef.content.carregarMedicamentos(this.prescricao.apresentacaoId);
    this.bsModalRef.content.title = "Aviar Prescrição";
  }

}
