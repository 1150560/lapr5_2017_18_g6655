import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FarmaceuticoComponent } from './farmaceutico.component';
import { FarmaceuticoRoutingModule } from './farmaceutico-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    FarmaceuticoRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ FarmaceuticoComponent]
})
export class FarmaceuticoModule { }
