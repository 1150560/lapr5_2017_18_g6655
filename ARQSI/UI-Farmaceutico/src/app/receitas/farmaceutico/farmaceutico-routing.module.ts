import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { FarmaceuticoComponent } from './farmaceutico.component';

const routes: Routes = [
  {
    path: '',
    component: FarmaceuticoComponent,
    data: {
      title: 'Farmaceutico'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FarmaceuticoRoutingModule {}
