import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }
    canActivate() {
        if (this.authenticationService.userInfo) {
            const dateFarmacia = new Date(0);
            const dateReceitas = new Date(0);
            dateFarmacia.setUTCSeconds(this.authenticationService.userInfo.tokenExp);
            dateReceitas.setUTCSeconds(this.authenticationService.userInfo.tokenGdr.tokenExp);
            if (dateFarmacia !== undefined && dateReceitas !== undefined) {
                if ((dateFarmacia.valueOf() > new Date().valueOf())
                    && (dateReceitas.valueOf() > new Date().valueOf())) {
                    return true;
                }
            }
        }
        this.router.navigate(['/home/login']);
        return false;
    }
}