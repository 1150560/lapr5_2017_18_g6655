import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormateDatePipe } from 'app/pipes/formate-date.pipe';
import { HideMessagePipe } from 'app/pipes/hide-message.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [FormateDatePipe, HideMessagePipe],
  exports: [CommonModule, FormsModule, FormateDatePipe, HideMessagePipe]
})
export class SharedModule { }
