import { TestBed, inject } from '@angular/core/testing';

import { FarmacosService } from './farmacos.service';

describe('FarmacosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FarmacosService]
    });
  });

  it('should be created', inject([FarmacosService], (service: FarmacosService) => {
    expect(service).toBeTruthy();
  }));
});
