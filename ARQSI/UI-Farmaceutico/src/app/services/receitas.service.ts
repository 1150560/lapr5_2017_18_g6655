import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Receita } from '../models/receita';
import { AuthenticationService } from './authentication.service';
@Injectable()
export class ReceitasService {
  private receitasUrl = 'https://apigestaofarmacias.azurewebsites.net/api/receitas';
  private farmaciaUrl = 'https://apigestaofarmacias.azurewebsites.net/api/farmacia';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  /**
   * Get receita with id
   * @param idReceita - id of receita
   */
  getReceita(idReceita: string): Observable<Receita> {
    const url = `${this.receitasUrl}/${idReceita}`;
    return this.http.get<Receita>(url, this.getHeaders());
  }

/**
 * Post aviamento of receita and prescricao with the given id
 * @param idReceita - id of receita
 * @param idPrescricao - id of prescricao
 * @param aviamento - aviamento (must have quantity)
 */
  putAviamento(idReceita: string, idPrescricao: string, aviamento: any, idMedicamento: any): Observable<any> {
    const url = `${this.farmaciaUrl}/medicamento/${idMedicamento}/aviar`;
    aviamento.idReceita = idReceita;
    aviamento.idPrescricao = idPrescricao;
    return this.http.put<any>(url, aviamento, this.getHeaders());
  }

  /**
   * Get aviamentos of Prescricao with the given id
   * @param idReceita - id of receita
   * @param idPrescricao - id of prescricao
   */
  getAviamentosOfPrescricao(idReceita: string, idPrescricao: string): Observable<any> {
    const url = `${this.receitasUrl}/${idReceita}/prescricao/${idPrescricao}/aviamentos`;
    return this.http.get<any>(url, this.getHeaders());
  }

  /**
   * Headers of application
   */
  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });
    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
