import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class StocksService {

  private farmaciaUrl = 'https://apigestaofarmacias.azurewebsites.net/api/farmacia';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  /**
   * Get all stocks from farmaci
   */
  getStocks(): Observable<any> {
    const url = `${this.farmaciaUrl}/stocks`;
    return this.http.get<any>(url, this.getHeaders());
  }

  /**
   * Post stock in farmacia
   * @param stock 
   */
  postStock(stock): Observable<any> {
    const url = `${this.farmaciaUrl}/stock/`;
    return this.http.post<any>(url, stock, this.getHeaders());
  }
  /**
   * Change quantidade minima and lote of stock
   * @param stock - stock
   */
  putStock(stock): Observable<any> {
    const url = `${this.farmaciaUrl}/stock/${stock._id}`;
    return this.http.put<any>(url, stock, this.getHeaders());
  }

  /**
   * Delete the stock given by parameter
   * @param stock Delete this stock
   */
  deleteStock(stock): Observable<any> {
    const url = `${this.farmaciaUrl}/stock/${stock._id}`;
    return this.http.delete<any>(url, this.getHeaders());
  }

  /**
   * Get stock chart
   */
  getStocksChart(): Observable<any> {
    const url = `${this.farmaciaUrl}/stocksChart/`;
    return this.http.get<any>(url, this.getHeaders());
  }

  /**
   * Headers of application
   */
  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });
    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
