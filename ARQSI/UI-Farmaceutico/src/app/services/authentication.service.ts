import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import * as jwt_decode from 'jwt-decode';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

class Token { token: string };

@Injectable()
export class AuthenticationService {
  private authUrl = 'https://apigestaofarmacias.azurewebsites.net/api/login'; 
  private userUrl = 'https://apigestaoreceitas.azurewebsites.net/api/users';
  private authAzure = 'https://apigestaoreceitas.azurewebsites.net/api/users/login';
  private alterarDoisFatoresUrl = 'https://apigestaofarmacias.azurewebsites.net/api/login/alterarDoisFatores';
  private getDoisFatoresUrl = 'https://apigestaofarmacias.azurewebsites.net/api/login/doisFatores/isAtivo';
  //verificar o servidor:porta
  public userInfo: User;
  public erro: any;
  authentication: Subject<User> = new Subject<User>();

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {
    this.userInfo = localStorage.userInfo && JSON.parse(localStorage.userInfo);
  }
  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }


  login(email: string, password: string): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.http.post<Token>(this.authUrl, {
        email: email,
        password: password
      })
        .subscribe(data => {
          if (data.token) {
            const tokenDecoded = jwt_decode(data.token);
            const tokenGdrDecoded = jwt_decode(tokenDecoded.tokenGdr);
            this.userInfo = {
              id: tokenGdrDecoded.id,
              token: data.token,
              tokenExp: tokenDecoded.exp,
              name: tokenGdrDecoded.name,
              medico: tokenGdrDecoded.medico,
              farmaceutico: tokenGdrDecoded.farmaceutico,
              doisFatoresAtivado: tokenGdrDecoded.doisFatoresAtivado,
              tokenGdr: {
                token: tokenDecoded.tokenGdr,
                tokenExp: tokenGdrDecoded.exp
              }
            }
            localStorage.userInfo = JSON.stringify(this.userInfo);
            this.authentication.next(this.userInfo);
            observer.next(true);
          } else {
            this.authentication.next(this.userInfo);
            observer.next(false);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
          } else {
          }
          this.authentication.next(this.userInfo);
          observer.next(false);
        });

    });
  }

  logout() {
    this.userInfo = null;
    localStorage.removeItem('userInfo');
    this.authentication.next(this.userInfo);
  }

  getUserByEmail(email: string): Observable<any> {
    const url = `${this.userUrl}/${email}`;
    return this.http.get<any>(url);
  }

  postSecret(segredo: String) {
    const url = `${this.authUrl}/confirmarSecret`;
    return this.http.post<any>(url, {
      secret: segredo
    }, this.getHeaders());
  }

  alterarDoisFatores() {
    return this.http.put<any>(this.alterarDoisFatoresUrl, {}, this.getHeaders());
  }

  getDoisFatoresIsAtivo() {
    return this.http.get<any>(this.getDoisFatoresUrl, this.getHeaders());
  }
}
