import { TestBed, inject } from '@angular/core/testing';

import { OrdensEncomendaService } from './ordens-encomenda.service';

describe('OrdensEncomendaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrdensEncomendaService]
    });
  });

  it('should be created', inject([OrdensEncomendaService], (service: OrdensEncomendaService) => {
    expect(service).toBeTruthy();
  }));
});
