import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class OrdensEncomendaService {

  private farmaciaUrl = 'https://apigestaofarmacias.azurewebsites.net/api/farmacia';
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  /*
  * Get ordensEncomenda por entregar
  */
  getOrdensPorEntregar(): Observable<any> {
    const url = `${this.farmaciaUrl}/ordensEncomenda/PorEntregar`;
    return this.http.get<any>(url, this.getHeaders());
  }

  /*
* Get ordensEncomenda por confirmar
*/
  getOrdensPorConfirmar(): Observable<any> {
    const url = `${this.farmaciaUrl}/ordensEncomenda/PorConfirmar`;
    return this.http.get<any>(url, this.getHeaders());
  }
  /* 
  * Confirm Delivery
  */
  confirmarEntrega(idOrdemEncomenda: string): Observable<any> {
    const url = `${this.farmaciaUrl}/ordemEncomenda/${idOrdemEncomenda}/confirmar`;
    return this.http.put<any>(url, {}, this.getHeaders());
  }

  /*
  * Headers of application
  */
  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });
    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
