
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class FarmacosService {
  private farmacosUrl = 'https://apigestaoreceitas.azurewebsites.net/api/farmacos';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  getFarmacos(): Observable<any[]> {
    return this.http.get<any[]>(this.farmacosUrl,
      this.getHeaders());
  }

  getApresentacoesOfFarmaco(idFarmaco: string): Observable<any> {
    const url = `${this.farmacosUrl}/${idFarmaco}/apresentacoes`;
    return this.http.get<any>(url, this.getHeaders());
  }

  getPosologiasOfFarmaco(idFarmaco: string): Observable<any> {
    const url = `${this.farmacosUrl}/${idFarmaco}/posologias`;
    return this.http.get<any>(url, this.getHeaders());
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
