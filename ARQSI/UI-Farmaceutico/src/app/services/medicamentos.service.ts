import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from 'app/services/authentication.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MedicamentosService {

  private medicamentosUrl = 'https://apigestaofarmacias.azurewebsites.net/api/farmacia'; //alterar o url quando se fizer o deploy
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  /**
   * Get medicamentos of farmacia with apresentacao id
   * @param apresentacaoId - apresentacao id
   */
  getMedicamentosOfApresentacao(apresentacaoId: string): Observable<any[]> {
    const url = `${this.medicamentosUrl}/apresentacao/${apresentacaoId}/medicamentos`;
    return this.http.get<any[]>(url, this.getHeaders());
  }

  /**
   * Get available medicamentos (to create stock)
   */
  getAvailableMedicamentos(): Observable<any[]> {
    const url = `${this.medicamentosUrl}/stock/medicamentos`;
    return this.http.get<any[]>(url, this.getHeaders());
  }

  /**
   * Headers of application
   */
  getHeaders() {
    let headers = new HttpHeaders({
      'x-acess-token':
        this.authenticationService.userInfo.token
    });
    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }
}
