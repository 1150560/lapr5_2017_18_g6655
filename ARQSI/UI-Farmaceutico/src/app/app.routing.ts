import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AuthGuard } from './guards/auth.guards';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home/apresentacoes',
    pathMatch: 'full',
  },
  
  {
    path: 'home',
    component: FullLayoutComponent,
    data: {
      title: 'Lista de Apresentações'
    },
    children: [
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
      },    
      {
        path: 'apresentacoes',
        loadChildren: './apresentacoes/apresentacoes.module#ApresentacoesModule'
      },
      {
        path: 'receitas',
        loadChildren: './receitas/receitas.module#ReceitasModule'
      },
      {
        path: 'stocks',
        loadChildren: './stocks/stocks.module#StocksModule'
      },
      {
        path: 'ordensEncomenda',
        loadChildren: './ordens-encomenda/ordens-encomenda.module#OrdensEncomendaModule'
      },
      {
        path: 'estatisticas',
        loadChildren: './estatisticas/estatisticas.module#EstatisticasModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)] , 
  exports: [RouterModule]
})
export class AppRoutingModule { }
