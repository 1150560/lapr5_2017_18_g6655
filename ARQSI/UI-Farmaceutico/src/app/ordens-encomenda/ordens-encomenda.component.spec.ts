import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdensEncomendaComponent } from './ordens-encomenda.component';

describe('OrdensEncomendaComponent', () => {
  let component: OrdensEncomendaComponent;
  let fixture: ComponentFixture<OrdensEncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdensEncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdensEncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
