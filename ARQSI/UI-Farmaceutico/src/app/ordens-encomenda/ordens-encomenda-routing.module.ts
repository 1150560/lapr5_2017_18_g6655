import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { AuthGuard } from 'app/guards/auth.guards';
import { FarmaceuticoGuard } from 'app/guards/farmaceutico.guards';
import { OrdensEncomendaComponent } from 'app/ordens-encomenda/ordens-encomenda.component';

export const routes: Routes = [
  {
    path: '',
    component: OrdensEncomendaComponent,
    data: {
      title: 'OrdensEncomenda'
    },
    canActivate: [AuthGuard, FarmaceuticoGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdensEncomendaRoutingModule { }
