import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { OrdensEncomendaRoutingModule } from 'app/ordens-encomenda/ordens-encomenda-routing.module';
import { OrdensEncomendaComponent } from 'app/ordens-encomenda/ordens-encomenda.component';


@NgModule({
  imports: [
    OrdensEncomendaRoutingModule,
    SharedModule
  ],
  declarations: [OrdensEncomendaComponent]
})
export class OrdensEncomendaModule { }
