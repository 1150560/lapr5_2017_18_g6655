import { Component, OnInit } from '@angular/core';
import { OrdensEncomendaService } from 'app/services/ordens-encomenda.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ordens-encomenda',
  templateUrl: './ordens-encomenda.component.html',
  styleUrls: ['./ordens-encomenda.component.scss']
})
export class OrdensEncomendaComponent implements OnInit {

  ordensEncomenda: any = [];
  ordensEncomendaPorConfirmar: any = [];

  constructor(
    private ordensEncomendaService: OrdensEncomendaService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.ordensEncomendaService.getOrdensPorEntregar().subscribe(ordens => {
      this.ordensEncomenda = ordens;
    }, error => {
      this.ordensEncomenda = [];
    });
    this.ordensEncomendaService.getOrdensPorConfirmar().subscribe(ordens => {
      this.ordensEncomendaPorConfirmar = ordens;
    }, error => {
      this.ordensEncomendaPorConfirmar = [];
    })
  }

  confirmar(ordemEncomenda: any) {
    this.ordensEncomendaService.confirmarEntrega(ordemEncomenda._id).subscribe(success => {
      this.ngOnInit();
      this.toastr.success("Ordem confirmada com sucesso");
    }, error => {
      this.toastr.error("Erro a confirmar a ordem");
    });
  }

}
