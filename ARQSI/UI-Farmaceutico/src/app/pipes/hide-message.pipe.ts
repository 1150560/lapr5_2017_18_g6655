import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hideMessage'
})
export class HideMessagePipe implements PipeTransform {

  transform(value: string, args?: any): any {
    if(!value){
      return "";
    }
    return (value.slice(0, 30) + '...');
  }

}
