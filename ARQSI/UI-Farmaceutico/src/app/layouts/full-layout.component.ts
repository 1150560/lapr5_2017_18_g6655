import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ToastrService, Toast } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit, OnDestroy {
  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };
  subscriptionAuth: Subscription;
  userInfo: User;
  mensagens: Array<string> = [];
  private doisFatoresAtivado: boolean;

  constructor(
    private authenticationService: AuthenticationService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private toastr: ToastrService
  ) { }

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit() {
    this.userInfo = this.authenticationService.userInfo;
    if (!this.userInfo) {
      this.doisFatoresAtivado = false;
    } else {
      this.authenticationService.getDoisFatoresIsAtivo().subscribe(result => {
        this.doisFatoresAtivado = result.doisFatoresAtivado;
      }, err => {
        this.doisFatoresAtivado = false;
      })
    }
    this.subscriptionAuth =
      this.authenticationService.authentication.subscribe((userInfo) => {
        this.userInfo = userInfo;
        this.cdr.detectChanges();
      });
  }

  notificar(msg) {
    this.toastr.warning(msg, null, { extendedTimeOut: 1000, timeOut: 10000, progressBar: true });
  }

  logout() {
    this.authenticationService.logout();
    this.mensagens = [];
    this.router.navigate(['/home/apresentacoes']);
  }
  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
  }

  alterarDoisFatoroes(event) {
    this.authenticationService.alterarDoisFatores().subscribe(result => {
      this.doisFatoresAtivado = !this.doisFatoresAtivado;
      if (this.doisFatoresAtivado) {
        this.toastr.success("Dois fatores ativado com sucesso");
      } else {
        this.toastr.success("Dois fatores desativado com sucesso");
      }
    }, err => {
      this.toastr.error("Configuração não foi efetuada");
    })
  }
}
