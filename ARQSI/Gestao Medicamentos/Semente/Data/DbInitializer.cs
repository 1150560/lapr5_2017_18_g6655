﻿using Semente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Data
{
    public static class DbInitializer
    {
        public static void Initialize(SementeContext context)
        {
            context.Database.EnsureCreated();
            // Verficar se existe base de dados
            if (context.Medicamento.Any())
            {
                return;
            }
            //Farmacos
            Farmaco f1 = new Farmaco { Nome = "Paracetamol" };
            Farmaco f2 = new Farmaco { Nome = "Ibuprofeno" };
            Farmaco f3 = new Farmaco { Nome = "Ácido Acetilsalicílico" };
            context.Farmaco.Add(f1);
            context.Farmaco.Add(f2);
            context.Farmaco.Add(f3);
            context.SaveChanges();
            //Apresentações
            Apresentacao ap1 = new Apresentacao
            {
                Forma = "Comprimidos",
                Quantidade = "5",
                Concentracao = "500mg",
                Farmaco = f1,
                FarmacoId = f1.Id
            };
            Apresentacao ap2 = new Apresentacao
            {
                Forma = "Xarope",
                Quantidade = "50",
                Concentracao = "125mg",
                Farmaco = f1,
                FarmacoId = f1.Id

            };
            Apresentacao ap3 = new Apresentacao
            {
                Forma = "Xarope",
                Quantidade = "400ml",
                Concentracao = "150ml",
                Farmaco = f2,
                FarmacoId = f2.Id
            };
            Apresentacao ap4 = new Apresentacao
            {
                Forma = "Comprimidos",
                Quantidade = "25",
                Concentracao = "250mg",
                Farmaco = f2,
                FarmacoId = f2.Id
            };
            Apresentacao ap5 = new Apresentacao
            {
                Forma = "Xarope",
                Quantidade = "400ml",
                Concentracao = "200ml",
                Farmaco = f3,
                FarmacoId = f3.Id
            };
            Apresentacao ap6 = new Apresentacao
            {
                Forma = "Comprimidos",
                Quantidade = "7",
                Concentracao = "200mg",
                Farmaco = f3,
                FarmacoId = f3.Id

            };

            //Apresentacoes
            context.Apresentacao.Add(ap1);
            context.Apresentacao.Add(ap2);
            context.Apresentacao.Add(ap3);
            context.Apresentacao.Add(ap4);
            context.Apresentacao.Add(ap5);
            context.Apresentacao.Add(ap6);

            context.SaveChanges();

            //Medicamentos
            context.Medicamento.Add(new Medicamento { Nome = "Cefalium", Apresentacao = ap1, ApresentacaoId = ap1.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Acetofen", Apresentacao = ap1, ApresentacaoId = ap1.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Dorico", Apresentacao = ap1, ApresentacaoId = ap1.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Fluviral", Apresentacao = ap2, ApresentacaoId = ap2.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Maxidrin", Apresentacao = ap2, ApresentacaoId = ap2.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Notuss", Apresentacao = ap2, ApresentacaoId = ap2.Id });

            context.Medicamento.Add(new Medicamento { Nome = "Advil", Apresentacao = ap3, ApresentacaoId = ap3.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Dalsy", Apresentacao = ap3, ApresentacaoId = ap3.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Ibufran", Apresentacao = ap3, ApresentacaoId = ap3.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Ibrofeno", Apresentacao = ap4, ApresentacaoId = ap4.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Motrin", Apresentacao = ap4, ApresentacaoId = ap4.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Alvium", Apresentacao = ap4, ApresentacaoId = ap4.Id });

            context.Medicamento.Add(new Medicamento { Nome = "Aspirina", Apresentacao = ap5, ApresentacaoId = ap5.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Alicura", Apresentacao = ap5, ApresentacaoId = ap5.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Asetisin", Apresentacao = ap5, ApresentacaoId = ap5.Id });
            context.Medicamento.Add(new Medicamento { Nome = "As-Med", Apresentacao = ap6, ApresentacaoId = ap6.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Aceticil", Apresentacao = ap6, ApresentacaoId = ap6.Id });
            context.Medicamento.Add(new Medicamento { Nome = "Acetildor", Apresentacao = ap6, ApresentacaoId = ap6.Id });
            context.SaveChanges();

            //Posologias
            context.Posologia.Add(new Posologia { Frequencia = "6 em 6 h", Apresentacao = ap1, ApresentacaoId = ap1.Id });
            context.Posologia.Add(new Posologia { Frequencia = "8 em 8 h", Apresentacao = ap1, ApresentacaoId = ap1.Id });
            context.Posologia.Add(new Posologia { Frequencia = "5 em 5 h", Apresentacao = ap1, ApresentacaoId = ap1.Id });
            context.Posologia.Add(new Posologia { Frequencia = "1x por dia", Apresentacao = ap2, ApresentacaoId = ap2.Id });
            context.Posologia.Add(new Posologia { Frequencia = "2x por dia", Apresentacao = ap2, ApresentacaoId = ap2.Id });
            context.Posologia.Add(new Posologia { Frequencia = "6 em 6 h", Apresentacao = ap2, ApresentacaoId = ap2.Id });

            context.Posologia.Add(new Posologia { Frequencia = "7 em 7 h", Apresentacao = ap3, ApresentacaoId = ap3.Id });
            context.Posologia.Add(new Posologia { Frequencia = "3x por dia", Apresentacao = ap3, ApresentacaoId = ap3.Id });
            context.Posologia.Add(new Posologia { Frequencia = "2x por dia", Apresentacao = ap3, ApresentacaoId = ap3.Id });
            context.Posologia.Add(new Posologia { Frequencia = "6 em 6 h", Apresentacao = ap4, ApresentacaoId = ap4.Id });
            context.Posologia.Add(new Posologia { Frequencia = "5 em 5 h", Apresentacao = ap4, ApresentacaoId = ap4.Id });
            context.Posologia.Add(new Posologia { Frequencia = "1x por dia", Apresentacao = ap4, ApresentacaoId = ap4.Id });

            context.Posologia.Add(new Posologia { Frequencia = "2x por dia", Apresentacao = ap5, ApresentacaoId = ap5.Id });
            context.Posologia.Add(new Posologia { Frequencia = "4 em 4 h", Apresentacao = ap5, ApresentacaoId = ap5.Id });
            context.Posologia.Add(new Posologia { Frequencia = "5 em 5 h", Apresentacao = ap5, ApresentacaoId = ap5.Id });
            context.Posologia.Add(new Posologia { Frequencia = "1x por dia", Apresentacao = ap6, ApresentacaoId = ap6.Id });
            context.Posologia.Add(new Posologia { Frequencia = "4x por dia", Apresentacao = ap6, ApresentacaoId = ap6.Id });
            context.Posologia.Add(new Posologia { Frequencia = "8 em 8 h", Apresentacao = ap6, ApresentacaoId = ap6.Id });


            context.SaveChanges();
        }
    }
}