﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
   // [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Farmaco")]
    public class FarmacosController : Controller
    {
        private readonly SementeContext _context;

        public FarmacosController(SementeContext context)
        {
            _context = context;
        }

        //DTO da Farmaco
        private static readonly Expression<Func<Farmaco, FarmacoDTO>> AsFarmacoDTO =
            x => new FarmacoDTO
            {
                Id = x.Id,
                Nome = x.Nome,
            };

        //DTO do Medicamento
        private static readonly Expression<Func<Medicamento, MedicamentoDTO>> AsMedicamentoDTO =
            x => new MedicamentoDTO
            {
                Id = x.Id,
                Nome = x.Nome,
            };

        //DTO da Apresentação
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDTO =
            x => new ApresentacaoDTO
            {
                Id = x.Id,
                Forma = x.Forma,
                Concentracao = x.Concentracao,
                Quantidade = x.Quantidade
            };
        // GET: api/Farmacos
        [HttpGet]
        public IEnumerable<FarmacoDTO> GetFarmaco(string nome)
        {
            if (nome != null)
            {
                return _context.Farmaco.Where(f => f.Nome == nome).Select(AsFarmacoDTO);
            }
            return _context.Farmaco.Select(AsFarmacoDTO);
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.Select(AsFarmacoDTO).SingleOrDefaultAsync(f => f.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // api/Farmaco/{id}/Medicamentos
        [HttpGet("{id}/Medicamentos")]
        public async Task<IActionResult> GetMedicamentos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Farmaco farmaco = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            List<Apresentacao> apresentacoes = _context.Apresentacao.Where(a => a.FarmacoId == id).ToList();
            List<MedicamentoDTO> medicamentos = new List<MedicamentoDTO>();
            foreach (Apresentacao apresentacao in apresentacoes)
            {
                int apresentacaoID = apresentacao.Id;
                List<Medicamento> medicamentos2 = _context.Medicamento.Where(m => m.ApresentacaoId == apresentacaoID).ToList();

                foreach (Medicamento medicamento in medicamentos2)
                {
                    medicamentos.Add(new MedicamentoDTO { Id = medicamento.Id, Nome = medicamento.Nome });
                }
            }
            return Ok(medicamentos);
        }

        // GET api/Farmaco/{id}/InteracoesDanosas
        [HttpGet("{id}/InteracoesDanosas")]
        public IActionResult GetInteracoes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<InteracoesDanosas> interacoes = _context.InteracoesDanosas.Where(i => i.FarmacoId1 == id
            || i.FarmacoId2 == id).ToList();

            List<InteracoesDanosasMedicamentoDTO> lstInteracoes = new List<InteracoesDanosasMedicamentoDTO>();
            foreach (InteracoesDanosas interacao in interacoes)
            {
                if (interacao.FarmacoId1 == id)
                {
                    lstInteracoes.Add(
                    new InteracoesDanosasMedicamentoDTO()
                    {
                        FarmacoId = interacao.FarmacoId2
                    });
                }
                if (interacao.FarmacoId2 == id)
                {
                    lstInteracoes.Add(new InteracoesDanosasMedicamentoDTO()
                    {
                        FarmacoId = interacao.FarmacoId1
                    });
                }
            }
            return Ok(lstInteracoes);
        }

        // api/Farmaco/{id}/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacoes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Farmaco farmaco = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            var apresentacoes = _context.Apresentacao.Where(a => a.FarmacoId == id).Select(AsApresentacaoDTO);
            return Ok(apresentacoes);
        }

        // api/Farmaco/{id}/Posologias
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetPosologias([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Farmaco farmaco = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }
            List<Apresentacao> apresentacoes = _context.Apresentacao.Where(a => a.FarmacoId == id).ToList();
            List<PosologiaDTO> posologias = new List<PosologiaDTO>();
            foreach (Apresentacao apresentacao in apresentacoes)
            {
                int apresentacaoID = apresentacao.Id;
                List<Posologia> posologiasAp = _context.Posologia.Where(m => m.ApresentacaoId == apresentacaoID).ToList();
                foreach (Posologia posologia in posologiasAp)
                {
                    posologias.Add(new PosologiaDTO { Id = posologia.Id, Frequencia = posologia.Frequencia });
                }
            }
            return Ok(posologias);
        }

        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmacos
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var farmaco2 = await _context.Farmaco.SingleOrDefaultAsync(f => f.Nome == farmaco.Nome);
            if (farmaco2 != null)
            {
                return BadRequest();
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, farmaco);
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            //Impedir de apagar Farmacos, se houver Apresentacoes com este Farmaco
            var apresentacoes = _context.Apresentacao.Where(m => m.FarmacoId == id);
            if (apresentacoes.ToList().Count > 0)
            {
                return new StatusCodeResult(403);

            }
            else
            {

                _context.Farmaco.Remove(farmaco);
                await _context.SaveChangesAsync();

                return Ok(farmaco);
            }
        }

        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.Id == id);
        }
    }
}