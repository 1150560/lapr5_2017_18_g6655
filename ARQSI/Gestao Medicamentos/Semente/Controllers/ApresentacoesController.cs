﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System.Linq.Expressions;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacao")]
    public class ApresentacoesController : Controller
    {
        private readonly SementeContext _context;

        public ApresentacoesController(SementeContext context)
        {
            _context = context;
        }

        //DTO da Apresentação
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDTO =
            x => new ApresentacaoDTO
            {
                Id = x.Id,
                Forma = x.Forma,
                Concentracao = x.Concentracao,
                Quantidade = x.Quantidade,
                Farmaco = x.Farmaco
            };

        //DTO da Apresentação
        private static readonly Expression<Func<Comentario, ComentarioDTO>> AsComentarioDTO =
            x => new ComentarioDTO
            {
                Id = x.Id,
                Apresentacao = x.Apresentacao,
                ApresentacaoId = x.ApresentacaoId,
                Descricao = x.Descricao
            };

        //DTO do MedicamentoInfo
        private static readonly Expression<Func<Medicamento, MedicamentoDTOInfo>> AsMedicamentoDTOInfo =
            x => new MedicamentoDTOInfo
            {
                Id = x.Id,
                Nome = x.Nome,
                Forma = x.Apresentacao.Forma,
                Concentracao = x.Apresentacao.Concentracao,
                Quantidade = x.Apresentacao.Quantidade,
                NomeFarmaco = x.Apresentacao.Farmaco.Nome,
            };

        // GET: api/Apresentacoes
        [HttpGet]
        public IEnumerable<ApresentacaoDTO> GetApresentacao()
        {
            return _context.Apresentacao.Select(AsApresentacaoDTO);
        }

        // GET: api/Apresentacoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.Select(AsApresentacaoDTO).SingleOrDefaultAsync(m => m.Id == id);

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }

        // api/Farmaco/{id}/Apresentacoes
        [HttpGet("{id}/Comentarios")]
        public async Task<IActionResult> GetComentarios([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Apresentacao apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(a => a.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            var comentarios = _context.Comentario.Where(c => c.ApresentacaoId == id).Select(AsComentarioDTO);
            return Ok(comentarios);
        }

        // api/Apresentacao/{id}/Medicamentos
        [HttpGet("{id}/medicamentos")]
        public async Task<IActionResult> GetMedicamenos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Apresentacao apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(a => a.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            var medicamentos = _context.Medicamento.Where(m => m.ApresentacaoId == id).Select(AsMedicamentoDTOInfo);
            return Ok(medicamentos);
        }

        // PUT: api/Apresentacoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacoes
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (apresentacao.FarmacoId == 0)
            {
                return BadRequest(ModelState);
            }

            _context.Apresentacao.Add(apresentacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApresentacao", new { id = apresentacao.Id }, apresentacao);
        }

        // DELETE: api/Apresentacoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }
            //Impedir de apagar Apresentacao, se houver Medicamentos/Posologias com esta Apresentacao
            var medicamentos = _context.Medicamento.Where(m => m.ApresentacaoId == id);
            var posologias = _context.Posologia.Where(m => m.ApresentacaoId == id);
            if (medicamentos.ToList().Count > 0 || posologias.ToList().Count > 0)
            {
                return new StatusCodeResult(403);

            }
            else
            {
                _context.Apresentacao.Remove(apresentacao);
                await _context.SaveChangesAsync();

                return Ok(apresentacao);
            }
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }
    }
}