﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Medicamento")]
    public class MedicamentosController : Controller
    {
        private readonly SementeContext _context;

        public MedicamentosController(SementeContext context)
        {
            _context = context;
        }

        //DTO da Apresentação
        private static readonly Expression<Func<Medicamento, ApresentacaoDTO>> AsApresentacaoDTO =
            x => new ApresentacaoDTO
            {
                Id = x.ApresentacaoId,
                Forma = x.Apresentacao.Forma,
                Concentracao = x.Apresentacao.Concentracao,
                Quantidade = x.Apresentacao.Quantidade
            };


        //DTO do Medicamento
        private static readonly Expression<Func<Medicamento, MedicamentoDTO>> AsMedicamentoDTO =
            x => new MedicamentoDTO
            {
                Id = x.Id,
                Nome = x.Nome
            };


        //DTO do MedicamentoInfo
        private static readonly Expression<Func<Medicamento, MedicamentoDTOInfo>> AsMedicamentoDTOInfo =
            x => new MedicamentoDTOInfo
            {
                Id = x.Id,
                Nome = x.Nome,
                Forma = x.Apresentacao.Forma,
                Concentracao = x.Apresentacao.Concentracao,
                Quantidade = x.Apresentacao.Quantidade,
                NomeFarmaco = x.Apresentacao.Farmaco.Nome,
            };


        //DTO da Posologia
        private static readonly Expression<Func<Posologia, PosologiaDTO>> AsPosologiaDTO =
            x => new PosologiaDTO
            {
                Id = x.Id,
                Frequencia = x.Frequencia,
            };

        // GET: api/Medicamentos
        [HttpGet]
        public IQueryable<MedicamentoDTOInfo> GetMedicamento(string nome)
        {
            if (nome != null)
            {
                var medicamentos = _context.Medicamento.Where(m => m.Nome == nome)

       .Select(AsMedicamentoDTOInfo);

                return medicamentos;
            }
            else
            {
                var medicamentos = _context.Medicamento.Select(AsMedicamentoDTOInfo);

                return medicamentos;
            }
        }


        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacoes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Medicamento medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            var apresentacoes = _context.Medicamento.Include(m => m.Apresentacao)
                .Where(m => m.Id == id).Select(AsApresentacaoDTO);


            return Ok(apresentacoes);
        }

        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetPosologias([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Medicamento medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }
            int apresentacaoID = medicamento.ApresentacaoId;
            var posologias = _context.Posologia.Where(p => p.Apresentacao.Id == apresentacaoID)
                .Select(AsPosologiaDTO);

            return Ok(posologias);
        }

        [HttpGet("{id}/Info")]
        public async Task<IActionResult> GetMedicamentoInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var medicamento = await _context.Medicamento.Select(AsMedicamentoDTOInfo).SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(medicamento);
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.Select(AsMedicamentoDTO).SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(medicamento);
        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] int id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.Id)
            {
                return BadRequest();
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (medicamento.ApresentacaoId == 0)
            {
                return BadRequest(ModelState);
            }

            var medicamento2 = await _context.Medicamento.SingleOrDefaultAsync(m => m.Nome == medicamento.Nome);
            if (medicamento2 != null)
            {
                return BadRequest();
            }

            _context.Medicamento.Add(medicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamento", new { id = medicamento.Id }, medicamento);
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamento.Remove(medicamento);
            await _context.SaveChangesAsync();

            return Ok(medicamento);
        }

        private bool MedicamentoExists(int id)
        {
            return _context.Medicamento.Any(e => e.Id == id);
        }
    }
}