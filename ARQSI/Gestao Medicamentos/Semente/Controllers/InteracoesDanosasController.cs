﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System.Linq.Expressions;

namespace Semente.Controllers
{
    [Produces("application/json")]
    [Route("api/InteracoesDanosas")]
    public class InteracoesDanosasController : Controller
    {
        private readonly SementeContext _context;

        public InteracoesDanosasController(SementeContext context)
        {
            _context = context;
        }

        private static readonly Expression<Func<InteracoesDanosas, InteracoesDanosasDTO>> AsInteracoesDTO =
            x => new InteracoesDanosasDTO
            {
                Id = x.Id,
                FarmacoId1 = x.FarmacoId1,
                FarmacoId2 = x.FarmacoId2,

            };

        // GET: api/InteracoesDanosas
        [HttpGet]
        public IEnumerable<InteracoesDanosasDTO> GetInteracoesDanosas()
        {

            return _context.InteracoesDanosas.Select(AsInteracoesDTO);
        }

        // GET: api/InteracoesDanosas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInteracoesDanosas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var interacoesDanosas = await _context.InteracoesDanosas.Select(AsInteracoesDTO).SingleOrDefaultAsync(m => m.Id == id);

            if (interacoesDanosas == null)
            {
                return NotFound();
            }

            return Ok(interacoesDanosas);
        }

        // PUT: api/InteracoesDanosas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInteracoesDanosas([FromRoute] int id, [FromBody] InteracoesDanosas interacoesDanosas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != interacoesDanosas.Id)
            {
                return BadRequest();
            }
            if (interacoesDanosas.FarmacoId1 == interacoesDanosas.FarmacoId2)
            {
                return BadRequest(ModelState);
            }

            Medicamento medicamento1 = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == interacoesDanosas.FarmacoId1);
            Medicamento medicamento2 = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == interacoesDanosas.FarmacoId2);
            if (medicamento1 == null || medicamento2 == null)
            {
                return BadRequest(ModelState);
            }


            _context.Entry(interacoesDanosas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InteracoesDanosasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Interacoes
        [HttpPost]
        public async Task<IActionResult> PostInteracoesDanosas([FromBody] InteracoesDanosas interacoesDanosas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (interacoesDanosas.FarmacoId1 == interacoesDanosas.FarmacoId2)
            {
                return BadRequest(ModelState);
            }
            List<InteracoesDanosas> interacoes = _context.InteracoesDanosas.Where(i => i.FarmacoId1 == interacoesDanosas.FarmacoId1)
                .Where(i => i.FarmacoId2 == interacoesDanosas.FarmacoId2).ToList();

            if (interacoes.Count() > 0)
            {
                return BadRequest(ModelState);
            }
            Farmaco farmaco1 = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == interacoesDanosas.FarmacoId1);
            Farmaco farmaco2 = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == interacoesDanosas.FarmacoId2);
            if (farmaco1 == null || farmaco2 == null)
            {
                return BadRequest(ModelState);
            }



            _context.InteracoesDanosas.Add(interacoesDanosas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInteracoesDanosas", new { id = interacoesDanosas.Id }, interacoesDanosas);
        }

        // DELETE: api/InteracoesDanosas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInteracoesDanosas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var interacoesDanosas = await _context.InteracoesDanosas.SingleOrDefaultAsync(m => m.Id == id);
            if (interacoesDanosas == null)
            {
                return NotFound();
            }

            _context.InteracoesDanosas.Remove(interacoesDanosas);
            await _context.SaveChangesAsync();

            return Ok(interacoesDanosas);
        }

        private bool InteracoesDanosasExists(int id)
        {
            return _context.InteracoesDanosas.Any(e => e.Id == id);
        }
    }
}