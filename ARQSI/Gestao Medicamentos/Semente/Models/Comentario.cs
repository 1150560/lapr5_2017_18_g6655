﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Semente.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        [Required]
        public string Descricao { get; set; }
        public int ApresentacaoId { get; set; }
        public Apresentacao Apresentacao { get; set; }
    }
}
