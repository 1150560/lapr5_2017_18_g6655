﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class InteracoesDanosasDTO
    {
        public int Id { get; set; }
        public int FarmacoId1 { get; set; }

        public int FarmacoId2 { get; set; }
        
    }
}
