﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class PosologiaDTO
    {
        public int Id { get; set; }

        public string Frequencia { get; set; }
    }
}
