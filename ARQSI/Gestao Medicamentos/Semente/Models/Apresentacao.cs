﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class Apresentacao
    {
        public int Id { get; set; }
        [Required]
        public string Forma { get; set; }
        [Required]
        public string Concentracao { get; set; }
        [Required]
        public string Quantidade { get; set; }
        public int FarmacoId { get; set; }
        public Farmaco  Farmaco { get; set; }
  


    }
}
