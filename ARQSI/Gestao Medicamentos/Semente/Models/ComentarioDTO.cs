﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class ComentarioDTO
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public int ApresentacaoId { get; set; }
        public Apresentacao Apresentacao { get; set; }
    }
}
