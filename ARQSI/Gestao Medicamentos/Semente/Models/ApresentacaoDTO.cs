﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class ApresentacaoDTO
    {
        public int Id { get; set; }
        public string Forma { get; set; }
        public string Concentracao { get; set; }
        public string Quantidade { get; set; }

        public Farmaco Farmaco { get; set; }


    }
}
